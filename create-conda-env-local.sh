#!/usr/bin/env bash

export python_version="3.10.9"
export name="mmdc-sgld-py3109-torch-200-light200"

export target=/home/$USER/anaconda3/envs/$name


echo "Installing $name in $target ..."

if [ -d "$target" ]; then
   echo "Cleaning previous conda env"
   rm -rf $target
fi

# Create blank virtualenv
conda create --yes --prefix $target python==${python_version} pip

# Enter virtualenv
conda deactivate
conda activate $target
conda install --yes conda mamba


which python
python --version

mamba install --yes "pytorch-lightning>=2.0.0" lightning-utilities -c conda-forge
mamba install --yes "pytorch>=2.0.0" "torchvision>=0.5" pytorch-cuda=11.7 -c pytorch -c nvidia

conda deactivate
conda activate $target

# Requirements
pip3 install -r requirements-mmdc-sgld.txt

# Install sensorsio
rm -rf thirdparties/sensorsio
git clone https://src.koda.cnrs.fr/mmdc/sensorsio.git thirdparties/sensorsio
pip install thirdparties/sensorsio

# Install torchutils
rm -rf thirdparties/torchutils
git clone https://src.koda.cnrs.fr/mmdc/torchutils.git thirdparties/torchutils
pip install thirdparties/torchutils

# Install the current project in edit mode
pip install -e .[testing]

# Activate pre-commit hooks
pre-commit install


conda deactivate
