"""Processing full images for inference"""

import logging
from collections.abc import Generator, Iterator
from dataclasses import dataclass
from enum import Enum, auto
from pathlib import Path
from typing import Literal

import numpy as np
import rasterio as rio
import torch

from ..datamodules.components.datamodule_utils import apply_log_to_s1
from ..datamodules.constants import S1_NOISE_LEVEL
from ..models.datatypes import AuxData, MMDCTorchModel

logging.basicConfig(
    level=getattr(logging, "INFO", None),
    datefmt="%y-%m-%d %H:%M:%S",
    format="%(asctime)s :: %(levelname)s :: %(message)s",
)


WC_BIO_SIZE = 19
WC_SIZE = 103


def srtm_height_aspect(srtm_data: torch.Tensor) -> torch.Tensor:
    """
    get the srtm height, slope, and aspect
    param : srtm_path_filename
    return : stack with height, sin slope, cos aspect, sin aspect
    """
    srtm_height = srtm_data[0, ...]
    srtm_slope = srtm_data[1, ...]
    srtm_aspect = srtm_data[2, ...]
    # calculate trigonometric representation
    sin_slope = torch.sin(torch.deg2rad(srtm_slope))
    cos_aspect = torch.cos(torch.deg2rad(srtm_aspect))
    sin_aspect = torch.sin(torch.deg2rad(srtm_aspect))

    return torch.stack([srtm_height, sin_slope, cos_aspect, sin_aspect])


def join_even_odd_s2_angles(s2_angles_data: torch.Tensor) -> torch.Tensor:
    """
    Compute the joining between angles
    between odd and even detectors in S2

    :param: s2_angles_data: [BxCxWxH]

    inspired on :
    https://framagit.org/jmichel-otb/sensorsio/-/blob/master/notebooks/sentinel2_angles.ipynb
    """
    # s2_angles_data
    # C = [sun_zen, sun_az, even_zen, odd_zen, even_az, odd_az]

    (sun_zen, sun_az, even_zen, odd_zen, even_az, odd_az) = (
        s2_angles_data[0, ...],
        s2_angles_data[1, ...],
        s2_angles_data[2, ...],
        s2_angles_data[3, ...],
        s2_angles_data[4, ...],
        s2_angles_data[5, ...],
    )

    join_zen = even_zen.clone().detach().requires_grad_(False)
    join_zen[torch.isnan(even_zen)] = odd_zen[torch.isnan(even_zen)]
    join_az = even_az.clone().detach().requires_grad_(False)
    join_az[torch.isnan(even_az)] = odd_az[torch.isnan(even_az)]

    return torch.stack(
        [
            torch.cos(torch.deg2rad(sun_zen)),
            torch.cos(torch.deg2rad(sun_az)),
            torch.sin(torch.deg2rad(sun_az)),
            torch.cos(torch.deg2rad(join_zen)),
            torch.cos(torch.deg2rad(join_az)),
            torch.sin(torch.deg2rad(join_az)),
        ]
    )


@dataclass
class PatchifyData:
    """Information needed to unpatchify the data"""

    patch_size: int
    overlap: int
    nb_patches: int
    last_patch_pos: int


class OrbitType(Enum):
    """S1 orbit direction type"""

    ASC = auto()
    DESC = auto()


@dataclass
class S1InferenceParams:
    """Specific configuration for S1 inference"""

    s1_asc: Path | None
    s1_desc: Path | None
    missing_orbit: OrbitType | None
    srtm: Path
    w_c: Path
    patch_size: int = 256
    overlap: int | None = None


@dataclass
class S2InferenceParams:
    """Specific configuration for S2 inference"""

    s2_data: Path
    srtm: Path
    w_c: Path
    patch_size: int = 256
    overlap: int | None = None


InferenceParams = S1InferenceParams | S2InferenceParams


@dataclass
class InferenceConfig:
    """Complete configuration for inference"""

    s1_config: S1InferenceParams | None
    s2_config: S2InferenceParams | None
    model: MMDCTorchModel
    out_dir: Path


@dataclass
class Chunk:
    """Model for an image chunk in windowed processing"""

    x_0: int
    y_0: int
    width: int
    height: int

    def as_tuple(self) -> tuple[int, int, int, int]:
        """Return the chunk coordinates as a tuple"""
        return self.x_0, self.y_0, self.width, self.height


@dataclass
class ChunkData:
    """Chunk definition and associated numpy data, for reading
    from and writing to disk."""

    roi: Chunk
    data: np.ndarray


@dataclass
class BatchData:
    """Chunk definition and associated torch data for processing."""

    roi: Chunk
    data: torch.Tensor


def patchify(
    data: torch.Tensor, patch_size: int, overlap: int
) -> tuple[torch.Tensor, PatchifyData]:
    """Dimensions are [C,H,W] H is supposed to match the patch size"""
    _, height, width = data.shape
    assert height == patch_size, f"{height=} vs {patch_size=}"
    assert patch_size > 0
    pos_last = 0
    patch_list = []
    nb_patches = 0
    while pos_last < width - patch_size:
        patch_list.append(
            torch.from_numpy(data[:, :, pos_last : pos_last + patch_size]).type(
                torch.FloatTensor
            )
        )
        pos_last += patch_size - overlap
        nb_patches += 1
    if pos_last - patch_size + overlap != width - patch_size:
        pos_last = width - patch_size
        patch_list.append(
            torch.from_numpy(data[:, :, -patch_size:]).type(torch.FloatTensor)
        )
        nb_patches += 1
    out_data = torch.stack(patch_list, dim=0)
    assert nb_patches == out_data.shape[0]
    return out_data, PatchifyData(patch_size, overlap, nb_patches, pos_last)


def unpatchify(data: torch.Tensor, patch_spec: PatchifyData) -> np.ndarray:
    """Input shape is [nb_patches, bands, patch_size, patch_size]"""
    nb_patches, nb_bands, patch_size, _ = data.shape
    assert patch_size == patch_spec.patch_size
    assert nb_patches == patch_spec.nb_patches
    assert patch_spec.overlap % 2 == 0
    width = patch_spec.last_patch_pos + patch_size
    out_data = np.zeros((nb_bands, patch_size, width))
    for idx in range(0, nb_patches - 1):
        start = (patch_size - patch_spec.overlap) * idx + patch_spec.overlap // 2
        stop = start + patch_size - patch_spec.overlap
        out_data[:, :, start:stop] = data[
            idx, :, :, patch_spec.overlap // 2 : patch_size - patch_spec.overlap // 2
        ]
    start = width - patch_size + patch_spec.overlap // 2
    stop = width - patch_spec.overlap // 2
    out_data[:, :, start:stop] = data[
        -1, :, :, patch_spec.overlap // 2 : patch_size - patch_spec.overlap // 2
    ]
    return out_data


def generate_chunks(
    im_width: int, im_height: int, chunk_size: int, overlap: int = 0
) -> Generator[Chunk, None, None]:
    """Given the width and height of an image, a number of lines per chunk and
    the number of overlap lines, generate the list of chunks for the image.
    The chunks span all the width. The last chunk is discarded if it is
    incomplete.
    """
    y_0 = 0
    while y_0 + chunk_size <= im_height:
        x_0 = 0
        while x_0 + chunk_size <= im_width:
            yield Chunk(x_0, y_0, chunk_size, chunk_size)
            x_0 += chunk_size - overlap
        y_0 += chunk_size - overlap


def chunk_generator(
    geotif: Path,
    chunk_size: int,
    overlap: int = 0,
    read_type: np.dtype | None = None,
    dummy_data: bool = False,
) -> Generator[ChunkData, None, None]:
    """If dummy_data = True, generate a tensor of zeros with the correct nb
    of bands and dimensions"""
    logging.info("Chunk generator for %s", geotif)
    if read_type is None:
        read_type = np.dtype(np.float32)
    with rio.open(geotif) as image:
        profile = image.profile.copy()
        height = profile["height"]
        width = profile["width"]
        nb_bands = profile["count"]
        for chunk in generate_chunks(width, height, chunk_size, overlap):
            roi = rio.windows.Window(*(chunk.as_tuple()))
            if not dummy_data:
                data = image.read(window=roi).astype(read_type)
                yield ChunkData(chunk, data)
            else:
                yield ChunkData(chunk, np.zeros((nb_bands, chunk_size, chunk_size)))


BatchGeneratorType = Generator[BatchData, None, None] | Generator[None, None, None]


def batch_generator(
    geotif: Path,
    patch_size: int = 256,
    overlap: int = 0,
    read_type: np.dtype | None = None,
    dummy_data: bool = False,
) -> Generator[BatchData, None, None]:
    """Returns a generator which provides batches made of patches extracted from
    chunks of the input image."""
    if read_type is None:
        read_type = np.dtype(np.float32)
    nlines = patch_size
    for chunk in chunk_generator(geotif, nlines, overlap, read_type, dummy_data):
        data = torch.from_numpy(chunk.data).type(torch.FloatTensor).unsqueeze(0)
        yield BatchData(roi=chunk.roi, data=data)


def build_s1_batch(
    s1_asc_data: torch.Tensor, s1_desc_data: torch.Tensor
) -> torch.Tensor:
    """Compute logs, ratio and stack"""
    return torch.stack(
        [
            apply_log_to_s1(s1_asc_data[:, 0, :, :]),
            apply_log_to_s1(s1_asc_data[:, 1, :, :]),
            apply_log_to_s1(
                s1_asc_data[:, 1, :, :] / (s1_asc_data[:, 0, :, :] + S1_NOISE_LEVEL)
            ),
            apply_log_to_s1(s1_desc_data[:, 0, :, :]),
            apply_log_to_s1(s1_desc_data[:, 1, :, :]),
            apply_log_to_s1(
                s1_desc_data[:, 1, :, :] / (s1_desc_data[:, 0, :, :] + S1_NOISE_LEVEL)
            ),
        ],
        dim=1,
    )


def build_s1_ang_batch(batch_size: int) -> torch.Tensor:
    """Get the angles and reshape"""
    angles = torch.ones(1)  # get_s1_acquisition_angles(s1_file_name.name)
    return torch.ones((batch_size, angles.shape[0])) * angles


def crop_chunk(chunk_data: ChunkData, overlap: int) -> ChunkData:
    """Crop the chunk with half the overlap and update the chunk coordinates"""
    crop = int(overlap // 2)
    return ChunkData(
        Chunk(
            chunk_data.roi.x_0 + crop,
            chunk_data.roi.y_0 + crop,
            chunk_data.roi.width - 2 * crop,
            chunk_data.roi.height - 2 * crop,
        ),
        chunk_data.data[:, crop:-crop, crop:-crop],
    )


def chunk_writer(
    outfile: Path, ref_image: Path, nb_output_bands: int, overlap: int = 0
) -> Generator[None, ChunkData, None]:
    """Returns a generator to which data can be sent for writing:

    srtm_gen = batch_generator(input_file, nlines, overlap)
    patch_data = next(srtm_gen)
    output_writer = chunk_writer(
        output_file,
        input_file,
        nb_output_bands=nb_bands,
    )
    next(output_writer)
    for chunk in srtm_gen:
        out_data = unpatchify_batch(
            chunk.data, patch_data, overlap
        )
        output_writer.send(ChunkData(roi=chunk.roi, data=out_data))"""
    with rio.open(ref_image) as refim:
        profile = refim.profile.copy()
    profile.update(dtype=rio.float32, count=nb_output_bands)
    with rio.open(outfile, "w", **profile) as dst:
        while True:
            chunk_data: ChunkData = yield
            if overlap > 0:
                chunk_data = crop_chunk(chunk_data, overlap)
            roi = rio.windows.Window(*(chunk_data.roi.as_tuple()))
            data = chunk_data.data
            print(f"{data.shape=}")
            assert data.shape[0] == nb_output_bands
            for idx in range(nb_output_bands):
                dst.write(data[idx, :, :].astype(rio.float32), idx + 1, window=roi)


def build_s2_ang_batch(data: torch.Tensor) -> torch.Tensor:
    """Get the angles and join even and odd detectors"""
    return join_even_odd_s2_angles(data[:, 14:, :, :].angles.transpose(0, 1)).transpose(
        0, 1
    )


def dummy_generator() -> Generator[None, None, None]:
    """A generator that always yields None"""
    while True:
        yield None


@dataclass
class InferenceData:
    """Data used in the inference loop"""

    s2_data: BatchData | None
    s1_asc: BatchData | None
    s1_desc: BatchData | None
    srtm: BatchData
    w_c: BatchData

    def __post_init__(self) -> None:
        assert self.s2_data is not None or (
            self.s1_asc is not None and self.s1_desc is not None
        )


@dataclass
class ModelConfig:
    """Torch model and associated device"""

    model: MMDCTorchModel
    device: torch.device


def process_batch(
    data: InferenceData,
    sensor: str,
    model_config: ModelConfig,
    config: InferenceParams,
    output_writer: Generator[None, ChunkData, None],
) -> None:
    """Process a batch of data and send it to the output writer"""
    batch_size = data.srtm.data.shape[0]
    psx, psy = data.srtm.data.shape[-2], data.srtm.data.shape[-1]
    wc_data = torch.zeros((batch_size, WC_SIZE, psx, psy))
    wc_data[:, -WC_BIO_SIZE:, :, :] = data.w_c.data
    srtm_data = srtm_height_aspect(data.srtm.data.transpose(0, 1)).transpose(0, 1)
    if sensor == "S1" and isinstance(config, S1InferenceParams):
        assert config.s1_asc is not None and config.s1_desc is not None
        assert data.s1_asc is not None and data.s1_desc is not None
        latent = model_config.model.get_latents1(
            build_s1_batch(data.s1_asc.data, data.s1_desc.data),
            AuxData(  # TODO: meteo before dem
                build_s1_ang_batch(batch_size).to(model_config.device),
                torch.ones(batch_size, 6, psx, psy).to(model_config.device),
                srtm_data.to(model_config.device),
                wc_data.to(model_config.device),
            ),
        )
    elif isinstance(config, S2InferenceParams):
        assert data.s2_data is not None
        latent = model_config.model.get_latents2(
            data.s2_data.data[:, :10, :, :],
            AuxData(  # TODO: meteo before dem
                torch.zeros(batch_size, 3).to(model_config.device),
                build_s2_ang_batch(data.s2_data.data).to(model_config.device),
                srtm_data.to(model_config.device),
                wc_data.to(model_config.device),
            ),
        )
    # TODO: stack mean and var in latent
    output_writer.send(ChunkData(roi=data.srtm.roi, data=latent.mean[0, ...].numpy()))


@dataclass
class InferenceDataStream:
    """Stream of data (image chunks as batches) for a sensor (S1 or S2) to be processed
    and written to the output image"""

    generators: Iterator[tuple[BatchData | None, ...]]
    sensor: Literal["S1", "S2"]
    out_image_name: Path


def build_inference_stream(
    config: InferenceParams, overlap: int, out_dir: Path, image_name_suffix: str = ""
) -> InferenceDataStream:
    """Use the config to build the InferenceDataStream"""
    s1_asc_gen: BatchGeneratorType
    s1_desc_gen: BatchGeneratorType
    s2_gen: BatchGeneratorType
    sensor: Literal["S1", "S2"]
    match config:
        case S1InferenceParams(
            s1_asc_conf, s1_desc_conf, missing_orbit, _, _, patch_size
        ):
            assert s1_asc_conf is not None and s1_desc_conf is not None
            s1_asc_gen = batch_generator(
                s1_asc_conf,
                patch_size=patch_size,
                overlap=overlap,
                dummy_data=(missing_orbit == OrbitType.ASC),
            )
            s1_desc_gen = batch_generator(
                s1_desc_conf,
                patch_size=patch_size,
                overlap=overlap,
                dummy_data=(missing_orbit == OrbitType.DESC),
            )
            s2_gen = dummy_generator()
            sensor = "S1"
            out_image_name = out_dir / f"s1_latent{image_name_suffix}.tif"
        case S2InferenceParams(s2_conf, _, _, patch_size):
            assert s2_conf is not None
            s2_gen = batch_generator(s2_conf, patch_size=patch_size, overlap=overlap)
            s1_asc_gen = dummy_generator()
            s1_desc_gen = dummy_generator()
            sensor = "S2"
            out_image_name = out_dir / f"s2_latent{image_name_suffix}.tif"
    return InferenceDataStream(
        zip(
            s2_gen,
            s1_asc_gen,
            s1_desc_gen,
            batch_generator(config.srtm, patch_size=patch_size, overlap=overlap),
            batch_generator(config.w_c, patch_size=patch_size, overlap=overlap),
            strict=True,
        ),
        sensor,
        out_image_name,
    )


def inference(
    config: InferenceParams,
    model: MMDCTorchModel,
    out_dir: Path,
    image_name_suffix: str = "",
    device: torch.device | None = None,
) -> Path:
    """Generic inference (data source specifics are in the config) using the given
    torch model"""
    if device is None:
        device = torch.device("cpu")
    if (overlap := config.overlap) is None:
        overlap = model.nb_cropped_hw * 2
    assert overlap < config.patch_size

    inf_stream = build_inference_stream(config, overlap, out_dir, image_name_suffix)

    if not out_dir.exists():
        out_dir.mkdir()

    output_writer = chunk_writer(  # pylint: disable=E1111
        inf_stream.out_image_name,
        config.srtm,
        nb_output_bands=model.config.auto_enc.s1_encoder.out_channels,
        overlap=overlap,
    )
    next(output_writer)
    model.nets.embedders.s1_srtm.device = device  # type: ignore[assignment]
    model.device = device
    model.eval()
    with torch.no_grad():
        for s2_data, s1_asc, s1_desc, srtm, w_c in inf_stream.generators:
            assert srtm is not None and w_c is not None
            process_batch(
                InferenceData(s2_data, s1_asc, s1_desc, srtm, w_c),
                inf_stream.sensor,
                ModelConfig(model, device),
                config,
                output_writer,
            )
    output_writer.close()
    return inf_stream.out_image_name
