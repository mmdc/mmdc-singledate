"""
Module which implements the processing of chunks of data provided by iota2 in
the external features pipeline using MMDC pre-trained models as feature
extractors.
"""


from dataclasses import dataclass
from typing import Any, Protocol

import numpy as np
import torch
from torchutils import patches

from ..datamodules.components.datamodule_utils import apply_log_to_s1
from ..datamodules.constants import S1_NOISE_LEVEL
from ..inference.processing import WC_BIO_SIZE, PatchifyData
from ..models.types import AuxData, MMDCTorchModel


@dataclass
class ChunkReturnType:
    """A processed chunk is made of a Numpy array containing the data and a
    list of labels for each computed feature"""

    data: np.ndarray
    labels: list[str]


Iota2Date = str
Iota2SensorName = str
Iota2DataDates = dict[Iota2SensorName, list[Iota2Date]]

PATCH_SIZE = 256


class DataContainer(Protocol):  # pylint: disable=R0904
    """Protocol with the methods that a iota2DataContainer should provide"""

    dates: Iota2DataDates

    # TODO: add missing protocol methods
    def get_raw_dates(self) -> Any:  # pylint: disable=C0103,C0116
        ...

    def get_raw_Sentinel1_ASC_vh(self) -> Any:  # pylint: disable=C0103,C0116
        ...

    def get_raw_Sentinel1_DESC_vh(self) -> Any:  # pylint: disable=C0103,C0116
        ...

    def get_raw_Sentinel1_ASC_vv(self) -> Any:  # pylint: disable=C0103,C0116
        ...

    def get_raw_Sentinel1_DESC_vv(self) -> Any:  # pylint: disable=C0103,C0116
        ...

    def get_raw_Sentinel2_B2(self) -> Any:  # pylint: disable=C0103,C0116
        ...

    def get_raw_Sentinel2_B3(self) -> Any:  # pylint: disable=C0103,C0116
        ...

    def get_raw_Sentinel2_B4(self) -> Any:  # pylint: disable=C0103,C0116
        ...

    def get_raw_Sentinel2_B5(self) -> Any:  # pylint: disable=C0103,C0116
        ...

    def get_raw_Sentinel2_B6(self) -> Any:  # pylint: disable=C0103,C0116
        ...

    def get_raw_Sentinel2_B7(self) -> Any:  # pylint: disable=C0103,C0116
        ...

    def get_raw_Sentinel2_B8(self) -> Any:  # pylint: disable=C0103,C0116
        ...

    def get_raw_Sentinel2_B8A(self) -> Any:  # pylint: disable=C0103,C0116
        ...

    def get_raw_Sentinel2_B11(self) -> Any:  # pylint: disable=C0103,C0116
        ...

    def get_raw_Sentinel2_B12(self) -> Any:  # pylint: disable=C0103,C0116
        ...

    def get_raw_userFeatures_S1IncidenceAscending(  # pylint: disable=C0103,C0116
        self,
    ) -> Any:
        ...

    def get_raw_userFeatures_S1AzimuthAscending(  # pylint: disable=C0103,C0116
        self,
    ) -> Any:
        ...

    def get_raw_userFeatures_S1IncidenceDescending(  # pylint: disable=C0103,C0116
        self,
    ) -> Any:
        ...

    def get_raw_userFeatures_S1AzimuthDescending(  # pylint: disable=C0103,C0116
        self,
    ) -> Any:
        ...

    def get_raw_userFeatures_EVEN_ZENITH(  # pylint: disable=C0103,C0116
        self,
    ) -> Any:
        ...

    def get_raw_userFeatures_EVEN_AZIMUTH(  # pylint: disable=C0103,C0116
        self,
    ) -> Any:
        ...

    def get_raw_userFeatures_ODD_ZENITH(  # pylint: disable=C0103,C0116
        self,
    ) -> Any:
        ...

    def get_raw_userFeatures_ODD_AZIMUTH(  # pylint: disable=C0103,C0116
        self,
    ) -> Any:
        ...

    def get_raw_userFeatures_ZENITH(  # pylint: disable=C0103,C0116
        self,
    ) -> Any:
        ...

    def get_raw_userFeatures_AZIMUTH(  # pylint: disable=C0103,C0116
        self,
    ) -> Any:
        ...

    def get_raw_userFeatures_elevation(  # pylint: disable=C0103,C0116
        self,
    ) -> Any:
        ...

    def get_raw_userFeatures_slope(  # pylint: disable=C0103,C0116
        self,
    ) -> Any:
        ...

    def get_raw_userFeatures_aspect(  # pylint: disable=C0103,C0116
        self,
    ) -> Any:
        ...

    def get_raw_userFeatures_worldclim(  # pylint: disable=C0103,C0116
        self,
    ) -> Any:
        ...


def build_labels(dates: list[str], prefix: str, band_names: list[str]) -> list[str]:
    """From a list of dates and band names, generate the list of labels as
    prefix_band_date"""
    return [f"{prefix}_{band}_{date}" for date in dates for band in band_names]


def patchify_data(
    data: list[np.ndarray], patch_size: int, patch_margin: int
) -> tuple[torch.Tensor, PatchifyData]:
    """Generate a tensor of patches from a list of arrays (one per
    channel/band)"""
    # Permute dimensions to fit patchify
    # Shape before permutation is C,H,W,D. D being the dates
    # Shape after permutation is D,C,H,W
    data_tensor = torch.Tensor(data).permute(-1, 0, 1, 2)
    band_h, band_w = data_tensor.shape[-2:]
    height, width = patches.patchify(
        data_tensor[0, ...], patch_size=patch_size, margin=patch_margin
    ).shape[:2]
    # patchify takes tensor of shape C,W,H and
    # returns a shape of PX, PY, C, PW, PH
    # The flatten2d method returns a
    # tensor of shape PX*PY, C, PW, PH
    # Applying this to all dates result in
    # a tensor of shape D, PX*PY, C, PW, PH
    input_data = torch.stack(
        [
            patches.flatten2d(
                patches.patchify(img, patch_size=patch_size, margin=patch_margin)
            )
            for img in data_tensor
        ]
    )
    return input_data, PatchifyData(height, width, band_h, band_w)


def build_s2_batch(
    data: DataContainer, patch_margin: int, patch_size: int = PATCH_SIZE
) -> tuple[torch.Tensor, PatchifyData]:
    """Generate batch of S2 patches from the iota2 data container"""
    s2_bands_i2 = [
        data.get_raw_Sentinel2_B2(),
        data.get_raw_Sentinel2_B3(),
        data.get_raw_Sentinel2_B4(),
        data.get_raw_Sentinel2_B5(),
        data.get_raw_Sentinel2_B6(),
        data.get_raw_Sentinel2_B7(),
        data.get_raw_Sentinel2_B8(),
        data.get_raw_Sentinel2_B8A(),
        data.get_raw_Sentinel2_B11(),
        data.get_raw_Sentinel2_B12(),
    ]
    return patchify_data(s2_bands_i2, patch_size, patch_margin)


def match_s1_dates(
    asc_dates: list[Iota2Date], desc_dates: list[Iota2Date]
) -> tuple[list[Iota2Date | None], list[Iota2Date | None]]:
    """Pair the ascending and descending dates. The 2 output date lists have
    the same lengths and cotain None for the missing dates of each orbit"""
    asc_set = set(asc_dates)
    desc_set = set(desc_dates)
    all_dates = sorted(list(asc_set.union(desc_set)))
    out_asc_dates = [d if d in asc_set else None for d in all_dates]
    out_desc_dates = [d if d in desc_set else None for d in all_dates]
    return out_asc_dates, out_desc_dates


def insert_missing_dates(data: np.ndarray, dates: list[Iota2Date | None]) -> np.ndarray:
    """Interleave images with zeros for the missing dates (given as None) in
    the input list"""
    result = np.zeros((data.shape[0], data.shape[1], len(dates)))
    indices = [i for i in range(len(dates)) if dates[i] is not None]
    result[:, :, indices] = data
    return result


def build_s1_batch(
    data: DataContainer, patch_margin: int, patch_size: int = PATCH_SIZE
) -> tuple[torch.Tensor, PatchifyData]:
    """Generate batch of S2 patches from the iota2 data container"""
    assert data.dates["Sentinel1_ASC_vv"] == data.dates["Sentinel1_ASC_vh"]
    assert data.dates["Sentinel1_DES_vv"] == data.dates["Sentinel1_DES_vh"]
    asc_dates, desc_dates = match_s1_dates(
        data.dates["Sentinel1_ASC_vv"],
        data.dates["Sentinel1_DES_vv"],
    )
    vv_asc = apply_log_to_s1(data.get_raw_Sentinel1_ASC_vv())
    vh_asc = apply_log_to_s1(data.get_raw_Sentinel1_ASC_vh())
    vv_desc = apply_log_to_s1(data.get_raw_Sentinel1_DESC_vv())
    vh_desc = apply_log_to_s1(data.get_raw_Sentinel1_DESC_vh())
    s1_bands_i2 = [
        insert_missing_dates(vv_asc, asc_dates),
        insert_missing_dates(vh_asc, asc_dates),
        insert_missing_dates(vh_asc / (vv_asc + S1_NOISE_LEVEL), asc_dates),
        insert_missing_dates(vv_desc, desc_dates),
        insert_missing_dates(vh_desc, desc_dates),
        insert_missing_dates(vh_desc / (vv_desc + S1_NOISE_LEVEL), desc_dates),
    ]
    return patchify_data(s1_bands_i2, patch_size, patch_margin)


def build_s1ang_batch(
    data: DataContainer, patch_margin: int, patch_size: int = PATCH_SIZE
) -> tuple[torch.Tensor, torch.Tensor, PatchifyData]:
    """For each patch, we have a vector of cos(eia), cos(azim), sin(azim)
    with shape (batch,3)"""
    asc_dates, desc_dates = match_s1_dates(
        data.dates["Sentinel1_ASC_vv"],
        data.dates["Sentinel1_DES_vv"],
    )
    s1_angs_i2 = [
        insert_missing_dates(
            np.cos(np.deg2rad(data.get_raw_userFeatures_S1IncidenceAscending())),
            asc_dates,
        ),
        insert_missing_dates(
            np.sin(np.deg2rad(data.get_raw_userFeatures_S1AzimuthAscending())),
            asc_dates,
        ),
        insert_missing_dates(
            np.cos(np.deg2rad(data.get_raw_userFeatures_S1AzimuthAscending())),
            asc_dates,
        ),
    ]
    patches_asc, patch_data = patchify_data(s1_angs_i2, patch_size, patch_margin)
    patches_asc = patches_asc[:, :, :, patch_size // 2, patch_size // 2]
    s1_angs_i2 = [
        insert_missing_dates(
            np.cos(np.deg2rad(data.get_raw_userFeatures_S1IncidenceDescending())),
            desc_dates,
        ),
        insert_missing_dates(
            np.sin(np.deg2rad(data.get_raw_userFeatures_S1AzimuthDescending())),
            desc_dates,
        ),
        insert_missing_dates(
            np.cos(np.deg2rad(data.get_raw_userFeatures_S1AzimuthDescending())),
            desc_dates,
        ),
    ]
    patches_desc, patch_data = patchify_data(s1_angs_i2, patch_size, patch_margin)
    patches_desc = patches_desc[:, :, :, patch_size // 2, patch_size // 2]
    return patches_asc, patches_desc, patch_data


def build_s2ang_batch(
    data: DataContainer, patch_margin: int, patch_size: int = PATCH_SIZE
) -> tuple[torch.Tensor, PatchifyData]:
    """Generate batch of S2 angles patches from the iota2 data container"""
    even_zen = data.get_raw_userFeatures_EVEN_ZENITH()
    odd_zen = data.get_raw_userFeatures_ODD_ZENITH()
    even_az = data.get_raw_userFeatures_EVEN_AZIMUTH()
    odd_az = data.get_raw_userFeatures_ODD_AZIMUTH()
    sun_zen = data.get_raw_userFeatures_ZENITH()
    sun_az = data.get_raw_userFeatures_AZIMUTH()

    joint_zen = np.array(even_zen)
    joint_zen[np.isnan(even_zen)] = odd_zen[np.isnan(even_zen)]
    joint_az = np.array(even_az)
    joint_az[np.isnan(even_az)] = odd_az[np.isnan(even_az)]
    s2_angs_i2 = [
        np.cos(np.deg2rad(sun_zen)),
        np.cos(np.deg2rad(sun_az)),
        np.sin(np.deg2rad(sun_az)),
        np.cos(np.deg2rad(joint_zen)),
        np.cos(np.deg2rad(joint_az)),
        np.sin(np.deg2rad(joint_az)),
    ]
    return patchify_data(s2_angs_i2, patch_size, patch_margin)


def build_srtm_batch(
    data: DataContainer, patch_margin: int, patch_size: int = PATCH_SIZE
) -> tuple[torch.Tensor, PatchifyData]:
    """Generate batch of SRTM patches from the iota2 data container"""
    srtm_height = data.get_raw_userFeatures_elevation()
    srtm_slope = data.get_raw_userFeatures_slope()
    srtm_aspect = data.get_raw_userFeatures_aspect()
    sin_slope = np.sin(np.deg2rad(srtm_slope))
    cos_aspect = np.cos(np.deg2rad(srtm_aspect))
    sin_aspect = np.sin(np.deg2rad(srtm_aspect))
    srtm_bands = [srtm_height, sin_slope, cos_aspect, sin_aspect]
    batch, patch_data = patchify_data(srtm_bands, patch_size, patch_margin)
    return torch.squeeze(batch, dim=0), patch_data


def build_wc_batch(
    data: DataContainer, patch_margin: int, patch_size: int = PATCH_SIZE
) -> tuple[torch.Tensor, PatchifyData]:
    """Generate batch of WorldClim patches from the iota2 data container"""
    wc_data = data.get_raw_userFeatures_worldclim()[:, :, -WC_BIO_SIZE:]
    chunk_h, chunk_w, _ = wc_data.shape
    wc_bands = [
        np.reshape(wc_data[:, :, wcb], (chunk_h, chunk_w, 1))
        for wcb in range(WC_BIO_SIZE)
    ]
    batch, patch_data = patchify_data(wc_bands, patch_size, patch_margin)
    return torch.squeeze(batch, dim=0), patch_data


@dataclass
class S2Batch:
    """The data needed for S2 latent generation"""

    batch: torch.Tensor
    batch_ang: torch.Tensor
    srtm: torch.Tensor
    w_c: torch.Tensor


def generate_s2_predictions(
    data: S2Batch,
    model: MMDCTorchModel,
    batch_size: int,
    patch_data: PatchifyData,
    device: torch.device,
) -> torch.Tensor:
    """Generate the S2 predictions"""
    nb_dates, _, _, _, _ = data.batch.shape
    list_pred_mu_s2 = []
    for date in range(nb_dates):
        list_pred = []
        for s2_patch, s2_ang, srtm_x, wc_x in zip(
            data.batch[date, ...].split(batch_size),
            data.batch_ang[date, ...].split(batch_size),
            data.srtm.split(batch_size),
            data.w_c.split(batch_size),
            strict=True,
        ):
            s2_patch = s2_patch.to(device)
            latent_s2 = model.get_latents2(
                s2_patch,
                AuxData(
                    torch.ones(batch_size, 2),
                    torch.ones(batch_size, 2),
                    s2_ang.to(device),
                    srtm_x.to(device),
                    wc_x.to(device),
                ),
            )
            list_pred.append(latent_s2.mean)
        list_pred_mu_s2.append(torch.cat(list_pred))

    pred_mu_s2 = torch.stack(
        [
            patches.unpatchify(
                patches.unflatten2d(mu_s2, patch_data.height, patch_data.width),
                margin=model.nb_cropped_hw,
            )
            for mu_s2 in list_pred_mu_s2
        ],
        -1,
    )
    return pred_mu_s2


def process_s2_batch(
    data: S2Batch,
    batch_size: int,
    model: MMDCTorchModel,
    patch_data: PatchifyData,
) -> np.ndarray:
    """Produce a chunk (numpy array) with the latents for the S2 encoder"""
    nb_dates, nb_patches, _, psx, psy = data.batch.shape
    assert data.batch_ang.shape[0] == nb_dates
    assert data.batch_ang.shape[1] == nb_patches
    assert data.batch_ang.shape[-2] == psx
    assert data.batch_ang.shape[-1] == psy
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = model.to(device=device)
    model.eval()
    with torch.no_grad():
        pred_mu_s2 = generate_s2_predictions(
            data, model, batch_size, patch_data, device
        )
    # We permute dimensions in order to have H,W,C,D
    # and we collapse the two to have H,W,D*C
    # Sizes after unpatchify might be greater than original
    # images because of padding. We crop to original dimensions
    pred_s2: np.ndarray = (
        (
            pred_mu_s2.permute(1, 2, 3, 0)
            .flatten(-2)[: patch_data.chunk_h, : patch_data.chunk_w, :]
            .contiguous()
        )
        .detach()
        .numpy()
    )
    return pred_s2


def process_s2_chunk(
    data: DataContainer, model: MMDCTorchModel, batch_size: int = 1
) -> ChunkReturnType:
    """Process the input S2 chunk. Build the butch and delegate to
    process_s2_batch"""
    latent_names = [
        f"latent_{i+1}" for i in range(model.config.auto_enc.s2_encoder.out_channels)
    ]
    labels = build_labels(data.get_raw_dates()["Sentinel2"], "S2", latent_names)
    patch_margin = model.nb_cropped_hw
    batch, patch_data = build_s2_batch(data, patch_margin=model.nb_cropped_hw)
    batch_ang, _ = build_s2ang_batch(data, patch_margin)
    srtm, pd_srtm = build_srtm_batch(data, patch_margin)
    assert patch_data == pd_srtm
    w_c, pd_wc = build_wc_batch(data, patch_margin)
    assert patch_data == pd_wc
    s2_chunk = process_s2_batch(
        S2Batch(batch, batch_ang, srtm, w_c), batch_size, model, patch_data
    )
    return ChunkReturnType(s2_chunk, labels)


@dataclass
class S1Batch:
    """The data needed for S1 latent generation"""

    batch: torch.Tensor
    batch_ang_asc: torch.Tensor
    batch_ang_desc: torch.Tensor
    srtm: torch.Tensor
    w_c: torch.Tensor


def generate_s1_predictions(
    data: S1Batch,
    model: MMDCTorchModel,
    batch_size: int,
    patch_data: PatchifyData,
    device: torch.device,
) -> torch.Tensor:
    """Generate the S2 predictions"""
    list_pred_mu_s1 = []
    nb_dates, _, _, psx, psy = data.batch.shape
    for date in range(nb_dates):
        list_pred = []
        for s1_patch, asc_ang_patch, desc_ang_patch, srtm_x, wc_x in zip(
            data.batch[date, ...].split(batch_size),
            data.batch_ang_asc[date, ...].split(batch_size),
            data.batch_ang_desc[date, ...].split(batch_size),
            data.srtm.split(batch_size),
            data.w_c.split(batch_size),
            strict=True,
        ):
            s1_patch = s1_patch.to(device)
            latent_s1 = model.get_latents1(
                s1_patch,
                AuxData(
                    asc_ang_patch.to(device),
                    desc_ang_patch.to(device),
                    torch.ones(batch_size, 4, psx, psy).to(device),
                    srtm_x.to(device),
                    wc_x.to(device),
                ),
            )
            list_pred.append(latent_s1.mean)
        list_pred_mu_s1.append(torch.cat(list_pred))

    return torch.stack(
        [
            patches.unpatchify(
                patches.unflatten2d(mu_s1, patch_data.height, patch_data.width),
                margin=model.nb_cropped_hw,
            )
            for mu_s1 in list_pred_mu_s1
        ],
        -1,
    )


def process_s1_batch(
    data: S1Batch,
    batch_size: int,
    model: MMDCTorchModel,
    patch_data: PatchifyData,
) -> np.ndarray:
    """Produce a chunk (numpy array) with the latents for the S1 encoder"""
    nb_dates, nb_patches, _, _, _ = data.batch.shape
    assert data.batch_ang_asc.shape[0] == nb_dates
    assert data.batch_ang_asc.shape[1] == nb_patches
    assert len(data.batch_ang_asc.shape) == 3
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = model.to(device=device)
    model.eval()
    with torch.no_grad():
        pred_mu_s1 = generate_s1_predictions(
            data, model, batch_size, patch_data, device
        )
    # We permute dimensions in order to have H,W,C,D
    # and we collapse the two to have H,W,D*C
    # Sizes after unpatchify might be greater than original
    # images because of padding. We crop to original dimensions
    pred_s1: np.ndarray = (
        (
            pred_mu_s1.permute(1, 2, 3, 0)
            .flatten(-2)[: patch_data.chunk_h, : patch_data.chunk_w, :]
            .contiguous()
        )
        .detach()
        .numpy()
    )
    return pred_s1


def process_s1_chunk(
    data: DataContainer, model: MMDCTorchModel, batch_size: int = 1
) -> ChunkReturnType:
    """Process the input S1 chunk. Build the batch and delegate to
    process_s1_batch"""
    # process common asc/desc dates together, then process separately,
    # return labels also
    latent_names = [
        f"latent_{i+1}" for i in range(model.config.auto_enc.s1_encoder.out_channels)
    ]
    asc_set = set(data.get_raw_dates()["Sentinel1_ASC_vv"])
    desc_set = set(data.get_raw_dates()["Sentinel1_DES_vv"])
    all_dates = sorted(list(asc_set.union(desc_set)))
    labels = build_labels(all_dates, "S1", latent_names)
    patch_margin = model.nb_cropped_hw
    batch, patch_data = build_s1_batch(data, patch_margin=model.nb_cropped_hw)
    batch_ang_asc, batch_ang_desc, _ = build_s1ang_batch(data, patch_margin)
    srtm, pd_srtm = build_srtm_batch(data, patch_margin)
    assert patch_data == pd_srtm
    w_c, pd_wc = build_wc_batch(data, patch_margin)
    assert patch_data == pd_wc
    s1_chunk = process_s1_batch(
        S1Batch(batch, batch_ang_asc, batch_ang_desc, srtm, w_c),
        batch_size,
        model,
        patch_data,
    )
    return ChunkReturnType(s1_chunk, labels)


def format_chunk(
    s1_chunk: ChunkReturnType, s2_chunk: ChunkReturnType
) -> ChunkReturnType:
    """Compose the S1 and S2 chunks to build the final chunk"""
    return ChunkReturnType(
        data=np.concatenate([s1_chunk.data, s2_chunk.data], axis=2),
        labels=s1_chunk.labels + s2_chunk.labels,
    )


def process_chunk(data: DataContainer, model: MMDCTorchModel) -> ChunkReturnType:
    """Given a iota2 chunk and a torch model, generate the chunk for
    the output by creating an MMDC data structure that can be fed to the
    model"""

    nb_s2_dates = len(data.get_raw_dates()["Sentinel2"])
    nb_s1_asc_dates = len(data.get_raw_dates()["Sentinel1_ASC_vv"])
    nb_s1_desc_dates = len(data.get_raw_dates()["Sentinel1_DES_vv"])
    latent_dim = model.config.auto_enc.s1_encoder.out_channels
    s2_out_chunk = process_s2_chunk(data, model)
    s1_out_chunk = process_s1_chunk(data, model)
    assert s2_out_chunk.data.shape[2] == nb_s2_dates * latent_dim
    assert s1_out_chunk.data.shape[2] >= nb_s1_asc_dates * latent_dim
    assert s1_out_chunk.data.shape[2] >= nb_s1_desc_dates * latent_dim
    return format_chunk(s1_out_chunk, s2_out_chunk)
