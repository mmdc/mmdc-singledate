#!/usr/bin/env python3
# copyright: (c) 2022 cesbio / Centre National d'Etudes Spatiales

"""
Module containing the utilities for the MMDC dataset exploration and visualization
"""

import logging
import os
import random
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import torch
from sensorsio.utils import rgb_render

from mmdc_singledate.datamodules.components.datamodule_utils import (
    create_tensors_path_set,
)
from mmdc_singledate.datamodules.datatypes import (
    MMDCDataStruct,
    MMDCMeteoData,
    MMDCS1Data,
    MMDCS2Data,
)


def read_tensor_file(
    input_dir: Path,
    tile: str,
    roi: int,
) -> MMDCDataStruct:
    """
    Eagerly load the tensors and build an MMDCDataStruct
    """

    assert input_dir.exists()

    (
        s2_set_filename,
        s2_masks_filename,
        s2_angles_filename,
        s1_set_filename,
        s1_set_valmasks,
        s1_angles_filename,
        meteo_dew_temp_filename,
        meteo_prec_filename,
        meteo_sol_rad_filename,
        meteo_temp_max_filename,
        meteo_temp_mean_filename,
        meteo_temp_min_filename,
        meteo_vap_press_filename,
        meteo_wind_speed_filename,
        dem_filename,
    ) = zip(
        *create_tensors_path_set(
            set_name="",
            path_to_exported_files=input_dir,
            roi_list=[roi],
            tile_list=[tile],
        ),
        strict=True,
    )

    return MMDCDataStruct(
        MMDCS2Data(
            torch.load(*s2_set_filename),
            torch.load(*s2_masks_filename),
            torch.load(*s2_angles_filename),
        ),
        MMDCS1Data(
            torch.load(*s1_set_filename),
            torch.load(*s1_set_valmasks),
            torch.load(*s1_angles_filename),
        ),
        MMDCMeteoData(
            torch.load(*meteo_dew_temp_filename),
            torch.load(*meteo_prec_filename),
            torch.load(*meteo_sol_rad_filename),
            torch.load(*meteo_temp_max_filename),
            torch.load(*meteo_temp_mean_filename),
            torch.load(*meteo_temp_min_filename),
            torch.load(*meteo_vap_press_filename),
            torch.load(*meteo_wind_speed_filename),
        ),
        torch.load(*dem_filename),
    )


def s2_row(
    row_col: tuple[int, int],
    data: MMDCDataStruct,
) -> None:
    """First row of the plot"""
    (figure_nb_rows, figure_nb_columns) = row_col

    s2_opt_render, _, _ = rgb_render(data.s2_data.s2_set.cpu().numpy(), bands=[2, 1, 0])
    s2_nir_render, _, _ = rgb_render(data.s2_data.s2_set.cpu().numpy(), bands=[6, 5, 4])
    s2_angles_render, _, _ = rgb_render(
        data.s2_data.s2_angles.cpu().numpy(),
        bands=[3, 2, 1],
        dmin=np.zeros(3),
        dmax=np.full(3, 1),
    )
    s2_masks_render, _, _ = rgb_render(
        data.s2_data.s2_masks.cpu().numpy(),
        bands=[0, 0, 0],
        dmin=np.zeros(3),
        dmax=np.ones(3),
    )

    ax_s2_opt = plt.subplot2grid(
        (figure_nb_rows, figure_nb_columns), (0, 0), colspan=2, rowspan=2
    )
    ax_s2_opt.imshow(s2_opt_render, interpolation="bicubic")
    ax_s2_opt.set_title("S2 RGB")

    ax_s2_nir = plt.subplot2grid(
        (figure_nb_rows, figure_nb_columns), (0, 2), colspan=2, rowspan=2
    )
    ax_s2_nir.imshow(s2_nir_render, interpolation="bicubic")
    ax_s2_nir.set_title("S2 NES")

    ax_s2_angles = plt.subplot2grid(
        (figure_nb_rows, figure_nb_columns), (0, 4), colspan=2, rowspan=2
    )
    ax_s2_angles.imshow(s2_angles_render, interpolation="bicubic", cmap="hsv")
    ax_s2_angles.set_title("S2 Angles")

    ax_s2_masks = plt.subplot2grid(
        (figure_nb_rows, figure_nb_columns), (0, 6), colspan=2, rowspan=2
    )
    ax_s2_masks.imshow(s2_masks_render, interpolation="bicubic")
    ax_s2_masks.set_title("S2 MASKS")


def s1_asc_row(
    row_col: tuple[int, int],
    data: MMDCDataStruct,
) -> None:
    """Second row of the plot"""
    (figure_nb_rows, figure_nb_columns) = row_col

    s1_asc_render, _, _ = rgb_render(
        data.s1_data.s1_set.cpu().numpy(),
        bands=[2, 1, 0],
    )
    s1_asc_angles_render, _, _ = rgb_render(
        data.s1_data.s1_angles[0].unsqueeze(0).cpu().numpy(), bands=[0, 0, 0]
    )
    s1_asc_masks_render, _, _ = rgb_render(
        data.s1_data.s1_valmasks.cpu().numpy(),
        bands=[0, 0, 0],
        dmin=np.zeros(3),
        dmax=np.ones(3),
    )

    ax_s1_asc = plt.subplot2grid(
        (figure_nb_rows, figure_nb_columns), (2, 1), colspan=2, rowspan=2
    )
    ax_s1_asc.imshow(s1_asc_render.astype(float), interpolation="bicubic")
    ax_s1_asc.set_title("S1 ASC")

    ax_s1_asc_angles = plt.subplot2grid(
        (figure_nb_rows, figure_nb_columns), (2, 3), colspan=2, rowspan=2
    )
    ax_s1_asc_angles.imshow(s1_asc_angles_render, interpolation="bicubic")
    ax_s1_asc_angles.set_title("S1 ASC ANG")

    ax_s1_asc_mask = plt.subplot2grid(
        (figure_nb_rows, figure_nb_columns), (2, 5), colspan=2, rowspan=2
    )
    ax_s1_asc_mask.imshow(s1_asc_masks_render, interpolation="bicubic")
    ax_s1_asc_mask.set_title("S1 ASC MASK")


def s1_desc_row(
    row_col: tuple[int, int],
    data: MMDCDataStruct,
) -> None:
    """Third row of the plot"""
    (figure_nb_rows, figure_nb_columns) = row_col
    s1_desc_render, _, _ = rgb_render(
        data.s1_data.s1_set.cpu().numpy(),
        bands=[5, 4, 3],
    )
    s1_desc_angles_render, _, _ = rgb_render(
        data.s1_data.s1_angles[1].unsqueeze(0).cpu().numpy(), bands=[0, 0, 0]
    )
    s1_desc_masks_render, _, _ = rgb_render(
        data.s1_data.s1_valmasks.cpu().numpy(),
        bands=[1, 1, 1],
        dmin=np.zeros(3),
        dmax=np.ones(3),
    )

    ax_s1_desc = plt.subplot2grid(
        (figure_nb_rows, figure_nb_columns), (4, 1), colspan=2, rowspan=2
    )
    ax_s1_desc.imshow(s1_desc_render.astype(float), interpolation="bicubic")
    ax_s1_desc.set_title("S1 DESC")

    ax_s1_desc_angles = plt.subplot2grid(
        (figure_nb_rows, figure_nb_columns), (4, 3), colspan=2, rowspan=2
    )
    ax_s1_desc_angles.imshow(s1_desc_angles_render, interpolation="bicubic")
    ax_s1_desc_angles.set_title("S1 DESC ANG")

    ax_s1_desc_mask = plt.subplot2grid(
        (figure_nb_rows, figure_nb_columns), (4, 5), colspan=2, rowspan=2
    )
    ax_s1_desc_mask.imshow(s1_desc_masks_render, interpolation="bicubic")
    ax_s1_desc_mask.set_title("S1 DESC MASK")


def dem_row(
    row_col: tuple[int, int],
    data: MMDCDataStruct,
) -> None:
    """Fourth row of the plot"""
    (figure_nb_rows, figure_nb_columns) = row_col
    dem_height_render, _, _ = rgb_render(data.dem[[0, 0, 0], ...].cpu().numpy())
    dem_slope_render, _, _ = rgb_render(
        np.arcsin(data.dem[[1, 1, 1], ...].cpu().numpy())
    )
    dem_aspect_render, _, _ = rgb_render(
        np.arcsin(data.dem[[3, 3, 3], ...].cpu().numpy())
    )

    ax_dem_height = plt.subplot2grid(
        (figure_nb_rows, figure_nb_columns), (6, 1), colspan=2, rowspan=2
    )
    ax_dem_height.imshow(dem_height_render, interpolation="bicubic")
    ax_dem_height.set_title("DEM Height")

    ax_dem_slope = plt.subplot2grid(
        (figure_nb_rows, figure_nb_columns), (6, 3), colspan=2, rowspan=2
    )
    ax_dem_slope.imshow(dem_slope_render, interpolation="bicubic")
    ax_dem_slope.set_title("DEM Slope")

    ax_dem_aspect = plt.subplot2grid(
        (figure_nb_rows, figure_nb_columns), (6, 5), colspan=2, rowspan=2
    )
    ax_dem_aspect.imshow(dem_aspect_render, interpolation="bicubic")
    ax_dem_aspect.set_title("DEM Aspect")


def meteo_row(
    row_col: tuple[int, int],
    data: MMDCDataStruct,
    meteo_day: int = 0,
    add_to_row: int = 0,
) -> None:
    """Last row of the plot"""
    (figure_nb_rows, figure_nb_columns) = row_col

    for enum, meteo in enumerate(list(data.meteo.__dict__.keys())):
        ax_meteo = plt.subplot2grid(
            (figure_nb_rows, figure_nb_columns),
            (8 + add_to_row, enum),
            colspan=1,
            rowspan=1,
        )
        ax_meteo.imshow(
            getattr(data.meteo, meteo).cpu().numpy()[meteo_day],
            cmap="Spectral",
            interpolation="bicubic",
        )
        ax_meteo.set_title(f"Meteo {meteo} d={meteo_day}")


def mmdc_visualization(
    data: MMDCDataStruct,
    export_path: Path,
    image_name: str,
) -> None:
    """
    Generate a plot of a MMDC patch

    return None
        but store the graph produced on disk
    """

    row_cols = (10, 8)
    plt.close()
    fig = plt.figure(figsize=(row_cols[0] * 2.5, row_cols[1] * 2.5))

    fig.suptitle("MMDC Dataset Inspection")

    s2_row(row_cols, data)
    s1_asc_row(row_cols, data)
    s1_desc_row(row_cols, data)
    dem_row(row_cols, data)
    meteo_row(row_cols, data, 0)
    meteo_row(row_cols, data, -1, add_to_row=1)

    export_label = os.path.join(export_path, image_name)
    fig.tight_layout(h_pad=2)
    fig.savefig(export_label)
    logging.info("exported: %s", export_label)


def export_visualization_graphs(
    input_directory: Path,
    input_tiles: list[str],
    patch_index: list[int] | None,
    nb_patches: int | None,
    export_path: Path,
) -> None:
    """
    Script entry point
    """
    # input path
    assert input_directory.exists()
    export_path.mkdir(exist_ok=True, parents=True)

    assert patch_index is not None or nb_patches is not None

    # loop over the tiles and rois
    # for export all the possible combinations
    for tile_roi in input_tiles:
        print("Tile, roi: %s", tile_roi)
        tile, roi = tile_roi.strip().split("_")

        # read the patch
        mmdc_batch = read_tensor_file(
            input_dir=input_directory,
            tile=tile,
            roi=int(roi),
        )
        # manage the patches to select
        max_patches = mmdc_batch.s2_data.s2_set.shape[0]
        if patch_index is None and nb_patches is not None:
            mode_random = True
            patch_index = random.sample(
                range(max_patches), min(nb_patches, max_patches)
            )
            logging.info(
                "patch_index : %s \n number of patches selected : %s",
                patch_index,
                len(patch_index),
            )
        else:
            mode_random = False
        assert patch_index is not None
        for patch_idx in patch_index:
            # make graph and export
            image_name = f"mmdc_ds_inspect_{tile}_{roi}_{patch_idx}.svg"
            mmdc_visualization(mmdc_batch[patch_idx], export_path, image_name)
        if mode_random:
            patch_index = None
