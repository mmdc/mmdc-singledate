""" Small functions to compute spectral indices """

import logging

import torch

all_spectral_indices = [
    "ndvi",
    "ndsi",
    "ndwi",
    "ndwig",
    "ndre1",
    "ndre2",
    "nssi",
    "rep",
    "psri",
    "sti",
]


def normalized_spectral_index(
    img_s2: torch.Tensor, idx_1: int, idx_2: int, dim: int = 1
) -> torch.Tensor:
    """Generic difference normalized spectra index with 2 bands"""
    eps = 1e-5
    s2_b1 = img_s2.select(dim, idx_1)
    s2_b2 = img_s2.select(dim, idx_2)
    return ((s2_b1 - s2_b2) / (s2_b1 + s2_b2 + eps)).unsqueeze(dim)


def rep_index(
    img_s2: torch.Tensor,
    bands: tuple[int, int, int, int],
    dim: int = 1,
) -> torch.Tensor:
    """Sentinel-2 REP index"""
    (
        idx_b4,
        idx_b5,
        idx_b6,
        idx_b7,
    ) = bands
    eps = 1e-5
    s2_b4 = img_s2.select(dim, idx_b4)
    s2_b5 = img_s2.select(dim, idx_b5)
    s2_b6 = img_s2.select(dim, idx_b6)
    s2_b7 = img_s2.select(dim, idx_b7)
    return (705 + 35 * ((s2_b4 + s2_b7) / 2 - s2_b5) / (s2_b6 + s2_b5 + eps)).unsqueeze(
        dim
    )


def psri_index(
    img_s2: torch.Tensor, idx_blue: int, idx_b4: int, idx_b6: int, dim: int = 1
) -> torch.Tensor:
    """Sentinel-2 PSRI"""
    eps = 1e-5
    s2_b4 = img_s2.select(dim, idx_b4)
    s2_b6 = img_s2.select(dim, idx_b6)
    s2_blue = img_s2.select(dim, idx_blue)
    return ((s2_b4 - s2_blue) / (s2_b6 + eps)).unsqueeze(dim)


def sti_index(
    img_s2: torch.Tensor, idx_swir1: int, idx_swir2: int, dim: int = 1
) -> torch.Tensor:
    """Sentinel-2 STI (SWIR ratio)"""
    eps = 1e-5
    s2_swir1 = img_s2.select(dim, idx_swir1)
    s2_swir2 = img_s2.select(dim, idx_swir2)
    return (s2_swir1 / (s2_swir2 + eps)).unsqueeze(dim)


def add_spectral_indices(
    img_s2: torch.Tensor,
    indices: list[str],
    dim: int = 1,
    logger: logging.Logger | None = None,
) -> torch.Tensor:
    """Add a list of spectral indices to a S2 spectral bands tensor"""
    if logger is not None:
        logger.info(f"Adding spectral indices: {indices}")

    idx_blue = 0
    idx_green = 1
    idx_red = 2
    idx_b5 = 3
    idx_b6 = 4
    idx_b7 = 5
    idx_nir = 6
    # idx_b8a = 7
    idx_swir1 = 8
    idx_swir2 = 9
    if "ndvi" in indices:
        img_s2 = torch.cat(
            (
                img_s2,
                normalized_spectral_index(img_s2, idx_nir, idx_red, dim=dim),
            ),
            dim,
        )
    if "ndsi" in indices:
        img_s2 = torch.cat(
            (
                img_s2,
                normalized_spectral_index(img_s2, idx_swir1, idx_green, dim=dim),
            ),
            dim,
        )
    if "ndwi" in indices:
        img_s2 = torch.cat(
            (
                img_s2,
                normalized_spectral_index(img_s2, idx_nir, idx_swir1, dim=dim),
            ),
            dim,
        )
    if "ndwig" in indices:
        img_s2 = torch.cat(
            (
                img_s2,
                normalized_spectral_index(img_s2, idx_green, idx_nir, dim=dim),
            ),
            dim,
        )
    if "ndre1" in indices:
        img_s2 = torch.cat(
            (
                img_s2,
                normalized_spectral_index(img_s2, idx_b6, idx_b5, dim=dim),
            ),
            dim,
        )
    if "ndre2" in indices:
        img_s2 = torch.cat(
            (
                img_s2,
                normalized_spectral_index(img_s2, idx_b7, idx_b5, dim=dim),
            ),
            dim,
        )
    if "nssi" in indices:
        img_s2 = torch.cat(
            (
                img_s2,
                normalized_spectral_index(img_s2, idx_nir, idx_b7, dim=dim),
            ),
            dim,
        )
    if "rep" in indices:
        img_s2 = torch.cat(
            (
                img_s2,
                rep_index(img_s2, (idx_red, idx_b5, idx_b6, idx_b7), dim=dim),
            ),
            dim,
        )
    if "psri" in indices:
        img_s2 = torch.cat(
            (img_s2, psri_index(img_s2, idx_blue, idx_red, idx_b6, dim=dim)),
            dim,
        )
    if "sti" in indices:
        img_s2 = torch.cat(
            (img_s2, sti_index(img_s2, idx_swir1, idx_swir2, dim=dim)), dim
        )
    return img_s2
