""" Utilities for training the models """
import logging
from collections.abc import Sequence

import torch

# Configure logging
NUMERIC_LEVEL = getattr(logging, "INFO", None)
logging.basicConfig(
    level=NUMERIC_LEVEL, format="%(asctime)-15s %(levelname)s: %(message)s"
)
logger = logging.getLogger(__name__)


def reshape_for_standardization(
    data: torch.Tensor, data_shape: Sequence
) -> torch.Tensor:
    """Deal with images vs vectors (no fourth dimension)"""
    if len(data_shape) == 3:
        data = data[:, None, None]

    if len(data_shape) == 4:
        if data.shape[-1] != data_shape[1]:
            data = data.repeat(1, int(data_shape[1] / data.shape[-1]))
        data = data.reshape(1, data_shape[1], 1, 1)
    return data


def zero_chans(
    x_ref: torch.Tensor, x_vals: torch.Tensor, threshold: float
) -> torch.Tensor:
    """Set to zero the pixels for which the sum of the channels is lower than a
    threshold"""
    mask_idx = (x_ref.sum(dim=1, keepdim=True) < threshold).broadcast_to(x_vals.shape)
    x_vals[mask_idx] = 0
    return x_vals


def standardize_data(
    data: torch.Tensor,
    shift: torch.Tensor,
    scale: torch.Tensor,
    coeff: float = 1,
    null_value: float | None = None,
) -> torch.Tensor:
    """
    Standardize 1d or 3d image tensor data by removing a shift and then scaling
    :param data: Tensor of data with shape [C] or [d1,...,dk,C,H,W]
    :param shfit: 1D tensor with shifting data
    :param scale: 1D tensor with scaling data
    :param coeff: Coeff used when scaling data
    :param null_value: Set to zero the pixels for which the sum of the channels is
                       lower than this value
    """

    shift = reshape_for_standardization(shift, data.shape)
    scale = reshape_for_standardization(scale, data.shape)
    data_stand = (data - shift) / (coeff * scale)
    if null_value is not None:
        data_stand = zero_chans(data, data_stand, null_value)
    # We replace all nans by zeros after standardization
    return data_stand.nan_to_num()


def unstandardize_data(
    data: torch.Tensor,
    shift: torch.Tensor,
    scale: torch.Tensor,
    coeff: float = 1,
) -> torch.Tensor:
    """
    Standardize 1d or 3d image tensor data by removing a shift and then scaling
    :param data: Tensor of data with shape [C] or [d1,...,dk,C,H,W]
    :param shfit: 1D tensor with shifting data
    :param scale: 1D tensor with scaling data
    :param coeff: Coeff used when scaling data
    """

    shift = reshape_for_standardization(shift, data.shape)
    scale = reshape_for_standardization(scale, data.shape)
    res = data * coeff * scale + shift

    return res
