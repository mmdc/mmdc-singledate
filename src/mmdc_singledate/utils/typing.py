""" Typing utilities """

from typing_extensions import Never


def assert_never(arg: Never) -> Never:
    """To be used to ensure that code is unreachable"""
    raise AssertionError("Expected code to be unreachable")
