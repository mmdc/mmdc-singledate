#!/usr/bin/env python3
# Copyright (c) CESBIO / Centre National d'Etudes Spatiales
"""
Utility functions for evaluate the project
Including the data, the models, etc
"""

import glob
import logging
import random
from pathlib import Path
from typing import Literal

import geopandas as gpd
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import rasterio as rio

# Configure logging
NUMERIC_LEVEL = getattr(logging, "INFO", None)
logging.basicConfig(
    level=NUMERIC_LEVEL, format="%(asctime)-15s %(levelname)s: %(message)s"
)
logger = logging.getLogger(__name__)

# define a random seed for reproducibility purpuse
random.seed(543)


def nb_tiles_to_select_by_percentage(
    theia_tiles_filepath: str, percentage_to_select: float
) -> int:
    """
    Compute a nb of tiles to select based
    in a percentage of the full data archive
    """
    nb_theia_tiles = len(gpd.read_file(theia_tiles_filepath))
    nb_tiles_to_select = int(nb_theia_tiles * percentage_to_select)

    return nb_tiles_to_select


def random_tiles_selection(
    theia_tiles_filepath: str, nb_selected_tiles: int, export_path: str
) -> None:
    """Select a number of tiles for generate datasamples and export to
    GPKG and CSV

    """
    # read the data
    theia_tiles = gpd.read_file(theia_tiles_filepath)

    # random sample tiles
    tiles_sample_selection = theia_tiles.sample(nb_selected_tiles)

    # append the years

    years_beg = ["2017-01-01", "2018-01-01", "2019-01-01"]
    years_end = ["2017-12-31", "2018-12-31", "2019-12-31"]

    tiles_sample_selection["date_beg"] = np.resize(
        years_beg, len(tiles_sample_selection)
    )
    tiles_sample_selection["date_end"] = np.resize(
        years_end, len(tiles_sample_selection)
    )

    # export to geopackage

    tiles_sample_selection.to_file(
        filename=f"{export_path}/tilenames.gpkg", driver="GPKG"
    )

    # export to txt
    tiles_samples_select_df = pd.DataFrame(tiles_sample_selection)

    tiles_samples_select_df.to_csv(f"{export_path}/tilenames.txt", sep=" ")


# Check the data generation


def verify_available_tiles(
    dataset_path: Path, export_path: Path, filename: str
) -> None:
    """
    Verify the available tiles for sampling
    And export a geo file with the available tiles

    :param: dataset_path : Path to the dataset
    """
    tiles = []
    # list the tiles
    for tile_dir in glob.glob(f"{dataset_path}/T*"):
        s1asc_files = sorted(glob.glob(f"{tile_dir}/S1_ASC*.txt"))
        s1desc_files = sorted(glob.glob(f"{tile_dir}/S1_DESC*.txt"))
        s2_files = sorted(glob.glob(f"{tile_dir}/S2L2A*.txt"))

        if len(s1asc_files) == len(s1desc_files) == len(s2_files):
            # verify the tiles have the txt files
            tiles.append(tile_dir.split("/")[-1])
    # read the geofile
    geofile = gpd.read_file(f"{dataset_path}/tilenames.gpkg")
    geofile = geofile.set_index("Name")

    # filter the tilenames sampling file with the actual tiles with data
    geofile_update = geofile[geofile.index.isin(tiles)].reset_index()
    # export the geopackage file
    geofile_update.to_file(filename=f"{export_path}/{filename}.gpkg", driver="GPKG")

    # export to txt
    file_update = pd.DataFrame(geofile_update)
    file_update.to_csv(
        f"{export_path}/{filename}.txt", sep=" ", header=False, index=False
    )


def verify_reading_tiles(
    dataset_path: Path, export_path: Path, filename: Path, tiles: list[str]
) -> None:
    """
    Verify the available tiles for sampling
    And export a geo file with the available tiles

    :param: dataset_path : Path to the dataset
    """
    dataset_path = Path(dataset_path)
    geofile = gpd.read_file(f"{dataset_path}/tilenames.gpkg")
    geofile = geofile.set_index("Name")
    geofile = gpd.read_file(Path(f"{export_path}/tilenames.gpkg"))

    # filter the tilenames sampling file with the actual tiles with data
    geofile_update = geofile[geofile.index.isin(tiles)].reset_index()
    # export the geopackage file
    if Path(f"{export_path}/{filename}.gpkg").is_file():
        logger.info("File already exist, no exporting")
    else:
        geofile_update.to_file(filename=f"{export_path}/{filename}.gpkg", driver="GPKG")

    # export to txt
    file_update = pd.DataFrame(geofile_update)
    file_update.to_csv(
        f"{export_path}/{filename}.txt", sep=" ", header=False, index=False
    )


def read_csv_training_log(log_path: Path) -> tuple[pd.DataFrame, pd.DataFrame]:
    """Retrieve train and validation loss evolutions from CSV log"""
    logger.info("Retrieving metrics from %s", log_path.name)
    metrics_csv = glob.glob(f"{log_path.absolute()}/*.csv")
    if not metrics_csv:
        raise RuntimeError(f"No csv file found in {log_path.name}")
    dataframe_filepath = log_path / metrics_csv[0]
    log_dataframe = pd.read_csv(dataframe_filepath)
    train_curve = log_dataframe.loc[:, ["epoch", "step", "train/loss"]].dropna()
    val_curve = log_dataframe.loc[:, ["epoch", "step", "val/loss"]].dropna()
    return (train_curve, val_curve)


def plot_training_evolution(
    train_evolution: pd.DataFrame, val_evolution: pd.DataFrame, discard: int
) -> tuple[plt.Figure, plt.Axes]:
    """Plot the val and train losses evolution"""
    fig, axes = plt.subplots(ncols=3, figsize=(20, 5), constrained_layout=True)

    axes[0].plot(
        train_evolution.loc[discard:, "epoch"],
        train_evolution.loc[discard:, "train/loss"],
        label="Training loss",
        color="tab:blue",
        alpha=0.75,
    )

    axes[0].set_title(" Training running losses")

    axes[0].set_xlabel("Number of epochs")
    axes[0].set_ylabel("Error")
    axes[0].grid(True)
    axes[0].legend()
    axes[1].plot(
        val_evolution.loc[discard:, "epoch"],
        val_evolution.loc[discard:, "val/loss"],
        label="Validation loss",
        color="tab:orange",
        alpha=0.75,
    )
    axes[1].set_title(" Validation running losses")
    axes[1].set_xlabel("Number of epochs")
    axes[1].set_ylabel("Error")
    axes[1].grid(True)
    axes[1].legend()
    axes[2].plot(
        train_evolution.loc[discard:, "epoch"],
        train_evolution.loc[discard:, "train/loss"],
        label="Training loss",
        color="tab:blue",
        alpha=0.75,
    )
    axes[2].plot(
        val_evolution.loc[discard:, "epoch"],
        val_evolution.loc[discard:, "val/loss"],
        label="Validation loss",
        color="tab:orange",
        alpha=0.75,
    )
    axes[2].set_title(" Total loss")
    axes[2].set_xlabel("Number of epochs")
    axes[2].set_ylabel("Error")
    axes[2].grid(True)
    axes[2].legend()
    fig.suptitle("Training Curves")
    # fig.show()
    return fig, axes


AuxDataType = Literal["srtm", "worldclim"]


def read_aux_dataset(data_path: Path, dataset_type: AuxDataType) -> np.ndarray:
    """
    "srtm" or "worldclim":
    """
    logger.info("Starting data reading")
    # list the files
    logger.info("Reading SRTM or Worldclim")
    data_list = sorted(glob.glob(f"{data_path}/T*/{dataset_type}*"))

    # read the data
    data_raster = []
    for data in enumerate(data_list):
        with rio.open(data) as data_file:
            raster_file = data_file.read()
            data_raster.append(raster_file)

        logger.info("Stacking the data")

    # data stack with for [, C, H, W]
    raster_stack: np.ndarray = np.stack(data_raster)

    return raster_stack


SatDataType = Literal["S1_ASC*.txt", "S1_DESC*.txt", "S2L2A*.txt"]


def read_sentinel_dataset(data_path: Path, dataset_type: SatDataType) -> np.ndarray:
    """
    "S1_ASC*.txt" or "S1_DESC*.txt" or "S2L2A*.txt":
    """
    logger.info("Reading Sentinel data")
    # read s1 and s2 data
    list_sentinel_rois = sorted(
        [f for f in glob.glob(f"{data_path}/T*/{dataset_type}") if "old" not in f]
    )
    sentinel_metadata = [pd.read_csv(j, sep=" ").iloc[:, 1] for j in list_sentinel_rois]
    data_list = list(pd.concat(sentinel_metadata, axis=0))

    # read the data
    data_raster = []
    for data in enumerate(data_list):
        with rio.open(data_path / Path(str(data))) as data_file:
            raster_file = data_file.read()
            data_raster.append(raster_file)

    logger.info("Stacking the data")
    # data stack with for [, C, H, W]
    raster_stack: np.ndarray = np.stack(data_raster)

    return raster_stack


# Train and Loss Curves

# Histogram


def make_data_histogram(
    raster_stack: np.ndarray,
    dataset_type: str,
    bands_label: list[str],
    export_path: str,
) -> None:
    """
    create and export a histogram fora data conjuntion
    """
    # make the graph

    fig, axes = plt.subplots(
        nrows=raster_stack.shape[1],
        ncols=1,
        sharex=False,
        sharey=False,
        figsize=(15, 15),
    )

    for i in range(raster_stack.shape[1]):
        counts, bins = np.histogram(raster_stack[:, i, :, :].flatten())
        axes[i].hist(bins[:-1], bins, weights=counts)
        axes[i].set_title(f"{bands_label[i]}")
    fig.savefig(f"{export_path}/{dataset_type}_histogram.png")

    logger.info("Succesfully Exported !!!")


def plot_s1_angles(
    s1_angles: np.ndarray, orbit_type: str
) -> tuple[plt.Figure, plt.Axes]:
    """
    Make a Polar graph for the S1 angles
    """

    rho = np.array([2, 2, 2])
    theta = s1_angles

    fig = plt.figure(figsize=(5, 5))
    axis = plt.subplot(111, projection="polar")

    # desactivate labels
    axis.set_yticklabels([])
    legend_labels = [
        f"{orbit_type} angle 0 : {np.rad2deg(theta[0]):,.2f}",
        f"{orbit_type} angle 1 : {np.rad2deg(theta[1]):,.2f}",
        f"{orbit_type} angle 2 : {np.rad2deg(theta[2]):,.2f}",
    ]

    for theta_el, rho_el in zip(theta, rho, strict=True):
        axis.plot([0, theta_el], [0, rho_el])

    plt.legend(legend_labels, loc="best", bbox_to_anchor=(0.75, 0.0, 0.75, 0.75))
    plt.title(f"{orbit_type} S1  Angles")

    return fig, axis
