"""
Constants for the MMDC callback modules
"""

LABELS_LIST = [
    [
        "Input S1 asc",
        "Input S1 desc ",
        "Input S2 rgb ",
        "Input S2 nes ",
    ],
    ["Pred S1 asc", "Pred S1 desc ", "Pred S2 rgb ", "Pred S2 nes "],
    [
        "Pred Cross S1 asc",
        "Pred Cross S1 desc ",
        "Pred Cross S2 rgb ",
        "Pred Cross S2 nes ",
    ],
    [
        "Mu lat S1 Component 0",
        "Mu lat S1 Component 1",
        "Mu lat S1 Component 2",
        "Mu lat S1 Component 3",
    ],
    [
        "Mu lat S2 Component 0",
        "Mu lat S2 Component 1",
        "Mu lat S2 Component 2",
        "Mu lat S2 Component 3",
    ],
    [
        "VarLog lat S1 Component 0",
        "VarLog lat S1 Component 1",
        "VarLog lat S1 Component 2",
        "VarLog lat S1 Component 3",
    ],
    [
        "Varlog lat S2 Component 0",
        "Varlog lat S2 Component 1",
        "Varlog lat S2 Component 2",
        "Varlog lat S2 Component 3",
    ],
]

LABELS_LIST_EXPERTS = [
    [
        "Input S1 asc",
        "Input S1 desc ",
        "Input S2 rgb ",
        "Input S2 nes ",
    ],
    ["Pred S1 asc ", "Pred S1 desc ", "Pred S2 rgb ", "Pred S2 nes "],
    [
        "Pred S1 asc S1Lat ",
        "Pred S1 desc S1Lat ",
        "Pred S2 rgb S2Lat ",
        "Pred S2 nes S2Lat ",
    ],
    [
        "Pred S1 asc S2Lat ",
        "Pred S1 desc S2Lat ",
        "Pred S2 rgb S1Lat ",
        "Pred S2 nes S1Lat ",
    ],
    [
        "Mu lat Component 0",
        "Mu lat Component 1",
        "Mu lat Component 2",
        "Mu lat Component 3",
    ],
    [
        "VarLog lat Component 0",
        "VarLog lat Component 1",
        "VarLog lat Component 2",
        "VarLog lat Component 3",
    ],
]

KEYS_S1 = (
    "vv_asc",
    "vh_asc",
    "ratio_asc",
    "vv_desc",
    "vh_desc",
    "ratio_desc",
)

KEYS_S2 = (
    "b2",
    "b3",
    "b4",
    "b8",
    "b5",
    "b6",
    "b7",
    "b8a",
    "b11",
    "b12",
)
