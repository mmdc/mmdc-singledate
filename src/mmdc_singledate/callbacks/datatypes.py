""" Types and classes for the MMDC callback modules """
from dataclasses import dataclass

import numpy as np
import torch

from mmdc_singledate.models.datatypes import S1S2VAEAuxiliaryEmbeddings


@dataclass
class SampleInfo:
    """Information about the sample to be displayed"""

    batch_idx: int
    batch_size: int
    patch_margin: int
    current_epoch: int


@dataclass
class AuxData:
    """Auxiliary data (angles, DEM and meteo embeddigs)"""

    s2_a: torch.Tensor
    s1_a: torch.Tensor
    dem_x: torch.Tensor
    embs: S1S2VAEAuxiliaryEmbeddings


@dataclass
class DEM:
    """The 3 components of a DEM"""

    height: np.ndarray
    slope: np.ndarray
    aspect: np.ndarray
