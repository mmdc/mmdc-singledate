#!/usr/bin/env python3
# Copyright: (c) 2022 CESBIO / Centre National d'Etudes Spatiales
""" Lightning image callbacks """
import logging
from dataclasses import dataclass
from pathlib import Path
from typing import Any

import matplotlib.pyplot as plt
import numpy as np
import pytorch_lightning as pl
import torch
from pytorch_lightning.callbacks import Callback
from sensorsio.utils import rgb_render

from mmdc_singledate.callbacks.constants import (
    KEYS_S1,
    KEYS_S2,
    LABELS_LIST,
    LABELS_LIST_EXPERTS,
)
from mmdc_singledate.callbacks.datatypes import DEM, AuxData, SampleInfo

from ..datamodules.mmdc_datamodule import MMDCBatch, destructure_batch
from ..models.datatypes import (
    MMDCForwardType,
    S1FowardType,
    S1S2VAEOutput,
    S1S2VAEOutputPreExperts,
    S1S2VAEOutputPreExpertsMuLatent,
    S2FowardType,
)
from ..models.lightning.base import MMDCBaseLitModule
from ..utils.filters import despeckle_batch

# Configure logging
NUMERIC_LEVEL = getattr(logging, "INFO", None)
logging.basicConfig(
    level=NUMERIC_LEVEL, format="%(asctime)-15s %(levelname)s: %(message)s"
)

logger = logging.getLogger(__name__)


@dataclass
class CorrelationData:
    """Original data, prediction and cross-reconstruction for scatter plots"""

    data: dict[str, np.array]
    pred: dict[str, np.array]
    cross: dict[str, np.array] = None


class MMDCAECallback(Callback):
    """
    Callback to inspect the reconstruction image
    """

    def __init__(
        self,
        save_dir: str,
        n_samples: int = 3,
        normalize: bool = False,
        value_range: tuple[float, float] = (-1.0, 1.0),
    ):
        self.save_dir = save_dir
        self.n_samples = n_samples
        self.normalize = normalize
        self.value_range = value_range

        # labels list
        self.labels_list = LABELS_LIST
        self.keys_s1 = KEYS_S1
        self.keys_s2 = KEYS_S2

    def prepare_s1_s2(
        self,
        s1_data: torch.Tensor,
        s2_data: torch.Tensor,
        samp_idx: int,
        patch_margin: int,
    ) -> tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]:
        """Prepare the patches for the S1 and S2 data"""
        s1_asc = s1_data[
            samp_idx,
            :3,
            patch_margin:-patch_margin,
            patch_margin:-patch_margin,
        ]
        s1_desc = s1_data[
            samp_idx,
            3:,
            patch_margin:-patch_margin,
            patch_margin:-patch_margin,
        ]
        s2_rgb = s2_data[
            samp_idx,
            [2, 1, 0],
            patch_margin:-patch_margin,
            patch_margin:-patch_margin,
        ]
        s2_nes = s2_data[
            samp_idx,
            [3, 5, 8],
            patch_margin:-patch_margin,
            patch_margin:-patch_margin,
        ]
        return s1_asc, s1_desc, s2_rgb, s2_nes

    def prepare_lat(
        self, pred: torch.Tensor, samp_idx: int, patch_margin: int
    ) -> tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]:
        """Prepare latents for display"""
        lat_s1mu_0 = pred[
            samp_idx,
            [0, 0, 0],
            patch_margin:-patch_margin,
            patch_margin:-patch_margin,
        ]
        lat_s1mu_1 = pred[
            samp_idx,
            [1, 1, 1],
            patch_margin:-patch_margin,
            patch_margin:-patch_margin,
        ]
        lat_s1mu_2 = pred[
            samp_idx,
            [2, 2, 2],
            patch_margin:-patch_margin,
            patch_margin:-patch_margin,
        ]
        lat_s1mu_3 = pred[
            samp_idx,
            [3, 3, 3],
            patch_margin:-patch_margin,
            patch_margin:-patch_margin,
        ]
        return lat_s1mu_0, lat_s1mu_1, lat_s1mu_2, lat_s1mu_3

    def prepare_patches_grid(
        self,
        samp_idx: int,
        sent_data: tuple[torch.Tensor, torch.Tensor],
        pred: S1S2VAEOutput,
        patch_margin: int,
    ) -> tuple[
        tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor],
        tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor],
        tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor],
        tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor],
        tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor],
        tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor],
        tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor],
    ]:
        """Generate the patches for the visualization grid
        Select the appropriate channels and crop with the patch margin.
        """

        (s1_data, s2_data) = sent_data
        inputs = self.prepare_s1_s2(s1_data, s2_data, samp_idx, patch_margin)

        preds = self.prepare_s1_s2(
            pred.encs1_mu_decs1, pred.encs2_mu_decs2, samp_idx, patch_margin
        )

        preds_cross = self.prepare_s1_s2(
            pred.encs2_mu_decs1, pred.encs1_mu_decs2, samp_idx, patch_margin
        )

        lat_s1_mu = self.prepare_lat(pred.latent.sen1.mean, samp_idx, patch_margin)

        lat_s2_mu = self.prepare_lat(pred.latent.sen2.mean, samp_idx, patch_margin)

        lat_s1_logvar = self.prepare_lat(
            pred.latent.sen1.logvar, samp_idx, patch_margin
        )

        lat_s2_logvar = self.prepare_lat(
            pred.latent.sen2.logvar, samp_idx, patch_margin
        )

        return (
            inputs,
            preds,
            preds_cross,
            lat_s1_mu,
            lat_s2_mu,
            lat_s1_logvar,
            lat_s2_logvar,
        )

    def render_row(
        self,
        imcol: tuple[torch.Tensor, ...],
        j: int,
        d_min_max: tuple[tuple[float, float], ...],
        last_rgb_row: int = 3,
    ) -> tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
        """Render a row of the image grid"""
        # calculate render the reconstruction use the same
        # dmin and dmax as the input
        if 0 < j < last_rgb_row:
            render_j0, _, _ = rgb_render(
                imcol[0].cpu().detach().numpy(),
                dmin=d_min_max[0][0],
                dmax=d_min_max[0][1],
            )
            render_j1, _, _ = rgb_render(
                imcol[1].cpu().detach().numpy(),
                dmin=d_min_max[1][0],
                dmax=d_min_max[1][1],
            )
            render_j2, _, _ = rgb_render(
                imcol[2].cpu().detach().numpy(),
                dmin=d_min_max[2][0],
                dmax=d_min_max[2][1],
            )
            render_j3, _, _ = rgb_render(
                imcol[3].cpu().detach().numpy(),
                dmin=d_min_max[3][0],
                dmax=d_min_max[3][1],
            )
        else:
            render_j0, _, _ = rgb_render(imcol[0].cpu().detach().numpy())
            render_j1, _, _ = rgb_render(imcol[1].cpu().detach().numpy())
            render_j2, _, _ = rgb_render(imcol[2].cpu().detach().numpy())
            render_j3, _, _ = rgb_render(imcol[3].cpu().detach().numpy())

        return render_j0, render_j1, render_j2, render_j3

    def generate_figure_for_sample(
        self,
        img_columns: list[tuple[torch.Tensor, ...]],
        d_min_max: tuple[tuple[np.array, np.array], ...],
    ) -> plt.Figure:
        """Generate figure for sample"""
        plt.close()
        fig, axes = plt.subplots(
            nrows=len(img_columns),
            ncols=4,
            sharex=True,
            sharey=True,
            figsize=((len(img_columns) + 1) * 2.5, 16),
        )
        fig.suptitle("Reconstructions Inspection", fontsize=20)

        plt.subplots_adjust(
            left=0.1, bottom=0.1, right=0.9, top=0.9, wspace=0.4, hspace=0.4
        )

        for j, imcol in enumerate(img_columns):
            (render_j0, render_j1, render_j2, render_j3) = self.render_row(
                imcol, j, d_min_max
            )

            # apply render
            axes[j, 0].imshow(render_j0, interpolation="bicubic")
            axes[j, 0].set_title(self.labels_list[j][0])
            axes[j, 1].imshow(render_j1, interpolation="bicubic")
            axes[j, 1].set_title(self.labels_list[j][1])
            axes[j, 2].imshow(render_j2, interpolation="bicubic")
            axes[j, 2].set_title(self.labels_list[j][2])
            axes[j, 3].imshow(render_j3, interpolation="bicubic")
            axes[j, 3].set_title(self.labels_list[j][3])
        return fig

    def save_image_for_sample(
        self,
        img_columns: list[
            tuple[
                torch.Tensor,
                torch.Tensor,
                torch.Tensor,
                torch.Tensor,
            ]
        ],
        image_name: Path,
    ) -> None:
        """Save the image grid for a sample"""
        # apply rgb render to the input to compute the min and max
        min_max = []
        for col in range(len(img_columns[0])):
            if col in (2, 3) or (img_columns[0][col].sum() != 0 and col in (0, 1)):
                _, d_min, d_max = rgb_render(img_columns[0][col].cpu().detach().numpy())
            else:  # in case an orbit is missing we use preds for normalization
                _, d_min, d_max = rgb_render(img_columns[1][col].cpu().detach().numpy())
            min_max.append((d_min, d_max))

        fig = self.generate_figure_for_sample(
            img_columns,
            tuple(min_max),
        )

        fig.savefig(image_name)

    def save_image_grid(
        self,
        sen_data: tuple[torch.Tensor, torch.Tensor],
        pred: S1S2VAEOutput | S1S2VAEOutputPreExperts,
        sample: SampleInfo,
    ) -> None:
        """Generate the matplotlib figure and save it to a file"""
        for samp_idx in range(
            0,
            sample.batch_size,
            sample.batch_size // min(sample.batch_size, self.n_samples),
        ):
            img_columns = list(
                self.prepare_patches_grid(samp_idx, sen_data, pred, sample.patch_margin)
            )
            # export name
            image_basename = (
                f"MMDC_val_ep_{sample.current_epoch:03}_samp_"
                f"{samp_idx:03}_{sample.batch_idx}"
            )
            image_name = Path(f"{self.save_dir}/{image_basename}.png")
            if not image_name.is_file() or sample.current_epoch == 0:
                self.save_image_for_sample(img_columns, image_name)

    @staticmethod
    def extract_crop(
        data: torch.Tensor, idx: int, bands: list[int] | int, margin: int
    ) -> torch.Tensor:
        """Return a cropped patch with a band subset from the whole batch"""
        return data[
            idx,
            bands,
            margin:-margin,
            margin:-margin,
        ]

    def to_flat_vector(
        self, data: torch.Tensor, idx: int, bands: list[int] | int, margin: int
    ) -> np.ndarray:
        """Select a patch from the batch, crop the center, select some
        bands and return a 1D vector with the pixels"""
        result: np.ndarray = (
            self.extract_crop(data, idx, bands, margin).flatten().cpu().detach().numpy()
        )
        return result

    def save_latent_scatterplots(
        self,
        pred: S1S2VAEOutput | S1S2VAEOutputPreExperts,
        samp_idx: int,
        sample: SampleInfo,
    ) -> None:
        """Save the PNG image of the scatterplots of the latent space of
        a sample of the batch"""
        image_basename = (
            f"MMDC_val_ep_{sample.current_epoch:03}_scat_latent_"
            f"{samp_idx:03}_{sample.batch_idx}"
        )
        image_name = Path(f"{self.save_dir}/{image_basename}.png")
        if not image_name.is_file() or sample.current_epoch == 0:
            lat_s1 = [
                self.to_flat_vector(
                    pred.latent.sen1.mean, samp_idx, i, sample.patch_margin
                )
                for i in range(4)
            ]
            lat_s2 = [
                self.to_flat_vector(
                    pred.latent.sen2.mean, samp_idx, i, sample.patch_margin
                )
                for i in range(4)
            ]

            plt.close()
            fig, axes = plt.subplots(
                nrows=2, ncols=2, sharex=False, sharey=False, figsize=(20, 16)
            )
            fig.suptitle(f"Latent {sample.current_epoch = }", fontsize=20)
            for i, axis in enumerate(axes.flatten()):
                axis.set_title(f"Latent {i}")
                axis.set_xlabel("S1")
                axis.set_ylabel("S2")
                axis.text(
                    0.01,
                    0.99,
                    f"Corr={np.round(np.corrcoef(lat_s1[i], lat_s2[i]), 2)[0, 1]}",
                    ha="left",
                    va="top",
                    transform=axis.transAxes,
                )
                axis.scatter(lat_s1[i], lat_s2[i])
                axis.plot(lat_s1[i], lat_s1[i])
            fig.savefig(image_name)

    def plot_scatter(
        self,
        axis: plt.axis,
        label: str,
        x_data: np.ndarray,
        y_data: np.ndarray,
    ) -> None:
        """Scatter plot with regression line and label"""
        axis.set_title(label)
        axis.scatter(x_data, y_data)
        axis.plot(x_data, x_data)

    def save_s1_scatterplots(
        self,
        s1_data: torch.Tensor,
        pred: S1S2VAEOutput,
        samp_idx: int,
        sample: SampleInfo,
    ) -> None:
        """Save scatterplots for S1 reconstruction vs prediction"""
        image_basename = (
            f"MMDC_val_ep_{sample.current_epoch:03}_scat_s1_"
            f"{samp_idx:03}_{sample.batch_idx}"
        )
        image_name = Path(f"{self.save_dir}/{image_basename}.png")
        if not image_name.is_file() or sample.current_epoch == 0:
            s1d = {
                key: self.to_flat_vector(s1_data, samp_idx, idx, sample.patch_margin)
                for idx, key in enumerate(self.keys_s1)
            }

            s1p = {
                key: self.to_flat_vector(
                    pred.encs1_mu_decs1, samp_idx, idx, sample.patch_margin
                )
                for idx, key in enumerate(self.keys_s1)
            }

            s1c = {
                key: self.to_flat_vector(
                    pred.encs2_mu_decs1, samp_idx, idx, sample.patch_margin
                )
                for idx, key in enumerate(self.keys_s1)
            }

            self.plot_correlation(
                image_name, sample.current_epoch, CorrelationData(s1d, s1p, s1c)
            )

    def save_s2_scatterplots(
        self,
        s2_data: torch.Tensor,
        pred: S1S2VAEOutput,
        samp_idx: int,
        sample: SampleInfo,
    ) -> None:
        """Save S2 scatterplots for preditions for a sample"""
        image_basename = (
            f"MMDC_val_ep_{sample.current_epoch:03}_scat_s2_"
            f"{samp_idx:03}_{sample.batch_idx}"
        )
        image_name = Path(f"{self.save_dir}/{image_basename}.png")
        if not image_name.is_file() or sample.current_epoch == 0:
            s2d = {
                key: self.to_flat_vector(s2_data, samp_idx, idx, sample.patch_margin)
                for idx, key in enumerate(self.keys_s2)
            }
            s2p = {
                key: self.to_flat_vector(
                    pred.encs2_mu_decs2, samp_idx, idx, sample.patch_margin
                )
                for idx, key in enumerate(self.keys_s2)
            }

            s2c = {
                key: self.to_flat_vector(
                    pred.encs1_mu_decs2, samp_idx, idx, sample.patch_margin
                )
                for idx, key in enumerate(self.keys_s2)
            }

            self.plot_correlation(
                image_name, sample.current_epoch, CorrelationData(s2d, s2p, s2c)
            )

    def plot_correlation(
        self,
        image_name: Path,
        current_epoch: int,
        datasets: CorrelationData,
    ):
        """Scatterplots between (cross-)reconstructions and target data"""
        bands = list(datasets.data.keys())
        satellite = "S1" if bands[0] == self.keys_s1[0] else "S2"
        nrows = 4 if datasets.cross is not None else 2
        ncols = int(len(bands) / 2)
        plt.close()
        fig, axes = plt.subplots(
            nrows=nrows,
            ncols=ncols,
            sharex=False,
            sharey=False,
            figsize=(20, nrows * 3.5 + 2),
        )
        fig.suptitle(f"{satellite} {current_epoch = }", fontsize=20)
        for i in range(nrows):
            for j in range(ncols):
                if i < 2:
                    band = bands[i * ncols + j]
                    self.plot_scatter(
                        axes[i, j],
                        f"{satellite} {band.upper()} Pred",
                        datasets.data[band],
                        datasets.pred[band],
                    )
                else:
                    band = bands[(i - 2) * ncols + j]
                    self.plot_scatter(
                        axes[i, j],
                        f"{satellite} {band.upper()} Cross",
                        datasets.data[band],
                        datasets.cross[band],
                    )
        fig.savefig(image_name)

    def save_scatterplots(
        self,
        data: tuple[torch.Tensor, torch.Tensor, S1S2VAEOutput],
        sample: SampleInfo,
    ) -> None:
        """Generate scatterplots for the latents and reconstructions"""
        s1_data, s2_data, pred = data
        for samp_idx in range(
            0,
            sample.batch_size,
            sample.batch_size // min(sample.batch_size, self.n_samples),
        ):
            self.save_latent_scatterplots(pred, samp_idx, sample)

            self.save_s1_scatterplots(s1_data, pred, samp_idx, sample)

            self.save_s2_scatterplots(s2_data, pred, samp_idx, sample)

    def on_validation_batch_end(  # pylint: disable=too-many-arguments
        self,
        trainer: pl.trainer.Trainer,
        pl_module: MMDCBaseLitModule,
        outputs: Any,
        batch: torch.Tensor,
        batch_idx: int,
        dataloader_idx: int = 0,
    ) -> None:
        """Method called from the validation looi"""
        if batch_idx < 2:
            model = pl_module.model
            patch_margin = model.nb_cropped_hw
            assert isinstance(patch_margin, int)

            debatch = destructure_batch(batch)

            pred: S1S2VAEOutput = pl_module.model.predict(
                MMDCForwardType(
                    S2FowardType(debatch.s2_x, debatch.s2_m, debatch.s2_a),
                    S1FowardType(debatch.s1_x, debatch.s1_vm, debatch.s1_a),
                    debatch.meteo_x,
                    debatch.dem_x,
                )
            )
            s2_x, s1_x = debatch.s2_x.nan_to_num(), debatch.s1_x.nan_to_num()
            batch_size = s1_x.shape[0]

            self.save_image_grid(
                (s1_x, s2_x),
                pred,
                SampleInfo(batch_idx, batch_size, patch_margin, trainer.current_epoch),
            )
            self.save_scatterplots(
                (s1_x, s2_x, pred),
                SampleInfo(batch_idx, batch_size, patch_margin, trainer.current_epoch),
            )


class MMDCAEAuxCallback(MMDCAECallback):
    """
    Callback to inspect the reconstruction image
    """

    def __init__(
        self,
        save_dir: str,
        n_samples: int = 3,
        normalize: bool = False,
        value_range: tuple[float, float] = (-1.0, 1.0),
    ):
        super().__init__(save_dir, n_samples, normalize, value_range)

    def to_vector(
        self, data: torch.Tensor, idx: int, bands: list[int] | int, margin: int
    ) -> np.ndarray:
        """Extract patch from batch, crop the margin and flatten the
        spatial dimensions"""
        res: np.ndarray = (
            self.extract_crop(data, idx, bands, margin).cpu().detach().numpy()
        )
        return res

    def image_subplot(
        self, position: tuple[int, int], img: np.ndarray, label: str
    ) -> None:
        """Insert an image as a subplot at the given position in the grid"""
        ax_dem_h = plt.subplot2grid((3, 3), position, colspan=1)
        ax_dem_h.imshow(img, interpolation="bicubic")
        ax_dem_h.set_title(label)

    def build_subplots(self, dem: DEM, embeddings: tuple[np.ndarray, ...]) -> None:
        """Build the axes for the subplots for a sample"""
        (
            dem_s1_render,
            dem_s2_render,
            meteo_render,
            s2_a_render,
            s1_a_render,
        ) = embeddings
        self.image_subplot((0, 0), dem.height, "DEM_H")
        self.image_subplot((0, 1), dem.slope, "DEM_S")
        self.image_subplot((0, 2), dem.aspect, "DEM_A")
        self.image_subplot((1, 0), dem_s1_render, "DEM_S1_EMB")
        self.image_subplot((1, 1), dem_s2_render, "DEM_S2_EMB")
        self.image_subplot((1, 2), meteo_render, "METEO_EMB")
        self.image_subplot((2, 0), s1_a_render, "S1_ANG_EMB")
        self.image_subplot((2, 1), s2_a_render, "S2_ANG_EMB")

    def save_auxiliary_grid(self, aux_data: AuxData, sample: SampleInfo) -> None:
        """Save the matplotlib figure for the auxiliary data"""
        for samp_idx in range(
            0,
            sample.batch_size,
            sample.batch_size // min(sample.batch_size, self.n_samples),
        ):
            # export name
            image_basename = (
                f"MMDC_val_aux_ep_{sample.current_epoch:03}_samp_"
                f"{samp_idx:03}_{sample.batch_idx}"
            )
            image_name = f"{self.save_dir}/{image_basename}.png"
            if not Path(image_name).is_file() or sample.current_epoch == 0:
                dem_h_render, _, _ = rgb_render(
                    self.to_vector(
                        aux_data.dem_x,
                        samp_idx,
                        [0, 0, 0],
                        sample.patch_margin,
                    )
                )
                dem_s_render, _, _ = rgb_render(
                    self.to_vector(
                        aux_data.dem_x,
                        samp_idx,
                        [1, 1, 1],
                        sample.patch_margin,
                    )
                )
                dem_a_render, _, _ = rgb_render(
                    self.to_vector(
                        aux_data.dem_x,
                        samp_idx,
                        [2, 2, 2],
                        sample.patch_margin,
                    )
                )
                dem_s1_render, _, _ = rgb_render(
                    self.to_vector(
                        aux_data.embs.s1_dem_emb,
                        samp_idx,
                        [0, 1, 2],
                        sample.patch_margin,
                    )
                )
                dem_s2_render, _, _ = rgb_render(
                    self.to_vector(
                        aux_data.embs.s2_dem_emb,
                        samp_idx,
                        [0, 1, 2],
                        sample.patch_margin,
                    )
                )
                meteo_render, _, _ = rgb_render(
                    self.to_vector(
                        aux_data.embs.meteo_emb,
                        samp_idx,
                        [0, 1, 2],
                        sample.patch_margin,
                    )
                )
                s1_a_render, _, _ = rgb_render(
                    self.to_vector(
                        aux_data.embs.s1_angles_emb,
                        samp_idx,
                        [0, 1, 2],
                        sample.patch_margin,
                    )
                )
                s2_a_render, _, _ = rgb_render(
                    self.to_vector(
                        aux_data.embs.s2_angles_emb,
                        samp_idx,
                        [0, 1, 2],
                        sample.patch_margin,
                    )
                )

                plt.close()
                fig = plt.figure(figsize=(20, 16))
                self.build_subplots(
                    DEM(dem_h_render, dem_s_render, dem_a_render),
                    (
                        dem_s1_render,
                        dem_s2_render,
                        meteo_render,
                        s2_a_render,
                        s1_a_render,
                    ),
                )
                fig.savefig(image_name)

    def on_validation_batch_end(  # pylint: disable=too-many-arguments
        self,
        trainer: pl.trainer.Trainer,
        pl_module: MMDCBaseLitModule,
        outputs: Any,
        batch: torch.Tensor,
        batch_idx: int,
        dataloader_idx: int = 0,
    ) -> None:
        """Method called from the validation loop"""
        if batch_idx < 2:
            debatch: MMDCBatch = destructure_batch(batch)

            pred, aux_embs = pl_module.model.predict(
                MMDCForwardType(
                    S2FowardType(debatch.s2_x, debatch.s2_m, debatch.s2_a),
                    S1FowardType(debatch.s1_x, debatch.s1_vm, debatch.s1_a),
                    debatch.meteo_x,
                    debatch.dem_x,
                )
            )

            s2_x, s1_x = debatch.s2_x.nan_to_num(), debatch.s1_x.nan_to_num()

            sample = SampleInfo(
                batch_idx,
                s1_x.shape[0],
                pl_module.model.nb_cropped_hw,
                trainer.current_epoch,
            )
            s1_x = despeckle_batch(s1_x)

            self.save_image_grid((s1_x, s2_x), pred, sample)
            self.save_auxiliary_grid(
                AuxData(
                    debatch.s2_a,
                    debatch.s1_a,
                    debatch.dem_x,
                    aux_embs,
                ),
                sample,
            )
            self.save_scatterplots((s1_x, s2_x, pred), sample)


class MMDCAECallbackExperts(MMDCAECallback):
    """
    Callback to inspect the reconstruction image
    """

    def __init__(
        self,
        save_dir: str,
        n_samples: int = 3,
        normalize: bool = False,
        value_range: tuple[float, float] = (-1.0, 1.0),
    ):
        super().__init__(save_dir, n_samples, normalize, value_range)

        # labels list
        self.labels_list = LABELS_LIST_EXPERTS

    def prepare_patches_grid(
        self,
        samp_idx: int,
        sent_data: tuple[torch.Tensor, torch.Tensor],
        pred: S1S2VAEOutputPreExperts,
        patch_margin: int,
    ) -> tuple[
        tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor],
        tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor],
        tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor],
        tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor],
        tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor],
        tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor],
    ]:
        """Generate the patches for the visualization grid
        Select the appropriate channels and crop with the patch margin.
        """
        (s1_data, s2_data) = sent_data
        inputs = self.prepare_s1_s2(s1_data, s2_data, samp_idx, patch_margin)
        preds = self.prepare_s1_s2(
            pred.encs1_mu_decs1, pred.encs2_mu_decs2, samp_idx, patch_margin
        )
        preds_pre_exp = self.prepare_s1_s2(
            pred.mu_latents.mu_lat_s1_dec_s1,
            pred.mu_latents.mu_lat_s2_dec_s2,
            samp_idx,
            patch_margin,
        )
        preds_pre_exp_cross = self.prepare_s1_s2(
            pred.mu_latents.mu_lat_s2_dec_s1,
            pred.mu_latents.mu_lat_s1_dec_s2,
            samp_idx,
            patch_margin,
        )
        lat_mu = self.prepare_lat(pred.latent_experts.mean, samp_idx, patch_margin)
        lat_logvar = self.prepare_lat(
            pred.latent_experts.logvar, samp_idx, patch_margin
        )

        return inputs, preds, preds_pre_exp, preds_pre_exp_cross, lat_mu, lat_logvar

    def render_row(
        self,
        imcol: tuple[torch.Tensor, ...],
        j: int,
        d_min_max: tuple[tuple[float, float], ...],
        last_rgb_row: int = 4,
    ) -> tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
        """Render a row of the image grid"""
        return super().render_row(imcol, j, d_min_max, last_rgb_row=4)

    def save_s1_scatterplots(
        self,
        s1_data: torch.Tensor,
        pred: S1S2VAEOutputPreExperts,
        samp_idx: int,
        sample: SampleInfo,
    ) -> None:
        """Save scatterplots for S1 reconstructeon vs prediction"""
        image_basename = (
            f"MMDC_val_ep_{sample.current_epoch:03}_scat_s1_"
            f"{samp_idx:03}_{sample.batch_idx}"
        )
        image_name = Path(f"{self.save_dir}/{image_basename}.png")
        if not image_name.is_file() or sample.current_epoch == 0:
            s1d = {
                key: self.to_flat_vector(s1_data, samp_idx, idx, sample.patch_margin)
                for idx, key in enumerate(self.keys_s1)
            }

            s1p = {
                key: self.to_flat_vector(
                    pred.encs1_mu_decs1, samp_idx, idx, sample.patch_margin
                )
                for idx, key in enumerate(self.keys_s1)
            }

            self.plot_correlation(
                image_name, sample.current_epoch, CorrelationData(s1d, s1p)
            )

    def save_s2_scatterplots(
        self,
        s2_data: torch.Tensor,
        pred: S1S2VAEOutputPreExperts,
        samp_idx: int,
        sample: SampleInfo,
    ) -> None:
        """Save S2 scatterplots for preditions for a sample
        for experts model"""
        image_basename = (
            f"MMDC_val_ep_{sample.current_epoch:03}_scat_s2_"
            f"{samp_idx:03}_{sample.batch_idx}"
        )
        image_name = Path(f"{self.save_dir}/{image_basename}.png")
        if not image_name.is_file() or sample.current_epoch == 0:
            s2d = {
                key: self.to_flat_vector(s2_data, samp_idx, idx, sample.patch_margin)
                for idx, key in enumerate(self.keys_s2)
            }
            s2p = {
                key: self.to_flat_vector(
                    pred.encs2_mu_decs2, samp_idx, idx, sample.patch_margin
                )
                for idx, key in enumerate(self.keys_s2)
            }

            self.plot_correlation(
                image_name, sample.current_epoch, CorrelationData(s2d, s2p)
            )

    def save_scatterplots(
        self,
        data: tuple[torch.Tensor, torch.Tensor, S1S2VAEOutputPreExperts],
        sample: SampleInfo,
    ) -> None:
        """Generate scatterplots for the latents and reconstructions"""
        s1_data, s2_data, pred = data
        for samp_idx in range(
            0,
            sample.batch_size,
            sample.batch_size // min(sample.batch_size, self.n_samples),
        ):
            self.save_latent_scatterplots(pred, samp_idx, sample)
            self.save_s1_scatterplots(s1_data, pred, samp_idx, sample)
            self.save_s2_scatterplots(s2_data, pred, samp_idx, sample)


class MMDCAEAuxCallbackExperts(MMDCAEAuxCallback, MMDCAECallbackExperts):
    """
    Callback to inspect the reconstruction image
    with experts model
    """

    def __init__(
        self,
        save_dir: str,
        n_samples: int = 3,
        normalize: bool = False,
        value_range: tuple[float, float] = (-1.0, 1.0),
    ):
        super().__init__(save_dir, n_samples, normalize, value_range)

    def save_image_grid(
        self,
        sen_data: tuple[torch.Tensor, torch.Tensor],
        pred: S1S2VAEOutputPreExperts,
        sample: SampleInfo,
    ) -> None:
        """Generate the matplotlib figure and save it to a file"""
        MMDCAECallbackExperts.save_image_grid(self, sen_data, pred, sample)

    def save_scatterplots(
        self,
        data: tuple[torch.Tensor, torch.Tensor, S1S2VAEOutputPreExperts],
        sample: SampleInfo,
    ) -> None:
        """Generate scatterplots for the latents and reconstructions"""
        MMDCAECallbackExperts.save_scatterplots(self, data, sample)

    def on_validation_batch_end(  # pylint: disable=too-many-arguments
        self,
        trainer: pl.trainer.Trainer,
        pl_module: MMDCBaseLitModule,
        outputs: Any,
        batch: torch.Tensor,
        batch_idx: int,
        dataloader_idx: int = 0,
    ) -> None:
        """Method called from the validation loop"""
        if batch_idx < 2:
            debatch: MMDCBatch = destructure_batch(batch)

            pred, aux_embs = pl_module.model.predict(
                MMDCForwardType(
                    S2FowardType(debatch.s2_x, debatch.s2_m, debatch.s2_a),
                    S1FowardType(debatch.s1_x, debatch.s1_vm, debatch.s1_a),
                    debatch.meteo_x,
                    debatch.dem_x,
                )
            )
            pred_pre_experts = pl_module.model.generate_mean_reconstructions_pre_latent(
                pred.latent, aux_embs
            )
            s2_x, s1_x = debatch.s2_x.nan_to_num(), debatch.s1_x.nan_to_num()
            pred = S1S2VAEOutputPreExperts(
                latent_experts=pred.latent_experts,
                latent=pred.latent,
                encs1_mu_decs1=pred.encs1_mu_decs1,
                encs2_mu_decs2=pred.encs2_mu_decs2,
                mu_latents=S1S2VAEOutputPreExpertsMuLatent(
                    mu_lat_s1_dec_s1=pred_pre_experts.encs1_mu_decs1,
                    mu_lat_s2_dec_s2=pred_pre_experts.encs2_mu_decs2,
                    mu_lat_s2_dec_s1=pred_pre_experts.encs2_mu_decs1,
                    mu_lat_s1_dec_s2=pred_pre_experts.encs1_mu_decs2,
                ),
            )

            sample = SampleInfo(
                batch_idx,
                s1_x.shape[0],
                pl_module.model.nb_cropped_hw,
                trainer.current_epoch,
            )
            s1_x = despeckle_batch(s1_x)

            self.save_image_grid((s1_x, s2_x), pred, sample)
            self.save_auxiliary_grid(
                AuxData(
                    debatch.s2_a,
                    debatch.s1_a,
                    debatch.dem_x,
                    aux_embs,
                ),
                sample,
            )
            self.save_scatterplots((s1_x, s2_x, pred), sample)
