"""Freezing layers module """

from pytorch_lightning.callbacks import BaseFinetuning
from torch.optim import Optimizer

from ..models.lightning.full import MMDCFullLitModule


class PretrainedFreezeUnfreeze(BaseFinetuning):
    """Lightning callback for finetuning a MMDC module"""

    def __init__(self, unfreeze_at_epoch: int = 10):
        super().__init__()
        self._unfreeze_at_epoch = unfreeze_at_epoch

    def freeze_before_training(self, pl_module: MMDCFullLitModule) -> None:
        """Freeze pretrained layers"""
        self.freeze(
            [
                getattr(pl_module.model, module_name)
                for module_name in pl_module.pretrained_layers
            ],
            train_bn=False,
        )

    def finetune_function(
        self, pl_module: MMDCFullLitModule, epoch: int, optimizer: Optimizer
    ) -> None:
        """Fine tune starting at given epoch"""
        if epoch == self._unfreeze_at_epoch:
            self.unfreeze_and_add_param_group(
                modules=[
                    getattr(pl_module.model, module_name)
                    for module_name in pl_module.pretrained_layers
                ],
                optimizer=optimizer,
                train_bn=True,
            )
