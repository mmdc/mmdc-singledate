#!/usr/bin/env python3
""" Lightning callbacks for the evaluation step """
from pathlib import Path

import pytorch_lightning as pl
from pytorch_lightning.callbacks import Callback

from ..utils.eval_utils import (
    plot_training_evolution,
    read_csv_training_log,
    verify_available_tiles,
)


class PlotEvolutionCallback(Callback):
    """
    Plot Evolution Callback
    """

    def __init__(  # pylint: disable=R0913
        self,
        save_dir: str,
        csv_save_dir: str,
        name_dir: str,
        csv_version: str,
        discard: int,
    ):
        """
        Init parameter
        """
        # export dir
        self.save_dir = save_dir
        # pass the csv log folder
        self.csv_log_folder = Path(f"{csv_save_dir}/{name_dir}/{csv_version}/")
        self.discard = discard

    def on_train_end(
        self,
        trainer: pl.trainer.Trainer,
        pl_module: pl.LightningModule,
    ) -> None:
        # read the csv file
        evolution = read_csv_training_log(log_path=self.csv_log_folder)
        if evolution is not None:
            train_evolution, val_evolution = evolution
            # plotting and store the graph
            fig, _ = plot_training_evolution(
                train_evolution, val_evolution, self.discard
            )
            # export name
            export_path = Path(self.save_dir) / "evolution_plot.png"
            fig.savefig(export_path)


class AvailableTilesCallback(Callback):
    """
    Export the geofile with the tiles
    using in training
    """

    def __init__(self, dataset_path: str, export_path: str):
        """
        Constructor

        :param: dataset_path : Path to the original
        dataset of TIF files
        :param: export_path : Path for export the geofile

        :return: None
        """

        self.dataset_path = Path(dataset_path)
        self.export_path = Path(export_path)

    def on_train_end(self, trainer: pl.Trainer, pl_module: pl.LightningModule) -> None:
        verify_available_tiles(
            dataset_path=self.dataset_path,
            export_path=self.export_path,
            filename="availabletiles",
        )
