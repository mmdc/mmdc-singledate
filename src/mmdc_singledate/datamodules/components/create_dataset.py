"""Function for dataset creation from existing ROIS. The geotiffs are
patchified, the patches are filtered and everything is exported to torch
tensors."""

import logging
import sys
import warnings
from dataclasses import dataclass
from pathlib import Path

from rasterio import logging as rio_logger

from mmdc_singledate.datamodules.components.compute_paths_and_dates_df import (
    extract_dates,
    extract_paths,
)

from .datamodule_components import StoreDataConfig, store_data
from .datatypes import DatasetPaths, MatchParams, PatchCreationParams, SeriesDays

# Configure the logger
NUMERIC_LEVEL = getattr(logging, "INFO", None)
logging.basicConfig(
    level=NUMERIC_LEVEL, format="%(asctime)-15s %(levelname)s: %(message)s"
)
logger = logging.getLogger(__name__)

logger_rio = rio_logger.getLogger("rasterio._env")
logger_rio.setLevel(logging.ERROR)

warnings.simplefilter("always")


@dataclass
class CreateDatasetParams:
    """Configuration for create data set"""

    input_tile_list: list[str]
    days_selection: SeriesDays | None = None
    patch_params: PatchCreationParams | None = None
    match_params: MatchParams | None = None
    rois: list[str | int] | None = None


def create_dataset(dataset_paths: DatasetPaths, params: CreateDatasetParams) -> None:
    """
    Create Dataset :
    1: Generate paths to all data
    2: Create a csv file with matching dates
    3: Open data from netcdf and write it to tensors
    """
    if (days_selection := params.days_selection) is None:
        days_selection = SeriesDays(0, 2, 2, None, None)
    if (patch_params := params.patch_params) is None:
        patch_params = PatchCreationParams(32, 4, 0.95)
    if (match_params := params.match_params) is None:
        match_params = MatchParams(False, 0.95, False)
    # Depending if the data is already exported or not read
    export_path = Path(dataset_paths.export_path)
    samples_dir = Path(dataset_paths.samples_dir)

    logger.info("Exporting...")
    # Read the metadata and create one dataframe with the image paths,
    # and the second one with selected dates
    mmdc_df = extract_paths(samples_dir, export_path, params.input_tile_list)
    sel_dates_all = extract_dates(
        mmdc_df,
        export_path,
        days_selection,
        match_params.s1_asc_desc_strict,
        match_params.prefilter_clouds,
    )

    logger.info("df : %s", len(sel_dates_all))
    logger.info("df.cols %s", sel_dates_all.columns)

    if match_params.only_csv:  # We only produce csv files with dates and paths
        sys.exit()

    store_data(
        path_df=mmdc_df,
        date_df=sel_dates_all,
        export_dir=export_path,
        config=StoreDataConfig(
            patch_config=(patch_params.patch_size, patch_params.patch_margin),
            threshold=patch_params.threshold,
            rois=params.rois,
        ),
    )

    logger.info("Export finished")
