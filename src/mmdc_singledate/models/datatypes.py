""" Type definitions for MMDC models """

from abc import abstractmethod
from dataclasses import dataclass
from typing import Literal, Protocol

import torch

from ..datamodules.datatypes import MMDCDataChannels


@dataclass
class UnetParams:
    """Configuration parameters for a Unet"""

    out_channels: int
    encoder_sizes: list[int]
    kernel_size: int
    tail_layers: int


@dataclass
class ConvnetParams:
    """Configuration parameters for a CNN"""

    out_channels: int
    sizes: list[int]
    kernel_sizes: list[int]


@dataclass
class MMDCDataUse:
    """Configuration for the use (or not) of the different aux data"""

    s1_angles: bool
    s2_angles: bool
    dem: bool
    meteo: bool


@dataclass
class TranslationLossWeights:
    """Weights of the different losses for an image translation task"""

    nll: float


@dataclass
class TranslationDataConfig:
    """Configuration of data (channels and use) for a translation task"""

    sizes: MMDCDataChannels
    use: MMDCDataUse


@dataclass
class MMDCConfig:
    """Base class for configs"""


@dataclass
class TranslationConfig(MMDCConfig):
    """Model configuration for a translation task"""

    data: TranslationDataConfig
    encoder_params: UnetParams
    wc_embed_size: int
    loss_weights: TranslationLossWeights
    translation_target: Literal["S1", "S2"]
    input_size: int
    output_size: int


@dataclass
class ModularEmbeddingConfig(MMDCConfig):
    """Configuration for a modular embedding set of modules"""

    s1_angles: ConvnetParams
    s1_dem: UnetParams
    s2_angles: ConvnetParams
    s2_dem: UnetParams
    meteo: ConvnetParams


@dataclass
class ConfigWithSizesAndEmbeddings(MMDCConfig):
    """Interface for the configuration of models with data sizes and
    embeddings"""

    # Would be much nicer with a protocol, but dataclasses don't support
    # them
    data_sizes: MMDCDataChannels
    embeddings: ModularEmbeddingConfig


@dataclass
class EmbedderProtocol(Protocol):
    """Protocol to model a set of nets used for aux data embedding"""

    s1_angles: torch.nn.Module
    s2_angles: torch.nn.Module
    s1_dem: torch.nn.Module
    s2_dem: torch.nn.Module
    meteo: torch.nn.Module


@dataclass
class MultiVAENetsProtocol(Protocol):
    """The set of networks for a MultiVAE model"""

    embedders: EmbedderProtocol
    encoder_s1: torch.nn.Module
    encoder_s2: torch.nn.Module
    decoder_s1: torch.nn.Module
    decoder_s2: torch.nn.Module


@dataclass
class TranslationNets:
    """Nets for a translation module"""

    encoder: UnetParams
    decoder: ConvnetParams


@dataclass
class ModularTranslationConfig(ConfigWithSizesAndEmbeddings):
    """Configuration for a translation model with modular embeddings"""

    data_sizes: MMDCDataChannels
    embeddings: ModularEmbeddingConfig
    nets: TranslationNets
    enc_use: MMDCDataUse
    dec_use: MMDCDataUse
    loss_weights: TranslationLossWeights
    translation_target: Literal["S1", "S2"]


@dataclass
class LatentLossWeights:
    "Weights for the loss term on the latent space"
    latent: float = 1.0
    latent_cov: float = 1.0
    latent_max_var: bool = False


@dataclass
class MultiVAELossWeights:
    """Weights of the different losses for a MultiVAE network"""

    forward: float
    latent: LatentLossWeights
    cross: float | None = None
    lai: float | None = None


@dataclass
class MultiVAEAuxUseConfig:
    """Configuration for the use of aux data in a MultiVAE"""

    s1_enc_use: MMDCDataUse
    s2_enc_use: MMDCDataUse
    s1_dec_use: MMDCDataUse
    s2_dec_use: MMDCDataUse


@dataclass
class MultiVAEEncDecConfig:
    """Configuration for a the AE of a MultiVAE network"""

    s1_encoder: UnetParams
    s2_encoder: UnetParams
    s1_decoder: ConvnetParams
    s2_decoder: ConvnetParams
    ae_use: MultiVAEAuxUseConfig


@dataclass
class MultiVAEConfig(ConfigWithSizesAndEmbeddings):
    """Configuration for a MultiVAE network"""

    data_sizes: MMDCDataChannels
    embeddings: ModularEmbeddingConfig
    auto_enc: MultiVAEEncDecConfig
    loss_weights: MultiVAELossWeights
    experts_strategy: str | None = None


@dataclass
class S2FowardType:
    """Forward type for S2 data in MMDC models"""

    s2_x: torch.Tensor
    s2_m: torch.Tensor
    s2_a: torch.Tensor


@dataclass
class S1FowardType:
    """Forward type for S1 data in MMDC models"""

    s1_x: torch.Tensor
    s1_vm: torch.Tensor
    s1_a: torch.Tensor


@dataclass
class MMDCForwardType:
    """Forward type for MMDC models"""

    s_2: S2FowardType
    s_1: S1FowardType
    meteo_x: torch.Tensor
    dem_x: torch.Tensor


@dataclass
class VAELatentSpace:
    """
    Class to hold data of the latent space distribution

    :param mu: Mean of Latent space ROI
    :param logvar: Log variance of Latent space ROI
    """

    mean: torch.Tensor
    logvar: torch.Tensor


@dataclass
class ExpertsOut:
    """
    Class to hold the output of experts latent space:
    latent space with mu and logvar and a drawn sample
    """

    latent_experts: VAELatentSpace
    sample: torch.Tensor


@dataclass
class S1S2VAELatentSpace:
    """Class to hold data of the latent space content"""

    sen1: VAELatentSpace
    sen2: VAELatentSpace


@dataclass
class AuxData:
    """Auxiliary data for MMDC models"""

    s1_angles: torch.Tensor
    s2_angles: torch.Tensor
    meteo: torch.Tensor
    dem: torch.Tensor


@dataclass
class S1S2VAEAuxiliaryEmbeddings:
    """Class to hold embeddings of the auxiliary data"""

    s1_angles_emb: torch.Tensor | None
    s2_angles_emb: torch.Tensor | None
    s1_dem_emb: torch.Tensor | None
    s2_dem_emb: torch.Tensor | None
    meteo_emb: torch.Tensor | None


@dataclass
class S1S2VAEOutput:
    """Class to hold data of the model predictions output"""

    latent: S1S2VAELatentSpace
    encs1_mu_decs1: torch.Tensor
    encs1_mu_decs2: torch.Tensor
    encs2_mu_decs1: torch.Tensor
    encs2_mu_decs2: torch.Tensor


@dataclass
class S1S2VAEOutputExperts:
    """Class to hold data of the model predictions output for experts module"""

    latent: S1S2VAELatentSpace
    latent_experts: VAELatentSpace
    encs1_mu_decs1: torch.Tensor
    encs2_mu_decs2: torch.Tensor


@dataclass
class S1S2VAEOutputPreExpertsMuLatent:
    mu_lat_s1_dec_s1: torch.Tensor  # S1 from lat S1
    mu_lat_s2_dec_s2: torch.Tensor  # S2 from lat S2
    mu_lat_s2_dec_s1: torch.Tensor  # S1 from lat S2
    mu_lat_s1_dec_s2: torch.Tensor  # S2 from lat S1


@dataclass
class S1S2VAEOutputPreExperts:  # TODO: please explain all these fields
    """Latent space before expert fusion"""

    latent_experts: VAELatentSpace
    latent: S1S2VAELatentSpace
    encs1_mu_decs1: torch.Tensor
    encs2_mu_decs2: torch.Tensor
    mu_latents: S1S2VAEOutputPreExpertsMuLatent


@dataclass
class S1S2VAEReconstructions:
    """Struct for forward and cross predictions of a MultiVAE"""

    s1_pred: VAELatentSpace
    s2_pred: VAELatentSpace
    s1_cross: VAELatentSpace | None = None
    s2_cross: VAELatentSpace | None = None


@dataclass
class S1S2VAEMeanReconstructions:
    """Struct for mean forward and cross predictions of a MultiVAE"""

    s1_pred: torch.Tensor
    s2_pred: torch.Tensor
    s1_cross: torch.Tensor | None = None
    s2_cross: torch.Tensor | None = None


@dataclass
class S1S2VAEForwardType:
    """Class to hold data of the model forward output"""

    aux_embs: S1S2VAEAuxiliaryEmbeddings
    latent: S1S2VAELatentSpace
    mean_recs: S1S2VAEMeanReconstructions
    recons: S1S2VAEReconstructions


@dataclass
class S1S2VAEForwardTypeExperts:
    """Class to hold data of the experts model forward output"""

    aux_embs: S1S2VAEAuxiliaryEmbeddings
    latent: S1S2VAELatentSpace
    latent_experts: VAELatentSpace
    mean_recs: S1S2VAEMeanReconstructions
    recons: S1S2VAEReconstructions


@dataclass
class MMDCFullDataForLoss:
    """Struct with target and mean predictions with and without
    standardization. Used for the computation of different losses."""

    x_u: torch.Tensor
    x_s: torch.Tensor
    x_mu_u: torch.Tensor
    x_mu_s: torch.Tensor


@dataclass
class S1S2VAELosses:
    """The different losses for a MultiVAE"""

    s1_rec: torch.Tensor
    s2_rec: torch.Tensor
    s1_cross: torch.Tensor | None = None
    s2_cross: torch.Tensor | None = None
    code: torch.Tensor | None = None
    covar_code: torch.Tensor | None = None
    lai_direct: torch.Tensor | None = None
    lai_cross: torch.Tensor | None = None


@dataclass
class S1S2VAEMaskingLosses:
    """
    The different losses for a MultiVAE
    with masking
    """

    sen1_rec: torch.Tensor
    sen2_rec: torch.Tensor


@dataclass
class PretrainedModel:
    """
    Path to pretrained model to partially load the weights from
    and selected layers weights we want to initialize
    """

    path: str
    selected_layers: list[str] | str
    freeze_epochs: int | None = None


class MMDCTorchModuleProtocol(Protocol):  # pylint: disable=R0903
    """Protocol for all MMDC torch modules"""

    nb_cropped_hw: int


class MMDCEncoder(MMDCTorchModuleProtocol):  # pylint: disable=R0903
    """Protocol for all MMDC encoders"""


class MMDCTorchModel(MMDCTorchModuleProtocol):
    """Protocol for all MMDC encoder / decoder models"""

    config: MultiVAEConfig
    nets: MultiVAENetsProtocol
    nb_cropped_hw: int
    nb_enc_cropped_hw: int
    device: torch.device

    @abstractmethod
    def get_latents1(
        self,
        s1_x: torch.Tensor,
        aux_data: AuxData,
    ) -> VAELatentSpace:
        """Produce a latent space from S1 data"""

    @abstractmethod
    def get_latents2(
        self,
        s2_x: torch.Tensor,
        aux_data: AuxData,
    ) -> VAELatentSpace:
        """Produce a latent space from S2 data"""

    def eval(self) -> None:
        """Put the model in eval mode"""

    @abstractmethod
    def to(self, device: str | torch.device) -> None:
        """Send the model to the device"""
