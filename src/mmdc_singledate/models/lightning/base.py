# Copyright: (c) 2022 CESBIO / Centre National d'Etudes Spatiales
""" Base classes for the MMDC Lightning module and Pytorch model"""
import logging
import os
from abc import abstractmethod
from pathlib import Path
from typing import Any

import torch
from pytorch_lightning import LightningModule
from torch import nn
from torchmetrics import MinMetric
from torchutils import metrics

from ...datamodules.datatypes import MMDCShiftScales, ShiftScale
from ...datamodules.mmdc_datamodule import MMDCDataStats
from ..datatypes import MMDCForwardType
from ..snap.lai_snap import BVNET
from ..torch.base import MMDCBaseModule

# Configure logging
NUMERIC_LEVEL = getattr(logging, "INFO", None)
logging.basicConfig(
    level=NUMERIC_LEVEL, format="%(asctime)-15s %(levelname)s: %(message)s"
)

logger = logging.getLogger(__name__)


class MMDCBaseLitModule(LightningModule):  # pylint: disable=too-many-ancestors
    """
    Base Lightning Module for the MMDC Single Date networks
    """

    def __init__(
        self,
        model: MMDCBaseModule,
        lr: float = 0.001,
        resume_from_checkpoint: str | None = None,
    ):
        super().__init__()

        # this line allows to access init params with 'self.hparams' attribute
        # it also ensures init params will be stored in ckpt
        self.save_hyperparameters(logger=False)
        self.model = model
        self.resume_from_checkpoint = resume_from_checkpoint

        # use separate metric instance for train, val and test step
        # to ensure a proper reduction over the epoch
        self.test_rmse = metrics.rmse

        # for logging best so far validation rmse
        self.val_rmse_best = MinMetric()
        self.learning_rate = lr

        # Snap model for LAI loss
        self.snap_model = BVNET(device="cuda", ver="2", variable="lai")
        self.snap_model.set_snap_weights(freeze_model=True)

    @abstractmethod
    def step(self, batch: Any) -> Any:
        """Perform one optimization step"""

    @abstractmethod
    def forward(self, data: MMDCForwardType) -> Any:  # pylint: disable=arguments-differ
        """Generic forward pass of the model. Just delegate to the Pytorch model."""

    def training_step(  # pylint: disable=arguments-differ
        self,
        batch: Any,
        batch_idx: int,  # pylint: disable=unused-argument
    ) -> dict[str, Any]:
        """Training step. Step and return loss."""
        torch.autograd.set_detect_anomaly(True)
        loss = self.step(batch)

        # log training metrics
        self.log(
            "train/loss",
            loss,
            on_step=False,
            on_epoch=True,
            prog_bar=False,
        )

        return {"loss": loss}

    def validation_step(  # pylint: disable=arguments-differ
        self,
        batch: Any,
        batch_idx: int,  # pylint: disable=unused-argument
    ) -> dict[str, Any]:
        """Validation step. Step and return loss."""
        loss = self.step(batch)

        # log val metrics
        self.log(
            "val/loss",
            loss,
            on_step=False,
            on_epoch=True,
            prog_bar=False,
        )
        return {"loss": loss}

    def test_step(  # pylint: disable=arguments-differ
        self,
        batch: Any,
        batch_idx: int,  # pylint: disable=unused-argument
    ) -> dict[str, Any]:
        """Test step. Step and return loss."""
        loss = self.step(batch)

        self.log(
            "test/loss",
            loss,
        )

        return {"loss": loss}

    def on_train_epoch_end(self) -> None:
        logger.info("Ended traning epoch %s", self.trainer.current_epoch)

    def on_validation_epoch_end(self) -> None:
        logger.info("Ended validation epoch %s", self.trainer.current_epoch)

    def on_test_epoch_end(self) -> None:
        """Callback after a test epoch"""

    def set_stats(self, stats: MMDCDataStats) -> None:
        """Set shift and scale for model"""
        scale_regul = nn.Threshold(1e-10, 1.0)
        shift_scale_s2 = ShiftScale(
            stats.sen2.median,
            scale_regul((stats.sen2.qmax - stats.sen2.qmin) / 2.0),
        )
        shift_scale_s1 = ShiftScale(
            stats.sen1.median,
            scale_regul((stats.sen1.qmax - stats.sen1.qmin) / 2.0),
        )
        shift_scale_meteo = ShiftScale(
            stats.meteo.concat_stats().median,
            scale_regul(
                (stats.meteo.concat_stats().qmax - stats.meteo.concat_stats().qmin)
                / 2.0
            ),
        )
        shift_scale_dem = ShiftScale(
            stats.dem.median,
            scale_regul((stats.dem.qmax - stats.dem.qmin) / 2.0),
        )

        self.model.set_scales(
            MMDCShiftScales(
                shift_scale_s2,
                shift_scale_s1,
                shift_scale_meteo,
                shift_scale_dem,
            )
        )

    def on_fit_start(self) -> None:
        """On fit start, get the stats, and set them into the model"""
        logger.info("On fit start")
        assert hasattr(self.trainer, "datamodule")
        assert hasattr(self.trainer.datamodule, "get_stats")
        stats: MMDCDataStats = self.trainer.datamodule.get_stats()
        assert hasattr(self.trainer, "checkpoint_callback")
        assert self.trainer.checkpoint_callback is not None
        assert hasattr(self.trainer.checkpoint_callback, "dirpath")
        Path(self.trainer.checkpoint_callback.dirpath).mkdir(parents=True)
        torch.save(
            stats, os.path.join(self.trainer.checkpoint_callback.dirpath, "stats.pt")
        )
        logger.info(
            "Stats are saved to %s",
            self.trainer.checkpoint_callback.dirpath + "/stats.pt",
        )
        self.set_stats(stats)
        self.snap_model.set_snap_weights(freeze_model=True)

    def configure_optimizers(self) -> dict[str, Any]:
        """A single optimizer with a LR scheduler"""
        optimizer = torch.optim.Adam(
            params=filter(lambda p: p.requires_grad, self.model.parameters()),
            lr=self.learning_rate,
        )

        # training_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
        #     optimizer,
        #     mode="min",
        #     factor=self.lr_sched_factor,
        #     patience=self.lr_sched_patience,
        #     min_lr=self.lr_sched_min_lr,
        # )

        training_scheduler = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(
            optimizer, T_0=4, T_mult=2, eta_min=0, last_epoch=-1
        )
        scheduler = {
            "scheduler": training_scheduler,
            "interval": "epoch",
            "monitor": "val/loss",
            "frequency": 1,
        }
        return {
            "optimizer": optimizer,
            "lr_scheduler": scheduler,
        }
