#!/usr/bin/env python3
# Copyright: (c) 2022 CESBIO / Centre National d'Etudes Spatiales
""" Full MMDC Lightning module and Pytorch model"""
import logging
from typing import Any, cast

import torch

from ...datamodules.datatypes import ShiftScale
from ...datamodules.mmdc_datamodule import destructure_batch
from ...utils.filters import despeckle_batch
from ..components.losses import (
    compute_mmdc_nll,
    lai_loss,
    off_diagonal_covar_loss,
    std_gaussian_kld,
    sym_gaussian_kld,
)
from ..components.masking import DataMasking
from ..datatypes import (
    MMDCForwardType,
    PretrainedModel,
    S1FowardType,
    S1S2VAELosses,
    S1S2VAEMaskingLosses,
    S2FowardType,
)
from ..torch.full_experts import MMDCFullExpertsModule
from .full import MMDCFullLitModule

# Configure logging
NUMERIC_LEVEL = getattr(logging, "INFO", None)
logging.basicConfig(
    level=NUMERIC_LEVEL, format="%(asctime)-15s %(levelname)s: %(message)s"
)

logger = logging.getLogger(__name__)


class MMDCFullExpertsLitModule(MMDCFullLitModule):  # pylint: disable=too-many-ancestors
    """
    Lightning Module for the Full MMDC Single Date network
    """

    def __init__(
        self,
        model: MMDCFullExpertsModule,
        masking: DataMasking | None = None,
        lr: float = 0.001,
        resume_from_checkpoint: str | None = None,
        load_weights_from_checkpoint: PretrainedModel | None = None,
    ):
        super().__init__(
            model,
            masking,
            lr,
            resume_from_checkpoint,
            load_weights_from_checkpoint,
        )
        self.model: MMDCFullExpertsModule = model

    def step(
        self,
        batch: Any,
        masking_loss: bool = False,
    ) -> S1S2VAELosses | tuple[S1S2VAELosses, S1S2VAEMaskingLosses]:
        """Optimization step"""
        batch = destructure_batch(batch)

        forward_batch = batch.copy()
        if self.masking is not None:
            forward_batch = self.masking.mask_data(self.current_epoch, forward_batch)

        out = self.forward(
            MMDCForwardType(
                S2FowardType(
                    forward_batch.s2_x, forward_batch.s2_m, forward_batch.s2_a
                ),
                S1FowardType(
                    forward_batch.s1_x, forward_batch.s1_vm, forward_batch.s1_a
                ),
                forward_batch.meteo_x,
                forward_batch.dem_x,
            ),
        )

        del forward_batch

        s1_x = despeckle_batch(batch.s1_x)
        s2_x = batch.s2_x

        c_s = self.model.nb_cropped_hw
        assert self.model.scales is not None
        s1_data_for_loss = self.build_data_for_loss(
            s1_x,
            out.recons.s1_pred.mean,
            c_s,
            ShiftScale(self.model.scales.sen1.shift, self.model.scales.sen1.scale),
        )
        s2_data_for_loss = self.build_data_for_loss(
            s2_x,
            out.recons.s2_pred.mean,
            c_s,
            ShiftScale(self.model.scales.sen2.shift, self.model.scales.sen2.scale),
        )
        # We have to mask also the data that has been (un)standardized
        # because the zeros of the previously masked data are shifted

        loss_d1_e1_s1 = compute_mmdc_nll(
            x_mu=out.recons.s1_pred.mean,
            x_logvar=out.recons.s1_pred.logvar,
            y_mu=s1_data_for_loss.x_s,
            mask=batch.s1_vm,
            margin=c_s,
        )

        loss_d2_e2_s2 = compute_mmdc_nll(
            x_mu=out.recons.s2_pred.mean,
            x_logvar=out.recons.s2_pred.logvar,
            y_mu=s2_data_for_loss.x_s,
            mask=batch.s2_m,
            margin=c_s,
        )

        vae_losses = S1S2VAELosses(loss_d1_e1_s1, loss_d2_e2_s2)

        if self.model.config.loss_weights.latent.latent > 0:
            vae_losses.code = (
                std_gaussian_kld(  # fusion is N(0, 1)
                    out.latent_experts.mean,
                    torch.exp(out.latent_experts.logvar),
                    aggregate=True,
                )
                + sym_gaussian_kld(  # marginals are close between them
                    out.latent.sen1.mean,
                    out.latent.sen2.mean,
                    torch.exp(out.latent.sen1.logvar),
                    torch.exp(out.latent.sen2.logvar),
                )
                + sym_gaussian_kld(  # marginals are close to fusion
                    out.latent.sen1.mean,
                    out.latent_experts.mean,
                    torch.exp(out.latent.sen1.logvar),
                    torch.exp(out.latent_experts.logvar),
                )
                + sym_gaussian_kld(  # marginals are close to fusion
                    out.latent_experts.mean,
                    out.latent.sen2.mean,
                    torch.exp(out.latent_experts.logvar),
                    torch.exp(out.latent.sen2.logvar),
                )
            )

        if self.model.config.loss_weights.latent.latent_cov > 0:
            vae_losses.covar_code = off_diagonal_covar_loss(
                out.latent_experts.mean[
                    :,
                    :,
                    c_s:-c_s,
                    c_s:-c_s,
                ],
                max_variances=self.model.config.loss_weights.latent.latent_max_var,
            )

        if self.model.config.loss_weights.lai is not None:
            mask_s2_clipped = batch.s2_m[:, :, c_s:-c_s, c_s:-c_s]
            angles_s2_clipped = batch.s2_a[:, :, c_s:-c_s, c_s:-c_s]
            vae_losses.lai_direct = lai_loss(
                snap_model=self.snap_model,
                pred=s2_data_for_loss.x_mu_u,
                s2_set=s2_data_for_loss.x_u,
                s2_angles=angles_s2_clipped,
                s2_mask=mask_s2_clipped,
            )

        if masking_loss and self.masking:
            masking_losses = self.masking_loss(
                batch,
                s1_data_for_loss,
                s2_data_for_loss,
            )
            return vae_losses, masking_losses
        return vae_losses

    def training_step(  # pylint: disable=arguments-differ
        self,
        batch: Any,
        batch_idx: int,  # pylint: disable=unused-argument
    ) -> dict[str, Any]:
        """Training step. Step and return loss."""
        torch.autograd.set_detect_anomaly(True)
        vae_losses = cast(S1S2VAELosses, self.step(batch))

        for loss_name in vae_losses.__dict__.keys():
            loss_value = getattr(vae_losses, loss_name)
            if loss_value is not None:
                self.log(
                    f"train/{loss_name}",
                    loss_value,
                    on_step=False,
                    on_epoch=True,
                    prog_bar=False,
                )

        loss = self.global_loss(vae_losses)

        # log training metrics
        self.log(
            "train/loss",
            loss,
            on_step=False,
            on_epoch=True,
            prog_bar=False,
        )

        return {"loss": loss}

    def validation_step(  # pylint: disable=arguments-differ
        self,
        batch: Any,
        batch_idx: int,  # pylint: disable=unused-argument
        prefix: str = "val",
    ) -> dict[str, Any]:
        """Validation step. Step and return loss."""
        if self.masking:
            vae_losses, masking_losses = cast(
                tuple[S1S2VAELosses, S1S2VAEMaskingLosses],
                self.step(batch, masking_loss=True),
            )
            self.log(
                f"{prefix}/s1_rec_masking",
                masking_losses.sen1_rec,
                on_step=False,
                on_epoch=True,
                prog_bar=False,
            )
            self.log(
                f"{prefix}/s2_rec_masking",
                masking_losses.sen2_rec,
                on_step=False,
                on_epoch=True,
                prog_bar=False,
            )
        else:
            vae_losses = cast(S1S2VAELosses, self.step(batch))

        for loss_name in vae_losses.__dict__.keys():
            loss_value = getattr(vae_losses, loss_name)
            if loss_value is not None:
                self.log(
                    f"{prefix}/{loss_name}",
                    loss_value,
                    on_step=False,
                    on_epoch=True,
                    prog_bar=False,
                )

        loss = self.global_loss(vae_losses)

        # log val metrics
        self.log(
            f"{prefix}/loss",
            loss,
            on_step=False,
            on_epoch=True,
            prog_bar=False,
        )
        return {"loss": loss}

    def test_step(  # pylint: disable=arguments-differ
        self,
        batch: Any,
        batch_idx: int,  # pylint: disable=unused-argument
    ) -> dict[str, Any]:
        """Test step. Step and return loss. Delegate to validation step"""
        return self.validation_step(batch, batch_idx, prefix="test")

    def on_train_epoch_start(self) -> None:
        if self.masking is not None:
            if (strat := self.masking.get_strategy(self.current_epoch)) is not None:
                logger.info(
                    "Starting epoch %d with masking %s at %f ",
                    self.current_epoch,
                    strat[1],
                    strat[0],
                )
        logger.info("Ended validation epoch %s", self.current_epoch)
