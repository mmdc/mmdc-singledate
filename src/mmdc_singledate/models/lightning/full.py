# Copyright: (c) 2022 CESBIO / Centre National d'Etudes Spatiales
""" Full MMDC Lightning module and Pytorch model"""
import logging
from typing import Any, cast

import torch
from torchmetrics import MinMetric
from torchutils import metrics

from ...datamodules.datatypes import MMDCBatch, ShiftScale
from ...datamodules.mmdc_datamodule import destructure_batch
from ...utils.filters import despeckle_batch
from ...utils.train_utils import standardize_data, unstandardize_data
from ..components.losses import (
    compute_mmdc_nll,
    compute_sym_gaussian_kld,
    lai_loss,
    off_diagonal_covar_loss,
)
from ..components.masking import DataMasking
from ..datatypes import (
    MMDCForwardType,
    MMDCFullDataForLoss,
    PretrainedModel,
    S1FowardType,
    S1S2VAELosses,
    S1S2VAEMaskingLosses,
    S2FowardType,
)
from ..torch.full import MMDCFullModule
from .base import MMDCBaseLitModule

# Configure logging
NUMERIC_LEVEL = getattr(logging, "INFO", None)
logging.basicConfig(
    level=NUMERIC_LEVEL, format="%(asctime)-15s %(levelname)s: %(message)s"
)

logger = logging.getLogger(__name__)


class MMDCFullLitModule(MMDCBaseLitModule):  # pylint: disable=too-many-ancestors
    """
    Lightning Module for the Full MMDC Single Date network
    """

    def __init__(
        self,
        model: MMDCFullModule,
        masking: DataMasking | None = None,
        lr: float = 0.001,
        resume_from_checkpoint: str | None = None,
        load_weights_from_checkpoint: PretrainedModel | None = None,
    ):
        super().__init__(model, lr)

        # this line allows to access init params with 'self.hparams' attribute
        # it also ensures init params will be stored in ckpt
        self.save_hyperparameters(logger=False)
        self.model: MMDCFullModule = model
        self.resume_from_checkpoint = resume_from_checkpoint
        self.masking = masking

        # use separate metric instance for train, val and test step
        # to ensure a proper reduction over the epoch
        self.test_rmse = metrics.rmse

        # for logging best so far validation rmse
        self.val_rmse_best = MinMetric()

        # for loading pretrained layers
        self.pretrained_layers: list[str] = []
        if (
            load_weights_from_checkpoint is not None
            and load_weights_from_checkpoint.path is not None
        ):
            self.load_partial_model(load_weights_from_checkpoint)

    def load_partial_model(self, load_weights_from_checkpoint: PretrainedModel) -> None:
        """
        Load only weights from a model
        that only partially matches the current expert model
        """
        checkpoint = torch.load(load_weights_from_checkpoint.path)
        model_weights = checkpoint["state_dict"]
        for key in list(model_weights.keys()):
            model_weights[key.replace("model.", "")] = model_weights.pop(key)

        if load_weights_from_checkpoint.selected_layers in (None, "all"):
            self.model.load_state_dict(model_weights, strict=False)
        else:
            assert all(
                any(layer in key for key in model_weights)
                for layer in load_weights_from_checkpoint.selected_layers
            ), "check selected layers names"
            selected_weights = {}

            for key, weight in model_weights.items():
                if any(
                    layer in key
                    for layer in load_weights_from_checkpoint.selected_layers
                ):
                    selected_weights[key] = weight
            self.model.load_state_dict(selected_weights, strict=False)

            if load_weights_from_checkpoint.freeze_epochs not in (None, 0):
                self.pretrained_layers = list(
                    load_weights_from_checkpoint.selected_layers
                )

    @staticmethod
    def build_data_for_loss(
        in_x: torch.Tensor,
        out_x: torch.Tensor,
        c_s: int,
        scales: ShiftScale,
    ) -> MMDCFullDataForLoss:
        """Crop and standardize data as needed for loss computation"""
        sen_x = in_x[
            :,
            :,
            c_s:-c_s,
            c_s:-c_s,
        ]
        sen_s = standardize_data(
            in_x,
            scales.shift,
            scales.scale,
        )

        sen_s = sen_s[
            :,
            :,
            c_s:-c_s,
            c_s:-c_s,
        ]
        mu_sen_s = out_x
        mu_sen = unstandardize_data(
            mu_sen_s,
            scales.shift,
            scales.scale,
        )
        mu_sen_s = mu_sen_s[
            :,
            :,
            c_s:-c_s,
            c_s:-c_s,
        ]
        mu_sen = mu_sen[
            :,
            :,
            c_s:-c_s,
            c_s:-c_s,
        ]
        return MMDCFullDataForLoss(sen_x, sen_s, mu_sen, mu_sen_s)

    def compute_losses(
        self, batch: Any
    ) -> tuple[MMDCFullDataForLoss, MMDCFullDataForLoss, S1S2VAELosses]:
        """Helper method for computing losses"""

        forward_batch = batch.copy()
        if self.masking is not None:
            forward_batch = self.masking.mask_data(self.current_epoch, forward_batch)

        out = self.forward(
            MMDCForwardType(
                S2FowardType(
                    forward_batch.s2_x, forward_batch.s2_m, forward_batch.s2_a
                ),
                S1FowardType(
                    forward_batch.s1_x, forward_batch.s1_vm, forward_batch.s1_a
                ),
                forward_batch.meteo_x,
                forward_batch.dem_x,
            ),
        )

        del forward_batch

        c_s = self.model.nb_cropped_hw
        assert self.model.scales is not None
        s1_data_for_loss = self.build_data_for_loss(
            despeckle_batch(batch.s1_x),
            out.recons.s1_pred.mean,
            c_s,
            ShiftScale(self.model.scales.sen1.shift, self.model.scales.sen1.scale),
        )
        s2_data_for_loss = self.build_data_for_loss(
            batch.s2_x,
            out.recons.s2_pred.mean,
            c_s,
            ShiftScale(self.model.scales.sen2.shift, self.model.scales.sen2.scale),
        )
        # We have to mask also the data that has been (un)standardized
        # because the zeros of the previously masked data are shifted

        loss_d1_e1_s1 = compute_mmdc_nll(
            x_mu=out.recons.s1_pred.mean,
            x_logvar=out.recons.s1_pred.logvar,
            y_mu=s1_data_for_loss.x_s,
            mask=batch.s1_vm,
            margin=c_s,
        )

        loss_d2_e2_s2 = compute_mmdc_nll(
            x_mu=out.recons.s2_pred.mean,
            x_logvar=out.recons.s2_pred.logvar,
            y_mu=s2_data_for_loss.x_s,
            mask=batch.s2_m,
            margin=c_s,
        )

        loss_d1_e2_s2 = compute_mmdc_nll(
            x_mu=out.recons.s1_cross.mean,
            x_logvar=out.recons.s1_cross.logvar,
            y_mu=s1_data_for_loss.x_s,
            mask=batch.s1_vm,
            margin=c_s,
        )

        loss_d2_e1_s1 = compute_mmdc_nll(
            x_mu=out.recons.s2_cross.mean,
            x_logvar=out.recons.s2_cross.logvar,
            y_mu=s2_data_for_loss.x_s,
            mask=batch.s2_m,
            margin=c_s,
        )

        loss_code_s1s2 = compute_sym_gaussian_kld(
            x_mu=out.latent.sen1.mean,
            x_logvar=out.latent.sen1.logvar,
            y_mu=out.latent.sen2.mean,
            y_logvar=out.latent.sen2.logvar,
            margin=c_s,
        )

        cov_lat_s1 = off_diagonal_covar_loss(
            out.latent.sen1.mean[
                :,
                :,
                c_s:-c_s,
                c_s:-c_s,
            ],
            max_variances=self.model.config.loss_weights.latent.latent_max_var,
        )
        cov_lat_s2 = off_diagonal_covar_loss(
            out.latent.sen2.mean[
                :,
                :,
                c_s:-c_s,
                c_s:-c_s,
            ],
            max_variances=self.model.config.loss_weights.latent.latent_max_var,
        )

        mask_s2_clipped = batch.s2_m[:, :, c_s:-c_s, c_s:-c_s]
        s2_cross_u = unstandardize_data(
            out.recons.s2_cross.mean,
            self.model.scales.sen2.shift,
            self.model.scales.sen2.scale,
        )[:, :, c_s:-c_s, c_s:-c_s]
        angles_s2_clipped = batch.s2_a[:, :, c_s:-c_s, c_s:-c_s]

        loss_lai_direct = lai_loss(
            snap_model=self.snap_model,
            pred=s2_data_for_loss.x_mu_u,
            s2_set=s2_data_for_loss.x_u,
            s2_angles=angles_s2_clipped,
            s2_mask=mask_s2_clipped,
        )
        loss_lai_cross = lai_loss(
            snap_model=self.snap_model,
            pred=s2_cross_u,
            s2_set=s2_data_for_loss.x_u,
            s2_angles=angles_s2_clipped,
            s2_mask=mask_s2_clipped,
        )
        # logger.info(loss_lai_direct)
        # logger.info(loss_lai_cross)
        return (
            s1_data_for_loss,
            s2_data_for_loss,
            S1S2VAELosses(
                loss_d1_e1_s1,
                loss_d2_e2_s2,
                loss_d1_e2_s2,
                loss_d2_e1_s1,
                loss_code_s1s2,
                (cov_lat_s1 + cov_lat_s2),
                loss_lai_direct,
                loss_lai_cross,
            ),
        )

    def step(
        self,
        batch: Any,
        masking_loss: bool = False,
    ) -> S1S2VAELosses | tuple[S1S2VAELosses, S1S2VAEMaskingLosses]:
        """Optimization step"""
        batch = destructure_batch(batch)

        s1_data_for_loss, s2_data_for_loss, vae_losses = self.compute_losses(batch)

        if masking_loss:
            masking_losses = self.masking_loss(
                batch,
                s1_data_for_loss,
                s2_data_for_loss,
            )
            return vae_losses, masking_losses
        return vae_losses

    def masking_loss(
        self,
        batch: MMDCBatch,
        s1_data_for_loss: MMDCFullDataForLoss,
        s2_data_for_loss: MMDCFullDataForLoss,
    ) -> S1S2VAEMaskingLosses:
        """Loss to check if model quality is not degrading during training"""
        with torch.no_grad():
            out = self.forward(
                MMDCForwardType(
                    S2FowardType(batch.s2_x, batch.s2_m, batch.s2_a),
                    S1FowardType(batch.s1_x, batch.s1_vm, batch.s1_a),
                    batch.meteo_x,
                    batch.dem_x,
                ),
            )
        c_s = self.model.nb_cropped_hw

        loss_d1_e1_s1_masking = compute_mmdc_nll(
            x_mu=out.recons.s1_pred.mean,
            x_logvar=out.recons.s1_pred.logvar,
            y_mu=s1_data_for_loss.x_s,
            mask=batch.s1_vm,
            margin=c_s,
        )
        loss_d2_e2_s2_masking = compute_mmdc_nll(
            x_mu=out.recons.s2_pred.mean,
            x_logvar=out.recons.s2_pred.logvar,
            y_mu=s2_data_for_loss.x_s,
            mask=batch.s2_m,
            margin=c_s,
        )

        return S1S2VAEMaskingLosses(loss_d1_e1_s1_masking, loss_d2_e2_s2_masking)

    def training_step(  # pylint: disable=arguments-differ
        self,
        batch: Any,
        batch_idx: int,  # pylint: disable=unused-argument
    ) -> dict[str, Any]:
        """Training step. Step and return loss."""
        torch.autograd.set_detect_anomaly(True)
        vae_losses = self.step(batch)
        assert isinstance(vae_losses, S1S2VAELosses)

        for loss_name in vae_losses.__dict__.keys():
            loss_value = getattr(vae_losses, loss_name)
            if loss_value is not None:
                self.log(
                    f"train/{loss_name}",
                    loss_value,
                    on_step=False,
                    on_epoch=True,
                    prog_bar=False,
                )

        loss = self.global_loss(vae_losses)

        # log training metrics
        self.log(
            "train/loss",
            loss,
            on_step=False,
            on_epoch=True,
            prog_bar=False,
        )

        return {"loss": loss}

    def validation_step(  # pylint: disable=arguments-differ
        self,
        batch: Any,
        batch_idx: int,  # pylint: disable=unused-argument
        prefix: str = "val",
    ) -> dict[str, Any]:
        """Validation step. Step and return loss."""
        if self.masking:
            vae_losses, masking_losses = cast(
                tuple[S1S2VAELosses, S1S2VAEMaskingLosses],
                self.step(batch, masking_loss=True),
            )
            self.log(
                f"{prefix}/s1_rec_masking",
                masking_losses.sen1_rec,
                on_step=False,
                on_epoch=True,
                prog_bar=False,
            )
            self.log(
                f"{prefix}/s2_rec_masking",
                masking_losses.sen2_rec,
                on_step=False,
                on_epoch=True,
                prog_bar=False,
            )
        else:
            vae_losses = cast(S1S2VAELosses, self.step(batch))

        for loss_name in vae_losses.__dict__.keys():
            loss_value = getattr(vae_losses, loss_name)
            if loss_value is not None:
                self.log(
                    f"{prefix}/{loss_name}",
                    loss_value,
                    on_step=False,
                    on_epoch=True,
                    prog_bar=False,
                )

        loss = self.global_loss(vae_losses)

        # log val metrics
        self.log(
            f"{prefix}/loss",
            loss,
            on_step=False,
            on_epoch=True,
            prog_bar=False,
        )
        return {"loss": loss}

    def test_step(  # pylint: disable=arguments-differ
        self,
        batch: Any,
        batch_idx: int,  # pylint: disable=unused-argument
    ) -> dict[str, Any]:
        """Test step. Step and return loss. Delegate to validation step"""
        return self.validation_step(batch, batch_idx, prefix="test")

    def forward(self, data: MMDCForwardType) -> Any:
        """Forward function"""
        return self.model.forward(data)

    def on_fit_start(self) -> None:
        """On fit start, get the stats, and set them into the model"""
        super().on_fit_start()
        if self.masking is not None:
            self.masking.scales = self.model.scales

    def global_loss(self, losses: S1S2VAELosses) -> torch.Tensor:
        """
        Computes the model's global loss by summing
        all losses with a specified weight
        """
        global_loss = self.model.config.loss_weights.forward * (
            losses.s1_rec + losses.s2_rec
        )

        if (
            losses.s1_cross is not None
            and self.model.config.loss_weights.cross is not None
        ):
            global_loss += self.model.config.loss_weights.cross * losses.s1_cross

        if (
            losses.s2_cross is not None
            and self.model.config.loss_weights.cross is not None
        ):
            global_loss += self.model.config.loss_weights.cross * losses.s2_cross

        if losses.code is not None and self.model.config.loss_weights.latent.latent > 0:
            global_loss += self.model.config.loss_weights.latent.latent * losses.code

        if (
            losses.covar_code is not None
            and self.model.config.loss_weights.latent.latent_cov > 0
        ):
            global_loss += (
                self.model.config.loss_weights.latent.latent_cov * losses.covar_code
            )

        if (
            losses.lai_direct is not None
            and self.model.config.loss_weights.lai is not None
        ):
            global_loss += self.model.config.loss_weights.lai * losses.lai_direct

        if (
            losses.lai_cross is not None
            and self.model.config.loss_weights.lai is not None
        ):
            global_loss += self.model.config.loss_weights.lai * losses.lai_cross

        return global_loss
