"""Script to compute bio-physical variables"""
import torch


def prepare_s2_image(
    s2_set: torch.Tensor,
    s2_angles: torch.Tensor,
    reshape: bool = True,
) -> torch.Tensor:
    """
    Prepare input data for BVNET model
    S2 bands that we select :
    B03, B04, B05, B06, B07, B8A, B11, B12

    S2 bands we initially have in the image set:
    B02, B03, B04, B05, B06, B07, B08, B8A, B11, B12

    S2 angles we need to compute LAI:
    cos Z_s2, cos Z_sun, cos(A_sun-A_s2)

    S2 angles we initially have in the set:
    cos Z_sun, cos A_sun, sin A_sun, cos Z_s2, cos A_s2, sin A_s2

    Therefore, we compute cos(A_sun-A_s2) using the formula:
    cos(A-B) = cosA*cosB - sinA*sinB
    """

    s2_set_sel = s2_set[:, [1, 2, 3, 4, 5, 7, 8, 9]]
    s2_angles_sel = s2_angles[:, [3, 0]]

    # Compute cos(A_sun-A_s2)
    cos_diff = s2_angles[:, 1] * s2_angles[:, 4] - s2_angles[:, 2] * s2_angles[:, 5]

    input_set = torch.cat((s2_set_sel, s2_angles_sel, cos_diff[:, None, :, :]), 1)
    if not reshape:
        return input_set

    # We transform to get input shape (B, 11)
    return input_set.permute(0, 2, 3, 1).reshape(-1, 11)


def process_output(
    output_set: torch.Tensor, output_size: torch.Size, mask: torch.Tensor = None
) -> torch.Tensor:
    """
    Transforms output data from shape (B,) to (B, W, H)
    and sets masked values to nan
    """
    output = output_set.reshape(output_size)
    if mask is not None:
        output[mask.bool()] = torch.nan
    return output


def stand_lai(
    value: torch.Tensor,
    mean: int = 3,
    std: int = 3,
) -> torch.Tensor:
    """Standardize LAI"""
    return (value.clip(0, 15) - mean) / std


def unstand_lai(
    value: torch.Tensor,
    mean: int = 3,
    std: int = 3,
) -> torch.Tensor:
    """Unstandardize LAI"""
    return value * std + mean
