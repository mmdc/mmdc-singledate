# /usr/bin/env python3
# Copyright: (c) 2023 CESBIO / Centre National d'Etudes Spatiales
"""Training masking module"""

import logging
from collections.abc import Sequence
from typing import Literal, TypeAlias

import numpy as np
import torch

from ...datamodules.datatypes import MMDCShiftScales
from ...datamodules.mmdc_datamodule import MMDCBatch
from ...utils.typing import assert_never

logger = logging.getLogger(__name__)


MaskingStrategy: TypeAlias = Literal["random", "one_satellite", "one_satellite_random"]
SatMasking: TypeAlias = Literal["S1", "S2", "both", "both_one_per_sample"]


class DataMasking:
    """
    Class for data masking during training
    Attributes:
        epochs_thr_list - epochs list that defines when strategy is changed
        masked_perc_list - list with percentage of bands to mask
        strategy_list: masking strategy, to choose from
            "random" - random bands masking of both satellites in the batch
            "one_satellite" - only one satellite is masked in the batch
            "one_satellite_random" - one satellite is masked within a sample,
                                     but different satellites within a batch
        scales: dataset mean/std for data standardization.
                Value set to this attribute in on_fit_start() in lightning module,
                right after stats are computed and appended to model.
    """

    def __init__(
        self,
        epochs_thr_list: Sequence[int],
        masked_perc_list: Sequence[float],
        strategy_list: Sequence[MaskingStrategy],
    ):
        self.epochs_thr_list = np.asarray(epochs_thr_list)
        self.masked_perc_list = np.asarray(masked_perc_list)
        self.strategy_list = np.asarray(strategy_list)
        self.scales: MMDCShiftScales | None = None

        assert (
            len(self.epochs_thr_list)
            == len(self.masked_perc_list)
            == len(self.strategy_list)
        ), "Masking lists are not of the same length"

        for strat in self.strategy_list:
            assert strat in ("random", "one_satellite", "one_satellite_random"), (
                f"{strat} strategy does not exist. "
                f"Choose from 'random', 'one_satellite', 'one_satellite_random'"
            )

        for mask_perc in self.masked_perc_list:
            assert (
                1 <= mask_perc <= 100 or mask_perc == 0
            ), "percentage should be comprised between 1 and 100%, or be 0"

    def get_strategy(
        self,
        current_epoch: int,
    ) -> tuple[float, MaskingStrategy] | None:
        """
        Get training strategy and masking percentage
        knowing the current epoch nb
        """
        if current_epoch < self.epochs_thr_list[0]:
            return None

        # We interpolate for epoch located between epochs from list
        masked_perc = np.interp(
            current_epoch,
            self.epochs_thr_list,
            self.masked_perc_list,
            right=self.masked_perc_list[-1],
        )
        assert isinstance(masked_perc, float)
        strategy = self.strategy_list[
            np.where(current_epoch >= self.epochs_thr_list)[0][-1]
        ]
        return masked_perc, strategy

    def mask_data(
        self,
        current_epoch: int,
        batch: MMDCBatch,
    ) -> MMDCBatch:
        """
        Applies mask to image
        """
        strat = self.get_strategy(current_epoch)
        if strat is None:
            return batch
        masked_perc, strategy = strat
        if masked_perc == 0:
            return batch
        match strategy:
            case "random":
                return self.mask_both(batch, masked_perc)
            case "one_satellite":
                if np.random.randint(2):  # 1 - S1, 0 - S2:
                    return self.mask_s1_only(batch, masked_perc)
                return self.mask_s2_only(batch, masked_perc)
            case "one_satellite_random":
                return self.mask_both(batch, masked_perc, one_sat_per_sample=True)
            case _:
                assert_never(strategy)

    def mask_s1_only(self, batch: MMDCBatch, masked_perc: float) -> MMDCBatch:
        """Mask S1 images in the batch only"""
        batch.s1_x, s1_mask = self.apply_mask(masked_perc, batch.s1_x, "S1")
        s1_angles_to_mask_asc = self.angles_check_and_mask(
            s1_mask[:, :3], batch.s1_a[:, :3]
        )
        s1_angles_to_mask_desc = self.angles_check_and_mask(
            s1_mask[:, 3:], batch.s1_a[:, 3:]
        )
        batch.s1_a = torch.cat((s1_angles_to_mask_asc, s1_angles_to_mask_desc), 1)
        return batch

    def mask_s2_only(self, batch: MMDCBatch, masked_perc: float) -> MMDCBatch:
        """Mask S2 images in the batch only"""
        batch.s2_x, s2_mask = self.apply_mask(masked_perc, batch.s2_x, "S2")
        batch.s2_a = self.angles_check_and_mask(s2_mask, batch.s2_a)
        return batch

    def mask_both(
        self, batch: MMDCBatch, masked_perc: float, one_sat_per_sample: bool = False
    ) -> MMDCBatch:
        """
        Mask bands in both S1 and S2 images within a batch.
        We concatenate both S1 and S2 images and mask bands.

        If one_sat_per_img is True, only one satellite within
        a batch sample is masked, but both satellites are masked within batch.
        If False, we randomly mask both S1 and S2 images within a batch sample.

        We ignore that S1 asc or desc might be unavailable and consider all data valid.
        If all bands of one satellite are masked, we also mask its angles.
        """
        img_to_mask = torch.cat((batch.s1_x, batch.s2_x), 1)
        if one_sat_per_sample:
            masked_image, random_mask = self.apply_mask(
                masked_perc,
                img_to_mask,
                "both_one_per_sample",
                bands_both=(batch.s1_x.shape[1], batch.s2_x.shape[1]),
            )
        else:
            masked_image, random_mask = self.apply_mask(
                masked_perc, img_to_mask, "both"
            )
        batch.s1_x, batch.s2_x = (
            masked_image[:, : batch.s1_x.shape[1]],
            masked_image[:, batch.s1_x.shape[1] :],
        )
        s1_mask, s2_mask = (
            random_mask[:, : batch.s1_x.shape[1]],
            random_mask[:, batch.s1_x.shape[1] :],
        )
        # We check if all bands of one sensor is masked for each batch element
        # If true, we mask corresponding angles
        s1_angles_to_mask_asc = self.angles_check_and_mask(
            s1_mask[:, :3], batch.s1_a[:, :3]
        )
        s1_angles_to_mask_desc = self.angles_check_and_mask(
            s1_mask[:, 3:], batch.s1_a[:, 3:]
        )
        batch.s1_a = torch.cat((s1_angles_to_mask_asc, s1_angles_to_mask_desc), 1)
        batch.s2_a = self.angles_check_and_mask(s2_mask, batch.s2_a)
        return batch

    @staticmethod
    def get_mask(batch_size: int, bands: int, masked_bands_nb: int) -> torch.Tensor:
        """
        Creates a random mask given batch size, bands number and number of bands to mask
        Returns a bool tensor of size (B, N)
        """
        mask = (
            torch.stack(
                [
                    torch.hstack(
                        (
                            torch.ones(masked_bands_nb),
                            torch.zeros(bands - masked_bands_nb),
                        )
                    )[torch.randperm(bands)]
                    for _ in range(batch_size)
                ]
            )
            > 0
        )
        return mask

    def create_mask(
        self, batch_size: int, bands: int | tuple[int, int], masked_perc: float
    ) -> torch.Tensor:
        """
        Creates a boolean mask, given batch size, number of data bands,
        and masked_perc - percentage of bands to mask.

        If bands parameter is tuple [S1 bands number, S2 bands number],
        we mask only one satellite within a sample ("one_satellite_random").
        Otherwise, if it is int, any band of a batch sample can be masked.
        """
        match bands:
            case int():
                masked_bands_nb: int = round(bands * masked_perc / 100)
                return self.get_mask(batch_size, bands, masked_bands_nb)
            case tuple():
                masked_bands_nbs: np.ndarray = np.round(
                    np.array(bands) * masked_perc / 100
                )
                # We create masks for each satellite separately
                mask_s1 = self.get_mask(batch_size, bands[0], int(masked_bands_nbs[0]))
                mask_s2 = self.get_mask(batch_size, bands[1], int(masked_bands_nbs[1]))
                # We complete them with False values,
                # so both masks are the same shape (BATCH, Bands[0]+Bands[1])
                mask_s1 = torch.cat(
                    (mask_s1, torch.zeros(batch_size, bands[1]).bool()), 1
                )
                mask_s2 = torch.cat(
                    (torch.zeros(batch_size, bands[0]).bool(), mask_s2), 1
                )
                # We randomly choose which satellite will be masked for each patch
                randomly_choose_sat = (
                    torch.randint(low=0, high=2, size=[batch_size]) > 0
                )
                return torch.cat(
                    (mask_s1[randomly_choose_sat], mask_s2[~randomly_choose_sat]), 0
                )
            case _:
                assert_never(bands)

    def apply_mask(
        self,
        masked_perc: float,
        img: torch.Tensor,
        satellite: SatMasking,
        bands_both: tuple[int, int] | None = None,
    ) -> tuple[torch.Tensor, torch.Tensor]:
        """
        Create and apply mask to data.
        Masked bands are replaced with random values from Gaussian distribution
        with mean, std from dataset stats for each band.

        If bands_both parameter is given, we mask only one satelite within a sample.

        masked_perc: percent of bands to mask
        img: the image to mask, can contain only S1 data, only S2 data,
        or concatenated S1 and S2 data if both satellites are masked within a batch
        satellite: type of data we want to mask:
            "S1" or "S2" - only one satellite data is provided to be masked in a batch
            "both" - both S1 and S2 are masked within a batch and within a sample
            "both_one_per_sample" - both S1 and S2 are masked within a batch,
                                    but only one satellite within a sample
        bands_both: only used when satellite="both_one_per_sample"
                    and defines nb of bands for each satellite [bands_nb_S1,bands_nb_S2]
        """
        batch, bands, height, width = img.shape
        # We randomly choose bands to mask
        if satellite == "both_one_per_sample":
            assert bands_both is not None
            mask = self.create_mask(batch, bands_both, masked_perc)
        else:
            mask = self.create_mask(batch, bands, masked_perc)
        img[mask] = self.get_gaussian_bands([batch, height, width], satellite).to(
            img.device
        )[mask]
        return img, mask

    def get_gaussian_bands(
        self,
        size: list[int],
        satellite: SatMasking,
    ) -> torch.Tensor:
        """
        Knowing mean/std stats for S1 and/or S2,
        we generate bands with gaussian distribution
        of the same size as the image to mask
        """
        assert self.scales is not None, "Scales are not set"
        if "both" not in satellite:  # if only one satellite is masked
            scales = self.scales.sen1 if satellite == "S1" else self.scales.sen2
            means, stds = scales.shift, scales.scale
        else:  # both satellites are masked, stack stats
            means = torch.hstack((self.scales.sen1.shift, self.scales.sen2.shift))
            stds = torch.hstack((self.scales.sen1.scale, self.scales.sen2.scale))
        return torch.stack(
            [
                torch.normal(mean=mean, std=std, size=size)
                for mean, std in zip(*means, *stds, strict=True)
            ],
            1,
        )

    @staticmethod
    def angles_check_and_mask(mask: torch.Tensor, angles: torch.Tensor) -> torch.Tensor:
        """
        We check if all bands of one modality (S1 asc, S1 desc or S2)
        are masked within a batch sample.
        If True, we mask the angles of correponding batch samples.
        """
        # Check what images from batch are fully masked
        # If mask mean values per sample are 1, all bands of sample are masked
        angles_to_mask = mask.float().mean(1) == 1
        fully_masked_nb = int(
            angles_to_mask.sum()
        )  # nb of fully masked samples in batch
        if fully_masked_nb > 0:
            angles[angles_to_mask] = torch.normal(
                mean=0.0, std=1.0, size=(fully_masked_nb, *angles.shape[1:])
            ).to(angles.device)
        return angles
