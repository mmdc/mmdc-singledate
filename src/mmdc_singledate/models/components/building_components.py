""" Building blocks for MMDC networks """
import logging
from dataclasses import dataclass
from itertools import chain, starmap

import torch
import torch.nn.functional as F
from torch import nn

from ..datatypes import ConfigWithSizesAndEmbeddings, VAELatentSpace

NUMERIC_LEVEL = getattr(logging, "INFO", None)
logging.basicConfig(
    level=NUMERIC_LEVEL, format="%(asctime)-15s %(levelname)s: %(message)s"
)
logger = logging.getLogger(__name__)


class Encoder(nn.Module):
    """
    Implements an encoder with alternate
    convolutional and Relu layers
    """

    def __init__(self, encoder_sizes: list[int], enc_kernel_sizes: list[int]):
        """
        Constructor

        :param encoder_sizes: Number of features for each layer
        :param enc_kernel_sizes: List of kernel sizes for each layer
        """

        super().__init__()

        enc_blocks: list[nn.Module] = list(
            chain.from_iterable(
                [
                    [
                        nn.Conv2d(
                            in_layer,
                            out_layer,
                            kernel_size=kernel_size,
                            padding="same",
                        ),
                        nn.ReLU(),
                    ]
                    for in_layer, out_layer, kernel_size in zip(
                        encoder_sizes, encoder_sizes[1:], enc_kernel_sizes, strict=True
                    )
                ]
            )
        )
        self.encoder = nn.Sequential(*enc_blocks)
        # we abuse sum to concatenate lists of layers
        self.nb_enc_cropped_hw = sum(  # pylint: disable=R1728
            [(kernel_size - 1) // 2 for kernel_size in enc_kernel_sizes]
        )
        self.mu_conv = nn.Conv2d(encoder_sizes[-1], encoder_sizes[-1], kernel_size=1)
        self.logvar_conv = nn.Conv2d(
            encoder_sizes[-1], encoder_sizes[-1], kernel_size=1
        )

    def forward(self, data: torch.Tensor) -> VAELatentSpace:
        """
        Forward pass of the convolutionnal encoder

        :param x: Data tensor of shape [N,C_in,H,W]

        :return: Output Dataclass that holds mu and var
                 tensors of shape [N,C_out,H,W]
        """

        mu_out = torch.tanh(self.mu_conv(self.encoder(data)))
        logvar_out = torch.tanh(self.logvar_conv(self.encoder(data)))

        return VAELatentSpace(mu_out, logvar_out)


def conv_batch_norm(
    in_channels: int, out_channels: int, kernel_size: int
) -> nn.Sequential:
    """
    Creates a sequential block consisting of
    Convolution, Instance Normalization, ReLU

    :param in_channels: Number of channels of input
    :param out_channels: Number of channels of output
    :param kernel_size: Size of the convolution kernel
    """

    return nn.Sequential(
        nn.Conv2d(in_channels, out_channels, kernel_size=kernel_size, padding="same"),
        nn.BatchNorm2d(out_channels, track_running_stats=True),
    )


class ResidualBlock(nn.Module):
    """
    Implements a basic residual block with
    skip connections found in ResNet architecture
    """

    def __init__(self, in_channels: int, out_channels: int, kernel_size: int) -> None:
        """
        Constructor

        :param in_channels: Number of channels of input
        :param out_channels: Number of channels of output
        :param kernel_size: Kernel size for all layers
        """
        super().__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.kernel_size = kernel_size
        self.block = nn.Sequential(
            conv_batch_norm(self.in_channels, self.out_channels, kernel_size),
            nn.ReLU(),
            conv_batch_norm(self.out_channels, self.out_channels, kernel_size),
        )
        self.shortcut = conv_batch_norm(self.in_channels, self.out_channels, 1)

    def forward(self, data: torch.Tensor) -> torch.Tensor:
        """
        Forward pass of the residual block

        :param x: Data tensor of shape [N,C_in,H,W]

        :return: Output tensor of shape [N,C_out,H,W]
        """
        residual = data
        if self.should_apply_shortcut:
            residual = self.shortcut(data)
        data = self.block(data)
        data = F.relu(data)
        data = data + residual  # in place add (+=) causes backprop issues
        data = F.relu(data)

        return data

    @property
    def should_apply_shortcut(self) -> bool:
        """Only apply shortcut if nb of input and output channels are
        equal"""
        return self.in_channels != self.out_channels


class ResidualEncoder(nn.Module):
    """
    Implements an encoder composed of
    basic residual blocks
    """

    def __init__(self, encoder_sizes: list[int], enc_kernel_sizes: list[int]):
        """
        Constructor

        :param encoder_sizes: Number of features for each layer
        :param enc_kernel_sizes: List of kernel sizes for each layer
        """
        super().__init__()

        assert len(encoder_sizes) >= len(
            enc_kernel_sizes
        ), f"{len(encoder_sizes) = }, {len(enc_kernel_sizes) = }"

        enc_blocks = list(
            starmap(
                ResidualBlock,
                zip(encoder_sizes, encoder_sizes[1:], enc_kernel_sizes, strict=True),
            )
        )
        self.encoder = nn.Sequential(*enc_blocks)
        # we abuse sum to concatenate lists of layers
        self.nb_enc_cropped_hw = sum(  # pylint: disable=R1728
            [(kernel_size - 1) // 2 for kernel_size in enc_kernel_sizes]
        )
        self.mu_conv = nn.Conv2d(encoder_sizes[-1], encoder_sizes[-1], kernel_size=1)
        self.logvar_conv = nn.Conv2d(
            encoder_sizes[-1], encoder_sizes[-1], kernel_size=1
        )

    def forward(self, data: torch.Tensor) -> VAELatentSpace:
        """
        Forward pass of the Residual encoder

        :param x: Data tensor of shape [N,C_in,H,W]

        :return: Output Dataclass that holds mu and var
                 tensors of shape [N,C_out,H,W]
        """

        encoded = self.encoder(data)
        mu_out = torch.tanh(self.mu_conv(encoded))
        logvar_out = torch.tanh(self.logvar_conv(encoded))

        return VAELatentSpace(mu_out, logvar_out)


class UnetBlock(nn.Module):
    """
    Implements a basic Unet block
    """

    def __init__(self, in_channels: int, out_channels: int, kernel_size: int):
        """
        Constructor

        :param in_channels: Number of channels of input
        :param out_channels: Number of channels of output
        :param kernel_size: Kernel size for all layers
        """
        super().__init__()
        self.block = nn.Sequential(
            conv_batch_norm(in_channels, out_channels, kernel_size),
            nn.ReLU(),
            conv_batch_norm(out_channels, out_channels, kernel_size),
            nn.ReLU(),
        )

    def forward(self, data: torch.Tensor) -> torch.Tensor:
        """
        Forward pass of the convolutionnal encoder

        :param x: Input tensor of shape [N,C_in,H,W]

        :return: Output tensor of shape [N,C_out,H,W]
        """
        data = self.block(data)
        return data


class UnetEncoder(nn.Module):
    """
    Implements the contracting path of
    the Unet (Unet Encoder)
    """

    def __init__(self, encoder_sizes: list[int], kernel_size: int = 3):
        """
        Constructor

        :param encoder_sizes: Number of features for each Unet blocks
        :param kernel_size: Kernel size for all layer
        """
        super().__init__()
        self.enc_blocks = nn.Sequential(
            *[
                UnetBlock(in_layer, out_layer, kernel_size)
                for in_layer, out_layer in zip(
                    encoder_sizes[:-1], encoder_sizes[1:], strict=True
                )
            ]
        )
        self.pooling = nn.MaxPool2d(2)
        enc_layers = len(encoder_sizes[1:])
        self.nb_enc_cropped_hw = int((enc_layers / 2) * (kernel_size - 1))

    def forward(self, data: torch.Tensor) -> list[torch.Tensor]:
        """
        Forward pass of the Unet encoder

        :param x: Input tensor of shape [N,C_in,H,W]

        :return: List of tensor computed by each block saved as
                 skip connection feature maps for the decoder part
        """
        skips = []
        for block in self.enc_blocks:
            data = block(data)
            skips.append(data)
            data = self.pooling(data)

        return skips


class UnetDecoder(nn.Module):
    """
    Implements the expansive path of
    the Unet (Unet Decoder)
    """

    def __init__(self, decoder_sizes: list[int], kernel_size: int = 3):
        """
        Constructor

        :param decoder_sizes: Number of features for each Unet blocks
        :param kernel_size: Kernel size for all layer
        """
        super().__init__()
        self.decoder_sizes = decoder_sizes
        self.upconvs = nn.Sequential(
            *[
                nn.ConvTranspose2d(in_layer, out_layer, 2, 2)
                for in_layer, out_layer in zip(
                    decoder_sizes[:-1], decoder_sizes[1:], strict=True
                )
            ]
        )
        self.dec_blocks = nn.Sequential(
            *[
                UnetBlock(in_layer, out_layer, kernel_size)
                for in_layer, out_layer in zip(
                    decoder_sizes[:-1], decoder_sizes[1:], strict=True
                )
            ]
        )
        nb_layers = len(decoder_sizes)
        self.nb_dec_cropped_hw = nb_layers * 2 * (kernel_size - 1)

    def forward(
        self, data: torch.Tensor, encoder_features: list[torch.Tensor]
    ) -> torch.Tensor:
        """
        Forward pass of the Unet decoder

        :param x: Input tensor of shape [N,C_in,H,W]
        :param encoder_features: skip connection feature maps computed
                                 in the encoder part

        :return: Output tensor of shape [N,C_out,H,W]
        """
        for i in range(len(self.decoder_sizes) - 1):
            data = self.upconvs[i](data)
            data = torch.cat([data, encoder_features[i]], dim=1)
            data = self.dec_blocks[i](data)

        return data


class Unet(nn.Module):
    """Implements the Unet module"""

    def __init__(
        self,
        encoder_sizes: list[int],
        kernel_size: int = 3,
        tail_layers: int = 3,
    ):
        """
        Constructor

        :param encoder_sizes: Number of features for each Unet blocks
        :param kernel_size: Kernel size for all layer
        """
        super().__init__()
        self.encoder = UnetEncoder(encoder_sizes[:-1], kernel_size)
        # Decoder sizes are the reverse of encoder sizes
        # First element of encoder sizes is input size
        # Last element of encoder sizes is output size
        self.decoder = UnetDecoder(encoder_sizes[::-1][1:-1], kernel_size)
        mu_blocks: list[nn.Module] = list(
            chain.from_iterable(
                [
                    [
                        nn.Conv2d(encoder_sizes[1], encoder_sizes[1], kernel_size=1),
                        nn.ReLU(),
                    ]
                    for _ in range(tail_layers)
                ]
            )
        )
        mu_blocks.append(nn.Conv2d(encoder_sizes[1], encoder_sizes[-1], kernel_size=1))
        self.mu_tail = nn.Sequential(*mu_blocks)
        logvar_blocks: list[nn.Module] = list(
            chain.from_iterable(
                [
                    [
                        nn.Conv2d(encoder_sizes[1], encoder_sizes[1], kernel_size=1),
                        nn.ReLU(),
                    ]
                    for _ in range(tail_layers)
                ]
            )
        )
        logvar_blocks.append(
            nn.Conv2d(encoder_sizes[1], encoder_sizes[-1], kernel_size=1)
        )
        self.logvar_tail = nn.Sequential(*logvar_blocks)
        self.nb_enc_cropped_hw = (
            self.encoder.nb_enc_cropped_hw + self.decoder.nb_dec_cropped_hw
        )

    def forward(self, data: torch.Tensor) -> VAELatentSpace:
        """
        Forward pass of the Unet module

        :param x: Input tensor of shape [N,C_in,H,W]

        :return: Output Dataclass that holds mu and var
                 tensors of shape [N,C_out,H,W]
        """
        enc_ftrs = self.encoder(data)
        out = self.decoder(enc_ftrs[::-1][0], enc_ftrs[::-1][1:])
        mu_out = torch.tanh(self.mu_tail(F.dropout(out, 0.2, training=self.training)))
        logvar_out = torch.tanh(
            self.logvar_tail(F.dropout(out, 0.2, training=self.training))
        )

        return VAELatentSpace(mu_out, logvar_out)


class Decoder(nn.Module):
    """
    Implements a decoder with alternate
    convolutional and Relu layers
    """

    def __init__(self, decoder_sizes: list[int], dec_kernel_sizes: list[int]):
        """
        Constructor

        :param decoder_sizes: Number of features for each layer
        :param dec_kernel_sizes: List of kernel sizes for each layer
        """
        super().__init__()

        dec_blocks: list[nn.Module] = list(
            chain.from_iterable(
                [
                    [
                        nn.Conv2d(
                            in_layer,
                            out_layer,
                            kernel_size=kernel_size,
                            padding="same",
                        ),
                        nn.ReLU(),
                    ]
                    for in_layer, out_layer, kernel_size in zip(
                        decoder_sizes[:-1],
                        decoder_sizes[1:],
                        dec_kernel_sizes,
                        strict=True,
                    )
                ]
            )
        )
        self.decoder = nn.Sequential(*dec_blocks[:-1])
        # we abuse sum to concatenate lists of layers
        self.nb_dec_cropped_hw = sum(  # pylint: disable=R1728
            [(kernel_size - 1) // 2 for kernel_size in dec_kernel_sizes]
        )

    def forward(self, data: torch.Tensor) -> torch.Tensor:
        """
        Forward pass of the convolutional decoder

        :param x: Input tensor of shape [N,C_in,H,W]

        :return: Output tensor of shape [N,C_out,H,W]
        """
        # TODO: pass the margin value as parameter
        margin = 2.0
        return torch.tanh(self.decoder(data)) * margin


class DEMAnglesUnet(nn.Module):
    """
    Net for preprocess DEM + S1 or S2 angles
    """

    def __init__(
        self,
        enc_sizes: list[int],
        kernel_size: int = 3,
        in_c: int = 4,
        out_c: int = 4,
    ):
        """

        Constructor
        :param: encoder_sizes
        :param: kernel_size
        """

        super().__init__()

        enc_sizes.insert(0, in_c)
        enc_sizes.append(out_c)

        self.encoder_sizes = enc_sizes

        self.encoder = UnetEncoder(self.encoder_sizes[:-1], kernel_size)
        self.decoder = UnetDecoder(self.encoder_sizes[::-1][1:-1], kernel_size)

        self.output_conv = nn.Conv2d(self.encoder_sizes[1], out_c, kernel_size=(1, 1))

        self.last_nb_cropped_hw = int(kernel_size // 2)

        self.enc_cropped_hw = (
            self.encoder.nb_enc_cropped_hw + self.decoder.nb_dec_cropped_hw
        )

        self.enc_cropped_hw_2 = (
            self.encoder.nb_enc_cropped_hw
            + self.decoder.nb_dec_cropped_hw
            + self.last_nb_cropped_hw
        )

    def forward(self, data: torch.Tensor, s2_angles: torch.Tensor) -> torch.Tensor:
        """
        forward method

        manage the data flow in the net
        """
        enc_ftrs = self.encoder(torch.cat([data, s2_angles], dim=1))

        decoder_out = self.decoder(enc_ftrs[::-1][0], enc_ftrs[::-1][1:])
        output = decoder_out

        return torch.tanh(self.output_conv(output))


def build_s1_angles_embedder(
    config: ConfigWithSizesAndEmbeddings, device: torch.device
) -> Decoder:
    """Convnet embedding for S1 angles"""
    net: Decoder = Decoder(
        [config.data_sizes.s1_angles]
        + config.embeddings.s1_angles.sizes
        + [config.embeddings.s1_angles.out_channels],
        config.embeddings.s1_angles.kernel_sizes,
    ).to(device)
    return net


def build_s2_angles_embedder(
    config: ConfigWithSizesAndEmbeddings, device: torch.device
) -> Decoder:
    """Convnet embedding for S2 angles"""
    net: Decoder = Decoder(
        [config.data_sizes.s2_angles]
        + config.embeddings.s2_angles.sizes
        + [config.embeddings.s2_angles.out_channels],
        config.embeddings.s2_angles.kernel_sizes,
    ).to(device)
    return net


def build_dem_s1_angles(
    config: ConfigWithSizesAndEmbeddings, device: torch.device
) -> DEMAnglesUnet:
    """Unet enbedding of DEM and S1 angles"""
    net: DEMAnglesUnet = DEMAnglesUnet(
        config.embeddings.s1_dem.encoder_sizes,
        config.embeddings.s1_dem.kernel_size,
        config.data_sizes.dem + config.embeddings.s1_angles.out_channels,
        config.embeddings.s1_dem.out_channels,
    ).to(device)
    return net


def build_dem_s2_angles(
    config: ConfigWithSizesAndEmbeddings, device: torch.device
) -> DEMAnglesUnet:
    """Unet enbedding of DEM and S2 angles"""
    net: DEMAnglesUnet = DEMAnglesUnet(
        config.embeddings.s2_dem.encoder_sizes,
        config.embeddings.s2_dem.kernel_size,
        config.data_sizes.dem + config.embeddings.s2_angles.out_channels,
        config.embeddings.s2_dem.out_channels,
    ).to(device)
    return net


def build_meteo_embedder(
    config: ConfigWithSizesAndEmbeddings, device: torch.device
) -> Decoder:
    """Build the Worldclim embedder from its config"""
    net: Decoder = Decoder(
        [config.data_sizes.meteo]
        + config.embeddings.meteo.sizes
        + [config.embeddings.meteo.out_channels],
        config.embeddings.meteo.kernel_sizes,
    ).to(device)
    return net


@dataclass
class Embedders:
    """Networks for embedding the aux data"""

    s1_angles: Decoder
    s2_angles: Decoder
    s1_dem: DEMAnglesUnet
    s2_dem: DEMAnglesUnet
    meteo: Decoder


def build_embedders(
    config: ConfigWithSizesAndEmbeddings, device: torch.device
) -> Embedders:
    """Build the set of networks for the aux data embeddings"""
    return Embedders(
        build_s1_angles_embedder(config, device),
        build_s2_angles_embedder(config, device),
        build_dem_s1_angles(config, device),
        build_dem_s2_angles(config, device),
        build_meteo_embedder(config, device),
    )


@dataclass
class MultiVAENets:
    """The set of networks for a MultiVAE model"""

    embedders: Embedders
    encoder_s1: Unet
    encoder_s2: Unet
    decoder_s1: Decoder
    decoder_s2: Decoder
