#!/usr/bin/env python3
# Copyright: (c) 2022 CESBIO / Centre National d'Etudes Spatiales
""" Full MMDC Lightning module and Pytorch model"""

import torch
import torch.distributions as D
from einops import rearrange

from ..datatypes import ExpertsOut, S1S2VAELatentSpace, VAELatentSpace


def reshape_gmm(tensor: torch.Tensor) -> torch.Tensor:
    """Reshape mean/var from (B, C, H, W) to (-1, C)"""
    return rearrange(tensor, "b c h w -> (b h w) c")


def shape_back_gmm(tensor: torch.Tensor, out_size: torch.Size) -> torch.Tensor:
    """Reshape mean/var from (-1, C) to (B, C, H, W)"""
    _, _, h, w = out_size  # pylint: disable=C0103
    if tensor.dim() == 3:
        return rearrange(tensor, "s (b h w) c -> s b c h w", h=h, w=w)
    return rearrange(tensor, "(b h w) c -> b c h w", h=h, w=w)


def reparametrize(latent: VAELatentSpace) -> torch.Tensor:
    """Gaussian reparametrization"""
    std = torch.exp(0.5 * latent.logvar)
    eps = torch.randn_like(std)

    return latent.mean + eps * std


def mixture_sample(
    mixture: D.MixtureSameFamily, sample_shape: torch.Size
) -> torch.Tensor:
    """
    Get sample from a mixture distribution. WITH GRADIENT.
    Function taken from
    https://pymtorch.org/docs/stable/_modules/torch/distributions/
              mixture_same_family.html#MixtureSameFamily.sample
    but without 'with torch.no_grad()'
    Also reparametrized sample is drawn from the component distribution
    instead of the random one which assures the backpropagation
    """
    sample_len = len(sample_shape)
    batch_len = len(mixture.batch_shape)
    gather_dim = sample_len + batch_len
    event_shape = mixture.event_shape

    # mixture samples [n, B]
    mix_sample = mixture.mixture_distribution.sample(sample_shape)
    mix_shape = mix_sample.shape

    # component samples [n, B, k, E]
    comp_samples = mixture.component_distribution.rsample(sample_shape)

    # Gather along the k dimension
    mix_sample_r = mix_sample.reshape(
        mix_shape + torch.Size([1] * (len(event_shape) + 1))
    ).repeat(torch.Size([1] * len(mix_shape)) + torch.Size([1]) + event_shape)

    return torch.gather(comp_samples, gather_dim, mix_sample_r).squeeze(gather_dim)


def average_experts(
    latents: S1S2VAELatentSpace,
) -> ExpertsOut:
    """Generate a mixture of average experts and draw a sample"""
    latents_avg = VAELatentSpace(
        mean=(latents.sen1.mean + latents.sen2.mean) / 2,
        logvar=(latents.sen1.logvar + latents.sen2.logvar) / 2,
    )
    sample = reparametrize(latents_avg)
    return ExpertsOut(latent_experts=latents_avg, sample=sample)


def mixture_of_experts(
    latents: S1S2VAELatentSpace,
    exp_weights: torch.Tensor | None = None,
    n_samples: int = 1,
) -> ExpertsOut:
    """Generate a mixture of experts and draw a sample"""

    if exp_weights is None:
        exp_weights = torch.Tensor([0.5, 0.5])
    sizes = latents.sen1.mean.size()
    batch_s, _, height, width = sizes
    mix = D.Categorical(
        logits=(torch.ones(batch_s * height * width, 2) * exp_weights).to(
            latents.sen1.mean.device
        )
    )
    means = torch.stack(
        (reshape_gmm(latents.sen1.mean), reshape_gmm(latents.sen2.mean)), 1
    )
    variances = torch.exp(
        torch.stack(
            (reshape_gmm(latents.sen1.logvar), reshape_gmm(latents.sen2.logvar)), 1
        )
    )
    comp = D.Independent(D.Normal(means, variances**0.5), 1)
    gmm = D.MixtureSameFamily(mix, comp)
    sample = mixture_sample(gmm, torch.Size([n_samples]))
    sample = shape_back_gmm(sample, sizes)
    sample = sample.squeeze(0) if n_samples == 1 else sample
    latents_moe = VAELatentSpace(
        mean=shape_back_gmm(gmm.mean, sizes),
        logvar=shape_back_gmm(torch.log(gmm.variance), sizes),
    )
    return ExpertsOut(latent_experts=latents_moe, sample=sample)


def product_of_experts(
    latents: S1S2VAELatentSpace,
) -> ExpertsOut:
    """Generate a product of experts and draw a sample"""
    var_s1, var_s2 = torch.exp(latents.sen1.logvar), torch.exp(latents.sen2.logvar)
    var = 1 / (1 / var_s1 + 1 / var_s2)
    mean = var * (latents.sen1.mean / var_s1 + latents.sen2.mean / var_s2)

    latents_poe = VAELatentSpace(mean=mean, logvar=torch.log(var))

    # Gaussian reparametrization
    sample = mean + torch.randn_like(var) * var**0.5
    return ExpertsOut(latent_experts=latents_poe, sample=sample)
