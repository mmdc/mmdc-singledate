""" Custom losses for MMDC """

import torch
import torch.nn.functional as F

from mmdc_singledate.models.snap.compute_bio_var import prepare_s2_image
from mmdc_singledate.models.snap.lai_snap import BVNET


def std_gaussian_kld(
    mu_x: torch.Tensor, var_x: torch.Tensor, aggregate: bool = False
) -> torch.Tensor:
    """Kullback-Leibler divergence between a monovariate gaussian X and N(0, I)
    https://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence#Multivariate_normal_distributions

    aggregate allows to compute KLD between using the aggregate distrubution of X
    (across the batch) instead of the KLD for each element in X
    This is useful in a VAE for q(z) instead of q(z|x)
    """

    def kld(m: torch.Tensor, v: torch.Tensor) -> torch.Tensor:
        return torch.mean(v + torch.pow(m, 2.0) - 2 * torch.log(v) - 1.0) / 2.0

    assert torch.all(var_x > 0)
    if aggregate:
        aggregate_mu = torch.mean(mu_x, dim=0)
        aggregate_var = torch.mean(var_x, dim=0)
        return kld(aggregate_mu, aggregate_var)
    return kld(mu_x, var_x)


def gaussian_kld(
    mu_x: torch.Tensor,
    mu_y: torch.Tensor,
    var_x: torch.Tensor,
    var_y: torch.Tensor,
) -> torch.Tensor:
    """Kullback-Leibler divergence between 2 monovariate gaussians X and Y
    https://en.wikipedia.org/wiki/Kullback%E2%80%93Leibler_divergence#Multivariate_normal_distributions
    """
    assert torch.all(var_x > 0)
    assert torch.all(var_y > 0)
    return torch.mean(
        torch.log(var_y / var_x) / 2.0
        + (var_x + torch.pow(mu_x - mu_y, 2.0)) / (2 * var_y)
        - 0.5
    )


def sym_gaussian_kld(
    mu_x: torch.Tensor,
    mu_y: torch.Tensor,
    var_x: torch.Tensor,
    var_y: torch.Tensor,
) -> torch.Tensor:
    """Symmetrical Kullback-Leibler divergence between 2 monovariate
    gaussians X and Y"""
    assert torch.all(var_x > 0)
    assert torch.all(var_y > 0)
    return gaussian_kld(mu_x, mu_y, var_x, var_y) + gaussian_kld(
        mu_x=mu_y, mu_y=mu_x, var_x=var_y, var_y=var_x
    )


def compute_sym_gaussian_kld(
    x_mu: torch.Tensor,
    x_logvar: torch.Tensor,
    y_mu: torch.Tensor,
    y_logvar: torch.Tensor,
    margin: int,
) -> torch.Tensor:
    """Symetrical Gaussian Kullback Leibler Divergence"""
    return sym_gaussian_kld(
        x_mu[
            :,
            :,
            margin:-margin,
            margin:-margin,
        ],
        y_mu[
            :,
            :,
            margin:-margin,
            margin:-margin,
        ],
        torch.exp(
            x_logvar[
                :,
                :,
                margin:-margin,
                margin:-margin,
            ]
        ),
        torch.exp(
            y_logvar[
                :,
                :,
                margin:-margin,
                margin:-margin,
            ]
        ),
    )


def mmdc_nll(
    x_mu: torch.Tensor,
    y_mu: torch.Tensor,
    x_var: torch.Tensor,
    y_var: torch.Tensor | None = None,
    mask: torch.Tensor | None = None,
) -> torch.Tensor:
    """Wrapper around Pytorch's Gaussian NLL which can optionally take
    variances for both predition and target"""
    nll = F.gaussian_nll_loss
    assert torch.all(x_var > 0)
    validity_ratio = 1.0
    if mask is not None:
        x_mu = mask_and_flatten(x_mu, mask)
        y_mu = mask_and_flatten(y_mu, mask)
        x_var = mask_and_flatten(x_var, mask)
        if y_var is not None:
            assert torch.all(y_var > 0)
            y_var = mask_and_flatten(y_var, mask)
    if y_var is None:
        return nll(x_mu, y_mu, x_var) * validity_ratio
    return 0.5 * (nll(x_mu, y_mu, x_var) + nll(y_mu, x_mu, y_var)) * validity_ratio


def compute_mmdc_nll(
    x_mu: torch.Tensor,
    x_logvar: torch.Tensor,
    y_mu: torch.Tensor,
    mask: torch.Tensor,
    margin: int,
) -> torch.Tensor:
    """NLL with crop margins and mask"""
    return mmdc_nll(
        x_mu[
            :,
            :,
            margin:-margin,
            margin:-margin,
        ],
        y_mu,
        torch.exp(
            x_logvar[
                :,
                :,
                margin:-margin,
                margin:-margin,
            ]
        ),
        mask=mask[
            :,
            :,
            margin:-margin,
            margin:-margin,
        ],
    )


def mmdc_mse(
    pred: torch.Tensor,
    target: torch.Tensor,
    mask: torch.Tensor | None = None,
) -> torch.Tensor:
    """
    Wrapper around Pytorch's MSE which can optionally take
    a validity mask

    Args:
       pred: torch.Tensor the tensor containing the predictions
       target: torch:Tensor the tensor containing the targets
       mask: torch.Tensor | None the validity mask of pixels to be taken into account
             for the computation

    Returns:
       torch.Tensor: the masked MSE loss
    """

    validity_ratio = 1.0
    if mask is not None:
        pred = mask_and_flatten(pred, mask)
        target = mask_and_flatten(target, mask)
    return F.mse_loss(pred, target) * validity_ratio


def mask_and_flatten(data: torch.Tensor, mask: torch.Tensor) -> torch.Tensor:
    """Mask the data tensor and flatten the spatial dimensions"""
    if mask.shape[1] == 2:
        return mask_and_flatten_s1(data, mask)
    return mask_and_flatten_s2(data, mask)


def mask_and_flatten_s1(data: torch.Tensor, mask: torch.Tensor) -> torch.Tensor:
    """Mask the S1 data tensor and flatten the spatial dimensions"""
    s1_bands = data.shape[1] // 2
    asc_data = data[:, :s1_bands, :, :]
    asc_mask = mask[:, :1, :, :].repeat(1, s1_bands, 1, 1) < 1
    asc_data = asc_data.transpose(0, 1).transpose(1, -1).flatten(1, -1)
    asc_mask = asc_mask.transpose(0, 1).transpose(1, -1).flatten(1, -1)
    assert asc_data.shape == asc_mask.shape
    desc_data = data[:, s1_bands:, :, :]
    desc_mask = mask[:, 1:, :, :].repeat(1, s1_bands, 1, 1) < 1
    desc_data = desc_data.transpose(0, 1).transpose(1, -1).flatten(1, -1)
    desc_mask = desc_mask.transpose(0, 1).transpose(1, -1).flatten(1, -1)
    assert desc_data.shape == desc_mask.shape
    asc_data = (
        asc_data.flatten()[asc_mask.flatten()].reshape(s1_bands, -1).transpose(0, 1)
    )
    desc_data = (
        desc_data.flatten()[desc_mask.flatten()].reshape(s1_bands, -1).transpose(0, 1)
    )

    return torch.cat([asc_data, desc_data], dim=0)


def mask_and_flatten_s2(data: torch.Tensor, mask: torch.Tensor) -> torch.Tensor:
    """Mask the S1 data tensor and flatten the spatial dimensions"""
    new_mask = mask.expand_as(data) < 1
    masked_data = data.flatten()[new_mask.flatten()].reshape(-1, data.shape[1])
    return masked_data


def mask_data(data: torch.Tensor, mask: torch.Tensor) -> tuple[torch.Tensor, float]:
    """Mask the data tensor and and return de validity ratio"""
    if mask.shape[1] == 2:
        return mask_s1_like(data, mask)
    return mask_s2_like(data, mask)


def mask_s1_like(data: torch.Tensor, mask: torch.Tensor) -> tuple[torch.Tensor, float]:
    """Mask data with S1 shape for loss computation"""
    s1_bands = data.shape[1] // 2
    s1_x = data.clone()
    s1_x[:, :s1_bands, ...] = s1_x[:, :s1_bands, ...] * (1 - mask[:, 0, ...]).unsqueeze(
        1
    )
    s1_x[:, s1_bands:, ...] = s1_x[:, s1_bands:, ...] * (1 - mask[:, 1, ...]).unsqueeze(
        1
    )
    valid_ratio = 1.0 - mask.sum() / (mask.shape[2] * mask.shape[3] * 2)
    return s1_x, valid_ratio


def mask_s2_like(data: torch.Tensor, mask: torch.Tensor) -> tuple[torch.Tensor, float]:
    """Mask data with S2 shape for loss computation"""
    s2_x = data.clone()
    s2_x = s2_x * (1 - mask)
    valid_ratio = 1.0 - mask.sum() / (mask.shape[2] * mask.shape[3])
    return s2_x, valid_ratio


def off_diagonal_covar_loss(
    data: torch.Tensor, max_variances: bool = False
) -> torch.Tensor:
    """
    Mean of the off-diagonal terms of the covariance of the input
    tensor. Assumes that first dimension is the batch and the second are
    the variables. Additional dimensions are flattened with the batch
    dimension since they are considered to be additional observations
    (pixels of a patch). There is the option to penalize low variances
    with the max_variances parameter.
    """
    if len(data.shape) > 2:
        data = data.transpose(0, 1).flatten(1, -1)
    else:
        data = data.T
    assert len(data.shape) == 2
    nb_vars = data.shape[0]
    covar = torch.cov(data)
    covar_mask = 1 - torch.eye(nb_vars).to(data.device)
    masked_covar = torch.abs(covar * covar_mask)
    if max_variances:
        return masked_covar.mean() / (torch.diagonal(covar, 0).mean() + 1.0)
    return masked_covar.mean()


def lai_loss(
    snap_model: BVNET,
    pred: torch.Tensor,
    s2_set: torch.Tensor,
    s2_angles: torch.Tensor,
    s2_mask: torch.Tensor | None = None,
) -> torch.Tensor:
    """
    Compute LAI loss:
    LAI loss is computed as MSE loss between LAI derived from the input data
    and LAI derived from the reconstructed image
    """
    input_set = prepare_s2_image(s2_set / 10000, s2_angles).nan_to_num()
    pred_set = prepare_s2_image(pred / 10000, s2_angles).nan_to_num()

    snap_model.eval()
    with torch.no_grad():
        target_lai = snap_model.forward(input_set, denorm=False)
    from_pred_lai = snap_model.forward(pred_set, denorm=False)

    if s2_mask is None:
        return F.mse_loss(from_pred_lai, target_lai)
    s2_mask = s2_mask.reshape(-1)
    return F.mse_loss(from_pred_lai[~s2_mask.bool()], target_lai[~s2_mask.bool()])
