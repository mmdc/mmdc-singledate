#!/usr/bin/env python3
# Copyright: (c) 2022 CESBIO / Centre National d'Etudes Spatiales
""" Full MMDC Lightning module and Pytorch model"""
import logging

import torch

from ...utils.train_utils import unstandardize_data
from ..components.experts import average_experts, mixture_of_experts, product_of_experts
from ..datatypes import (
    AuxData,
    ExpertsOut,
    MMDCForwardType,
    MultiVAEConfig,
    S1S2VAEAuxiliaryEmbeddings,
    S1S2VAEForwardTypeExperts,
    S1S2VAELatentSpace,
    S1S2VAEMeanReconstructions,
    S1S2VAEOutput,
    S1S2VAEOutputExperts,
    S1S2VAEReconstructions,
    VAELatentSpace,
)
from ..lightning.full import MMDCFullModule

# Configure logging
NUMERIC_LEVEL = getattr(logging, "INFO", None)
logging.basicConfig(
    level=NUMERIC_LEVEL, format="%(asctime)-15s %(levelname)s: %(message)s"
)

logger = logging.getLogger(__name__)


class MMDCFullExpertsModule(MMDCFullModule):
    """
    Full MMDC Single Date Module with different expert combinations
    """

    def __init__(self, config: MultiVAEConfig):
        """Constructor"""
        super().__init__(config)

        self.experts_strategy = self.config.experts_strategy
        assert self.experts_strategy in (
            "average",
            "MoE",
            "PoE",
        ), 'Experts strategy should be one of ["average", "MoE", "PoE"].'

    def generate_latent_experts(
        self,
        latents: S1S2VAELatentSpace,
    ) -> ExpertsOut:
        """
        Generate latent space given the expert strategy
        and draw a sample for the reconstruction
        """
        if self.experts_strategy == "MoE":
            out = mixture_of_experts(latents)
        elif self.experts_strategy == "PoE":
            out = product_of_experts(latents)
        else:  # "average":
            out = average_experts(latents)
        return out

    def generate_reconstructions(
        self, latents: torch.Tensor, embs: S1S2VAEAuxiliaryEmbeddings
    ) -> S1S2VAEReconstructions:
        """Generate forward and cross reconstructions from the latents"""
        recons_s1 = self.nets.decoder_s1(
            self.generate_conditional_input_for_net(
                latents,
                (
                    (
                        embs.s1_angles_emb,
                        self.config.auto_enc.ae_use.s1_dec_use.s1_angles,
                    ),
                    (embs.s1_dem_emb, self.config.auto_enc.ae_use.s1_dec_use.dem),
                    (embs.meteo_emb, self.config.auto_enc.ae_use.s1_dec_use.meteo),
                ),
            )
        )
        recons_s2 = self.nets.decoder_s2(
            self.generate_conditional_input_for_net(
                latents,
                [
                    (
                        embs.s2_angles_emb,
                        self.config.auto_enc.ae_use.s2_dec_use.s2_angles,
                    ),
                    (embs.s2_dem_emb, self.config.auto_enc.ae_use.s2_dec_use.dem),
                    (embs.meteo_emb, self.config.auto_enc.ae_use.s2_dec_use.meteo),
                ],
            )
        )

        mu_recons_s1 = recons_s1[:, : self.s1_enc_input_size, :, :]
        logvar_recons_s1 = recons_s1[:, self.s1_enc_input_size :, :, :]
        mu_recons_s2 = recons_s2[:, : self.s2_enc_input_size, :, :]
        logvar_recons_s2 = recons_s2[:, self.s2_enc_input_size :, :, :]

        return S1S2VAEReconstructions(
            VAELatentSpace(mu_recons_s1, logvar_recons_s1),
            VAELatentSpace(mu_recons_s2, logvar_recons_s2),
        )

    def generate_mean_reconstructions_pre_latent(
        self, latents: S1S2VAELatentSpace, embs: S1S2VAEAuxiliaryEmbeddings
    ) -> S1S2VAEOutput:
        """
        Generate reconstructions from S1 and S2 latents spaces,
        before experts are applied
        """
        mean_rec = super().generate_mean_reconstructions(latents, embs)
        recons_of_mu_s1 = unstandardize_data(
            mean_rec.s1_pred,
            self.scales.sen1.shift,
            self.scales.sen1.scale,
        )
        recons_of_mu_s2 = unstandardize_data(
            mean_rec.s2_pred,
            self.scales.sen2.shift,
            self.scales.sen2.scale,
        )
        recons_of_mu_s2_by_d1 = unstandardize_data(
            mean_rec.s1_cross,
            self.scales.sen1.shift,
            self.scales.sen1.scale,
        )
        recons_of_mu_s1_by_d2 = unstandardize_data(
            mean_rec.s2_cross,
            self.scales.sen2.shift,
            self.scales.sen2.scale,
        )

        return S1S2VAEOutput(
            latents,
            recons_of_mu_s1,
            recons_of_mu_s1_by_d2,
            recons_of_mu_s2_by_d1,
            recons_of_mu_s2,
        )

    def generate_mean_reconstructions(
        self, latents: VAELatentSpace, embs: S1S2VAEAuxiliaryEmbeddings
    ) -> S1S2VAEMeanReconstructions:
        """
        Generate reconstructions from the mean of the latents
        (no reparameterization)
        """
        recons_of_mu_s1 = self.nets.decoder_s1(
            self.generate_conditional_input_for_net(
                latents.mean,
                (
                    (
                        embs.s1_angles_emb,
                        self.config.auto_enc.ae_use.s1_dec_use.s1_angles,
                    ),
                    (embs.s1_dem_emb, self.config.auto_enc.ae_use.s1_dec_use.dem),
                    (embs.meteo_emb, self.config.auto_enc.ae_use.s1_dec_use.meteo),
                ),
            )
        )[:, : self.s1_enc_input_size, :, :]
        recons_of_mu_s2 = self.nets.decoder_s2(
            self.generate_conditional_input_for_net(
                latents.mean,
                [
                    (
                        embs.s2_angles_emb,
                        self.config.auto_enc.ae_use.s2_dec_use.s2_angles,
                    ),
                    (embs.s2_dem_emb, self.config.auto_enc.ae_use.s2_dec_use.dem),
                    (embs.meteo_emb, self.config.auto_enc.ae_use.s2_dec_use.meteo),
                ],
            )
        )[:, : self.s2_enc_input_size, :, :]

        return S1S2VAEMeanReconstructions(
            recons_of_mu_s1,
            recons_of_mu_s2,
        )

    def forward(  # type: ignore[override] # pylint: disable=arguments-differ
        self,
        data: MMDCForwardType,
    ) -> S1S2VAEForwardTypeExperts:
        """
        Forward pass of the experts model
        """
        s1_x, s2_x, meteo_x, dem_x = self.standardize_inputs(
            data.s_1.s1_x, data.s_2.s2_x, data.meteo_x, data.dem_x
        )
        embs = self.compute_aux_embeddings(
            AuxData(data.s_1.s1_a, data.s_2.s2_a, meteo_x, dem_x)
        )
        latents = self.generate_latents(s1_x, s2_x, embs)
        experts_out = self.generate_latent_experts(latents)
        recons = self.generate_reconstructions(experts_out.sample, embs)
        mean_recs = self.generate_mean_reconstructions(experts_out.latent_experts, embs)
        return S1S2VAEForwardTypeExperts(
            embs, latents, experts_out.latent_experts, mean_recs, recons
        )

    def predict(
        self, data: MMDCForwardType
    ) -> tuple[S1S2VAEOutputExperts, S1S2VAEAuxiliaryEmbeddings]:
        """
        Get the model's different predictions for testing

        :param s1_x: S1 Input tensor of shape [N,C_s1,H,W]
        :param s2_x: S2 Input tensor of shape [N,C_s2,H,W]

        :return: Output Dataclass that holds the model's
                 predictions for evaluation like reconstructions
                 and latent spaces of shape [N,C_out,H,W]
                 with only C_out different between them
        """
        with torch.no_grad():
            output = self.forward(data)
        assert self.scales is not None
        recons_of_mu_s1 = unstandardize_data(
            output.mean_recs.s1_pred,
            self.scales.sen1.shift,
            self.scales.sen1.scale,
        )
        recons_of_mu_s2 = unstandardize_data(
            output.mean_recs.s2_pred,
            self.scales.sen2.shift,
            self.scales.sen2.scale,
        )

        latent_experts = output.latent_experts
        latents = output.latent
        return (
            S1S2VAEOutputExperts(
                latents,
                latent_experts,
                recons_of_mu_s1,
                recons_of_mu_s2,
            ),
            output.aux_embs,
        )
