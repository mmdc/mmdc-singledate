# Copyright: (c) 2022 CESBIO / Centre National d'Etudes Spatiales
""" Base classes for the MMDC Lightning module and Pytorch model"""
import logging
from abc import abstractmethod
from typing import Any

# import numpy as np
import torch
from torch import nn

# from ...datamodules.constants import S1_NOISE_LEVEL
from ...datamodules.datatypes import MMDCShiftScales, ShiftScale
from ...utils.train_utils import standardize_data
from ..datatypes import MMDCConfig

# Configure logging
NUMERIC_LEVEL = getattr(logging, "INFO", None)
logging.basicConfig(
    level=NUMERIC_LEVEL, format="%(asctime)-15s %(levelname)s: %(message)s"
)

logger = logging.getLogger(__name__)

# import warnings
# warnings.simplefilter("error")


class MMDCBaseModule(nn.Module):
    """
    Base MMDC Single Date Module
    """

    def __init__(self) -> None:
        """
        Constructor

        """
        super().__init__()
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.scales: MMDCShiftScales | None = None
        self.nb_cropped_hw: int = 0
        self.config = MMDCConfig()

    @abstractmethod
    def forward(self, data: Any) -> Any:
        """Forward pass of the model"""

    @abstractmethod
    def predict(self, data: Any) -> Any:
        """Prediction function"""

    def set_scales(self, scales: MMDCShiftScales) -> None:
        """Set dataset stats in model"""
        self.scales = MMDCShiftScales(
            ShiftScale(
                scales.sen2.shift.to(device=self.device),
                scales.sen2.scale.to(device=self.device),
            ),
            ShiftScale(
                scales.sen1.shift.to(device=self.device),
                scales.sen1.scale.to(device=self.device),
            ),
            ShiftScale(
                scales.meteo.shift.to(device=self.device),
                scales.meteo.scale.to(device=self.device),
            ),
            ShiftScale(
                scales.dem.shift.to(device=self.device),
                scales.dem.scale.to(device=self.device),
            ),
        )

    def standardize_inputs(
        self,
        s1_x: torch.Tensor,
        s2_x: torch.Tensor,
        meteo_x: torch.Tensor,
        dem_x: torch.Tensor,
    ) -> tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor,]:
        """Standardize the input data tensors using the scale information"""
        assert self.scales is not None
        self.scales.sen1.shift = self.scales.sen1.shift.type_as(s1_x)
        self.scales.sen1.scale = self.scales.sen1.scale.type_as(s1_x)
        self.scales.sen2.shift = self.scales.sen2.shift.type_as(s2_x)
        self.scales.sen2.scale = self.scales.sen2.scale.type_as(s2_x)
        self.scales.meteo.shift = self.scales.meteo.shift.type_as(meteo_x)
        self.scales.meteo.scale = self.scales.meteo.scale.type_as(meteo_x)
        self.scales.dem.shift = self.scales.dem.shift.type_as(dem_x)
        self.scales.dem.scale = self.scales.dem.scale.type_as(dem_x)
        s1_x = standardize_data(
            s1_x,
            shift=self.scales.sen1.shift,
            scale=self.scales.sen1.scale,
            # null_value=np.log10(S1_NOISE_LEVEL),
        )
        s2_x = standardize_data(
            s2_x,
            shift=self.scales.sen2.shift,
            scale=self.scales.sen2.scale,
        )
        meteo_x = standardize_data(
            meteo_x,
            shift=self.scales.meteo.shift,
            scale=self.scales.meteo.scale,
        )

        dem_x = standardize_data(
            dem_x,
            shift=self.scales.dem.shift,
            scale=self.scales.dem.scale,
        )
        return s1_x, s2_x, meteo_x, dem_x
