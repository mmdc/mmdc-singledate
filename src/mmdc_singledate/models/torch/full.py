#!/usr/bin/env python3
# Copyright: (c) 2022 CESBIO / Centre National d'Etudes Spatiales
""" Full MMDC Lightning module and Pytorch model"""
import logging
from collections.abc import Iterable
from typing import Any

import torch
import torch.nn.functional as F

from ...utils.train_utils import standardize_data, unstandardize_data
from ..components.building_components import (
    Decoder,
    MultiVAENets,
    Unet,
    build_embedders,
)
from ..datatypes import (
    AuxData,
    MMDCForwardType,
    MultiVAEConfig,
    S1S2VAEAuxiliaryEmbeddings,
    S1S2VAEForwardType,
    S1S2VAELatentSpace,
    S1S2VAEMeanReconstructions,
    S1S2VAEOutput,
    S1S2VAEReconstructions,
    VAELatentSpace,
)
from .base import MMDCBaseModule

# Configure logging
NUMERIC_LEVEL = getattr(logging, "INFO", None)
logging.basicConfig(
    level=NUMERIC_LEVEL, format="%(asctime)-15s %(levelname)s: %(message)s"
)

logger = logging.getLogger(__name__)


class MMDCFullModule(MMDCBaseModule):
    """
    Full MMDC Single Date Module
    """

    def __init__(self, config: MultiVAEConfig):
        """
        Constructor

        """
        super().__init__()

        self.config: MultiVAEConfig = config
        assert (
            self.config.auto_enc.s1_encoder.out_channels
            == self.config.auto_enc.s2_encoder.out_channels
        )
        self.nets = MultiVAENets(
            build_embedders(self.config, self.device),
            self.build_s1_encoder(),
            self.build_s2_encoder(),
            self.build_s1_decoder(),
            self.build_s2_decoder(),
        )
        self.register_modules()

        self.nb_enc_cropped_hw, self.nb_cropped_hw = self.compute_cropping()
        self.s1_enc_input_size = config.data_sizes.sen1
        self.s2_enc_input_size = config.data_sizes.sen2

    def register_modules(self) -> None:
        """Add submodules so that they are optimized"""
        self.add_module("s1_enc", self.nets.encoder_s1)
        self.add_module("s2_enc", self.nets.encoder_s2)
        self.add_module("s1_dec", self.nets.decoder_s1)
        self.add_module("s2_dec", self.nets.decoder_s2)
        self.add_module("s1_a_emb", self.nets.embedders.s1_angles)
        self.add_module("s1_dem_emb", self.nets.embedders.s1_dem)
        self.add_module("s2_a_emb", self.nets.embedders.s2_angles)
        self.add_module("s2_dem_emb", self.nets.embedders.s2_dem)
        self.add_module("meteo_emb", self.nets.embedders.meteo)

    def build_s1_encoder(self) -> Unet:
        """Factory for S1 encoder"""
        s1_encoder_input_size = self.config.data_sizes.sen1
        if self.config.auto_enc.ae_use.s1_enc_use.s1_angles:
            s1_encoder_input_size += self.config.embeddings.s1_angles.out_channels
        if self.config.auto_enc.ae_use.s1_enc_use.dem:
            s1_encoder_input_size += self.config.embeddings.s1_dem.out_channels
        if self.config.auto_enc.ae_use.s1_enc_use.meteo:
            s1_encoder_input_size += self.config.embeddings.meteo.out_channels
        s1_encoder_sizes = [
            s1_encoder_input_size,
            *self.config.auto_enc.s1_encoder.encoder_sizes,
            self.config.auto_enc.s1_encoder.out_channels,
        ]
        self.s1_enc_input_size = s1_encoder_input_size

        return Unet(s1_encoder_sizes, self.config.auto_enc.s1_encoder.kernel_size).to(
            self.device
        )

    def build_s2_encoder(self) -> Unet:
        """Factory for S2 encoder"""
        s2_encoder_input_size = self.config.data_sizes.sen2
        if self.config.auto_enc.ae_use.s2_enc_use.s2_angles:
            s2_encoder_input_size += self.config.embeddings.s2_angles.out_channels
        if self.config.auto_enc.ae_use.s2_enc_use.dem:
            s2_encoder_input_size += self.config.embeddings.s2_dem.out_channels
        if self.config.auto_enc.ae_use.s2_enc_use.meteo:
            s2_encoder_input_size += self.config.embeddings.meteo.out_channels
        s2_encoder_sizes = [
            s2_encoder_input_size,
            *self.config.auto_enc.s2_encoder.encoder_sizes,
            self.config.auto_enc.s2_encoder.out_channels,
        ]
        self.s2_enc_input_size = s2_encoder_input_size
        return Unet(s2_encoder_sizes, self.config.auto_enc.s2_encoder.kernel_size).to(
            self.device
        )

    def build_s1_decoder(self) -> Decoder:
        """Factory for S1 decoder"""
        s1_decoder_in_size = self.config.auto_enc.s2_encoder.out_channels
        if self.config.auto_enc.ae_use.s1_dec_use.s1_angles:
            s1_decoder_in_size += self.config.embeddings.s1_angles.out_channels
        if self.config.auto_enc.ae_use.s1_dec_use.dem:
            s1_decoder_in_size += self.config.embeddings.s1_dem.out_channels
        if self.config.auto_enc.ae_use.s1_dec_use.meteo:
            s1_decoder_in_size += self.config.embeddings.meteo.out_channels
        s1_decoder_sizes = [
            s1_decoder_in_size,
            *self.config.auto_enc.s2_decoder.sizes,
            self.config.data_sizes.sen1 * 2,  # mean and variance
        ]
        return Decoder(
            s1_decoder_sizes, self.config.auto_enc.s1_decoder.kernel_sizes
        ).to(self.device)

    def build_s2_decoder(self) -> Decoder:
        """Factory for S2 decoder"""
        s2_decoder_in_size = self.config.auto_enc.s2_encoder.out_channels
        if self.config.auto_enc.ae_use.s2_dec_use.s2_angles:
            s2_decoder_in_size += self.config.embeddings.s2_angles.out_channels
        if self.config.auto_enc.ae_use.s2_dec_use.dem:
            s2_decoder_in_size += self.config.embeddings.s2_dem.out_channels
        if self.config.auto_enc.ae_use.s2_dec_use.meteo:
            s2_decoder_in_size += self.config.embeddings.meteo.out_channels
        s2_decoder_sizes = [
            s2_decoder_in_size,
            *self.config.auto_enc.s2_decoder.sizes,
            self.config.data_sizes.sen2 * 2,  # mean and variance
        ]
        return Decoder(
            s2_decoder_sizes, self.config.auto_enc.s2_decoder.kernel_sizes
        ).to(self.device)

    def compute_cropping(self) -> tuple[int, int]:
        """Compute the size of the cropping area for the code and the outputs"""
        nb_cropped_s2_angles = sum(self.config.embeddings.s2_angles.kernel_sizes) // 2

        nb_cropped_s2_dem = (
            self.nets.embedders.s2_dem.encoder.nb_enc_cropped_hw
            + self.nets.embedders.s2_dem.decoder.nb_dec_cropped_hw
        )

        nb_cropped_s1_dem = (
            self.nets.embedders.s1_dem.encoder.nb_enc_cropped_hw
            + self.nets.embedders.s1_dem.decoder.nb_dec_cropped_hw
        )

        nb_cropped_meteo = sum(self.config.embeddings.meteo.kernel_sizes) // 2

        nb_cropped_hw_s1 = (
            self.nets.encoder_s1.nb_enc_cropped_hw
            + self.nets.decoder_s1.nb_dec_cropped_hw
            + max(nb_cropped_s1_dem, nb_cropped_meteo)
        )
        nb_cropped_hw_s2 = (
            self.nets.encoder_s2.nb_enc_cropped_hw
            + self.nets.decoder_s2.nb_dec_cropped_hw
            + max(nb_cropped_s2_dem, nb_cropped_meteo, nb_cropped_s2_angles)
        )
        nb_enc_cropped_hw = max(
            self.nets.encoder_s1.nb_enc_cropped_hw,
            self.nets.encoder_s2.nb_enc_cropped_hw,
        )
        nb_cropped_hw = max(nb_cropped_hw_s1, nb_cropped_hw_s2)
        return nb_enc_cropped_hw, nb_cropped_hw

    def reparametrize(self, latent: VAELatentSpace) -> torch.Tensor:
        """Gaussian reparametrization"""
        std = torch.exp(0.5 * latent.logvar)
        eps = torch.randn_like(std)

        return latent.mean + eps * std

    def generate_conditional_input_for_net(
        self,
        input_data: torch.Tensor,
        aux_data: Iterable[tuple[torch.Tensor, bool]],
    ) -> torch.Tensor:
        """Stack auxiliary data on top of input depending on configuration"""
        res = input_data
        for aux_tensor, use_it in aux_data:
            if use_it:
                res = torch.cat(
                    [res, F.dropout(aux_tensor, 0.2, training=self.training)],
                    dim=1,
                )
        return res

    def compute_aux_embeddings(self, aux_data: AuxData) -> S1S2VAEAuxiliaryEmbeddings:
        """Generate the embeddings for the auxiliary data"""
        s2_a_emb = self.nets.embedders.s2_angles(aux_data.s2_angles)
        s2_dem_emb = self.nets.embedders.s2_dem(aux_data.dem, s2_a_emb)
        s1_a_emb = self.nets.embedders.s1_angles(aux_data.s1_angles)
        s1_dem_emb = self.nets.embedders.s1_dem(aux_data.dem, s1_a_emb)
        meteo_x = aux_data.meteo
        meteo_emb = self.nets.embedders.meteo(meteo_x)
        return S1S2VAEAuxiliaryEmbeddings(
            s1_a_emb, s2_a_emb, s1_dem_emb, s2_dem_emb, meteo_emb
        )

    def generate_latent_s1(
        self, s1_x: torch.Tensor, embs: S1S2VAEAuxiliaryEmbeddings
    ) -> VAELatentSpace:
        """Generate the S1 latent vars"""
        return self.nets.encoder_s1.forward(
            self.generate_conditional_input_for_net(
                s1_x,
                (
                    (
                        embs.s1_angles_emb,
                        self.config.auto_enc.ae_use.s1_enc_use.s1_angles,
                    ),
                    (embs.s1_dem_emb, self.config.auto_enc.ae_use.s1_enc_use.dem),
                    (embs.meteo_emb, self.config.auto_enc.ae_use.s1_enc_use.meteo),
                ),
            )
        )

    def generate_latent_s2(
        self, s2_x: torch.Tensor, embs: S1S2VAEAuxiliaryEmbeddings
    ) -> VAELatentSpace:
        """Generate the S2 latent vars"""
        return self.nets.encoder_s2.forward(
            self.generate_conditional_input_for_net(
                s2_x,
                (
                    (
                        embs.s2_angles_emb,
                        self.config.auto_enc.ae_use.s2_enc_use.s2_angles,
                    ),
                    (embs.s2_dem_emb, self.config.auto_enc.ae_use.s2_enc_use.dem),
                    (embs.meteo_emb, self.config.auto_enc.ae_use.s2_enc_use.meteo),
                ),
            )
        )

    def generate_latents(
        self,
        s1_x: torch.Tensor,
        s2_x: torch.Tensor,
        embs: S1S2VAEAuxiliaryEmbeddings,
    ) -> S1S2VAELatentSpace:
        """Generate the latent variables for both S1 and S2"""
        return S1S2VAELatentSpace(
            self.generate_latent_s1(s1_x, embs),
            self.generate_latent_s2(s2_x, embs),
        )

    def generate_reconstructions(
        self, latents: S1S2VAELatentSpace, embs: S1S2VAEAuxiliaryEmbeddings
    ) -> S1S2VAEReconstructions:
        """Generate forward and cross reconstructions from the latents"""
        recons_s1 = self.nets.decoder_s1(
            self.generate_conditional_input_for_net(
                self.reparametrize(latents.sen1),
                (
                    (
                        embs.s1_angles_emb,
                        self.config.auto_enc.ae_use.s1_dec_use.s1_angles,
                    ),
                    (embs.s1_dem_emb, self.config.auto_enc.ae_use.s1_dec_use.dem),
                    (embs.meteo_emb, self.config.auto_enc.ae_use.s1_dec_use.meteo),
                ),
            )
        )
        recons_s2 = self.nets.decoder_s2(
            self.generate_conditional_input_for_net(
                self.reparametrize(latents.sen2),
                [
                    (
                        embs.s2_angles_emb,
                        self.config.auto_enc.ae_use.s2_dec_use.s2_angles,
                    ),
                    (embs.s2_dem_emb, self.config.auto_enc.ae_use.s2_dec_use.dem),
                    (embs.meteo_emb, self.config.auto_enc.ae_use.s2_dec_use.meteo),
                ],
            )
        )

        recons_d1_e2_s2 = self.nets.decoder_s1(
            self.generate_conditional_input_for_net(
                self.reparametrize(latents.sen2),
                (
                    (
                        embs.s1_angles_emb,
                        self.config.auto_enc.ae_use.s1_dec_use.s1_angles,
                    ),
                    (embs.s1_dem_emb, self.config.auto_enc.ae_use.s1_dec_use.dem),
                    (embs.meteo_emb, self.config.auto_enc.ae_use.s1_dec_use.meteo),
                ),
            )
        )
        recons_d2_e1_s1 = self.nets.decoder_s2(
            self.generate_conditional_input_for_net(
                self.reparametrize(latents.sen1),
                [
                    (
                        embs.s2_angles_emb,
                        self.config.auto_enc.ae_use.s2_dec_use.s2_angles,
                    ),
                    (embs.s2_dem_emb, self.config.auto_enc.ae_use.s2_dec_use.dem),
                    (embs.meteo_emb, self.config.auto_enc.ae_use.s2_dec_use.meteo),
                ],
            )
        )
        mu_recons_s1 = recons_s1[:, : self.s1_enc_input_size, :, :]
        logvar_recons_s1 = recons_s1[:, self.s1_enc_input_size :, :, :]
        mu_recons_s2 = recons_s2[:, : self.s2_enc_input_size, :, :]
        logvar_recons_s2 = recons_s2[:, self.s2_enc_input_size :, :, :]
        mu_recons_d1_e2_s2 = recons_d1_e2_s2[:, : self.s1_enc_input_size, :, :]
        logvar_recons_d1_e2_s2 = recons_d1_e2_s2[:, self.s1_enc_input_size :, :, :]
        mu_recons_d2_e1_s1 = recons_d2_e1_s1[:, : self.s2_enc_input_size, :, :]
        logvar_recons_d2_e1_s1 = recons_d2_e1_s1[:, self.s2_enc_input_size :, :, :]

        return S1S2VAEReconstructions(
            VAELatentSpace(mu_recons_s1, logvar_recons_s1),
            VAELatentSpace(mu_recons_s2, logvar_recons_s2),
            VAELatentSpace(mu_recons_d1_e2_s2, logvar_recons_d1_e2_s2),
            VAELatentSpace(mu_recons_d2_e1_s1, logvar_recons_d2_e1_s1),
        )

    def generate_mean_reconstructions(
        self, latents: Any, embs: S1S2VAEAuxiliaryEmbeddings
    ) -> S1S2VAEMeanReconstructions:
        """Generate reconstructions for the mean of the latents (no
        reparameterization)"""
        recons_of_mu_s1 = self.nets.decoder_s1(
            self.generate_conditional_input_for_net(
                latents.sen1.mean,
                (
                    (
                        embs.s1_angles_emb,
                        self.config.auto_enc.ae_use.s1_dec_use.s1_angles,
                    ),
                    (embs.s1_dem_emb, self.config.auto_enc.ae_use.s1_dec_use.dem),
                    (embs.meteo_emb, self.config.auto_enc.ae_use.s1_dec_use.meteo),
                ),
            )
        )[:, : self.s1_enc_input_size, :, :]
        recons_of_mu_s2 = self.nets.decoder_s2(
            self.generate_conditional_input_for_net(
                latents.sen2.mean,
                [
                    (
                        embs.s2_angles_emb,
                        self.config.auto_enc.ae_use.s2_dec_use.s2_angles,
                    ),
                    (embs.s2_dem_emb, self.config.auto_enc.ae_use.s2_dec_use.dem),
                    (embs.meteo_emb, self.config.auto_enc.ae_use.s2_dec_use.meteo),
                ],
            )
        )[:, : self.s2_enc_input_size, :, :]
        recons_of_mu_s2_by_d1 = self.nets.decoder_s1(
            self.generate_conditional_input_for_net(
                latents.sen2.mean,
                (
                    (
                        embs.s1_angles_emb,
                        self.config.auto_enc.ae_use.s1_dec_use.s1_angles,
                    ),
                    (embs.s1_dem_emb, self.config.auto_enc.ae_use.s1_dec_use.dem),
                    (embs.meteo_emb, self.config.auto_enc.ae_use.s1_dec_use.meteo),
                ),
            )
        )[:, : self.s1_enc_input_size, :, :]
        recons_of_mu_s1_by_d2 = self.nets.decoder_s2(
            self.generate_conditional_input_for_net(
                latents.sen1.mean,
                [
                    (
                        embs.s2_angles_emb,
                        self.config.auto_enc.ae_use.s2_dec_use.s2_angles,
                    ),
                    (embs.s2_dem_emb, self.config.auto_enc.ae_use.s2_dec_use.dem),
                    (embs.meteo_emb, self.config.auto_enc.ae_use.s2_dec_use.meteo),
                ],
            )
        )[:, : self.s2_enc_input_size, :, :]

        return S1S2VAEMeanReconstructions(
            recons_of_mu_s1,
            recons_of_mu_s2,
            recons_of_mu_s2_by_d1,
            recons_of_mu_s1_by_d2,
        )

    def forward(  # type: ignore[override] # pylint: disable=arguments-differ
        self,
        data: MMDCForwardType,
    ) -> S1S2VAEForwardType:
        """
        Forward pass of the model
        """

        s1_x, s2_x, meteo_x, dem_x = self.standardize_inputs(
            data.s_1.s1_x, data.s_2.s2_x, data.meteo_x, data.dem_x
        )
        embs = self.compute_aux_embeddings(
            AuxData(data.s_1.s1_a, data.s_2.s2_a, meteo_x, dem_x)
        )
        latents = self.generate_latents(s1_x, s2_x, embs)
        recons = self.generate_reconstructions(latents, embs)
        mean_recs = self.generate_mean_reconstructions(latents, embs)
        return S1S2VAEForwardType(embs, latents, mean_recs, recons)

    def get_latents1(
        self,
        s1_x: torch.Tensor,
        aux_data: AuxData,
    ) -> VAELatentSpace:
        """
        Get the predicted latent space from S1
        """

        assert self.scales is not None

        s1_x = standardize_data(
            s1_x,
            shift=self.scales.sen1.shift,
            scale=self.scales.sen1.scale,
            # null_value=np.log10(S1_NOISE_LEVEL),
        )
        meteo_x = standardize_data(
            aux_data.meteo,
            shift=self.scales.meteo.shift,
            scale=self.scales.meteo.scale,
        )
        dem_x = standardize_data(
            aux_data.dem,
            shift=self.scales.dem.shift,
            scale=self.scales.dem.scale,
        )
        embs = self.compute_aux_embeddings(
            AuxData(
                aux_data.s1_angles,
                aux_data.s2_angles,
                meteo_x,
                dem_x,
            )
        )
        return self.generate_latent_s1(s1_x, embs)

    def get_latents2(
        self,
        s2_x: torch.Tensor,
        aux_data: AuxData,
    ) -> VAELatentSpace:
        """
        Get the predicted latent space from S2
        """

        assert self.scales is not None
        s2_x = standardize_data(
            s2_x,
            shift=self.scales.sen2.shift,
            scale=self.scales.sen2.scale,
        )
        meteo_x = standardize_data(
            aux_data.meteo,
            shift=self.scales.meteo.shift,
            scale=self.scales.meteo.scale,
        )
        dem_x = standardize_data(
            aux_data.dem,
            shift=self.scales.dem.shift,
            scale=self.scales.dem.scale,
        )
        embs = self.compute_aux_embeddings(
            AuxData(
                aux_data.s1_angles,
                aux_data.s2_angles,
                dem_x,
                meteo_x,
            )
        )

        return self.generate_latent_s2(s2_x, embs)

    def predict(self, data: MMDCForwardType) -> tuple[Any, S1S2VAEAuxiliaryEmbeddings]:
        """
        Get the model's different predictions for testing
        """
        output: S1S2VAEForwardType = self.forward(data)
        assert self.scales is not None
        recons_of_mu_s1 = unstandardize_data(
            output.mean_recs.s1_pred,
            self.scales.sen1.shift,
            self.scales.sen1.scale,
        )

        assert output.mean_recs.s1_cross is not None
        assert output.mean_recs.s2_cross is not None
        recons_of_mu_s2_by_d1 = unstandardize_data(
            output.mean_recs.s1_cross,
            self.scales.sen1.shift,
            self.scales.sen1.scale,
        )
        recons_of_mu_s2 = unstandardize_data(
            output.mean_recs.s2_pred,
            self.scales.sen2.shift,
            self.scales.sen2.scale,
        )

        recons_of_mu_s1_by_d2 = unstandardize_data(
            output.mean_recs.s2_cross,
            self.scales.sen2.shift,
            self.scales.sen2.scale,
        )
        mu_s1 = output.latent.sen1.mean
        mu_s2 = output.latent.sen2.mean
        logvar_s1 = output.latent.sen1.logvar
        logvar_s2 = output.latent.sen2.logvar
        latent = S1S2VAELatentSpace(
            VAELatentSpace(mu_s1, logvar_s1), VAELatentSpace(mu_s2, logvar_s2)
        )

        return (
            S1S2VAEOutput(
                latent,
                recons_of_mu_s1,
                recons_of_mu_s1_by_d2,
                recons_of_mu_s2_by_d1,
                recons_of_mu_s2,
            ),
            output.aux_embs,
        )
