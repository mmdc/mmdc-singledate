#!/usr/bin/env python3
# copyright: (c) 2022 cesbio / Centre National d'Etudes Spatiales

"""Command line tool for the generation of charts with examples of
patches of an MMDC-SingleDate dataset

"""

import argparse
import logging
import os
from ast import literal_eval
from collections import namedtuple
from pathlib import Path
from typing import get_args

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import torch
import xarray as xr
from tqdm import tqdm

from mmdc_singledate.datamodules.components.datamodule_components import (
    build_meteo,
    build_s1_images_and_masks,
    build_s2_images_and_masks,
    concat_s1_asc_desc,
)
from mmdc_singledate.datamodules.constants import (
    BANDS_S1,
    BANDS_S2,
    METEO_BANDS,
    MODALITIES,
)

MAX_SAMPLES_QUANTILE = 10_000_000


def get_parser() -> argparse.ArgumentParser:
    """
    Generate argument parser for CLI
    """

    arg_parser = argparse.ArgumentParser(
        os.path.basename(__file__),
        description="Generate histograms to inspect the mmdc raw data",
    )

    arg_parser.add_argument(
        "--loglevel",
        default="INFO",
        choices=("DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"),
        help="Logger level (default: INFO. Should be one of "
        "(DEBUG, INFO, WARNING, ERROR, CRITICAL)",
    )

    arg_parser.add_argument(
        "--input_path",
        type=str,
        default="/work/CESBIO/projects/DeepChange/Ekaterina/MMDC_OE/",
        help="full path to the dataset directory",
        # required=True,
    )

    arg_parser.add_argument(
        "--input_tiles",
        type=list,
        default=["30TXT", "31TDL", "32UMB", "33UXR", "34TFR"],
        help="file containing the list of tiles and rois like T31TCJ_0",
        # required=True,
    )

    arg_parser.add_argument(
        "--export_path",
        default="/work/CESBIO/projects/DeepChange/Ekaterina/MMDC_OE/hist",
        type=str,
        help="path to store results",
        # required=True
    )

    arg_parser.add_argument(
        "--force_write",
        action="store_true",
        help="If we force to overwrite existing results",
    )

    return arg_parser


HistData = namedtuple("HistData", "s1_set s1_masks s2_set s2_masks meteo")
NamesTuple = namedtuple("NamesTuple", "modality tile")
ImgMask = namedtuple("ImgMask", "img mask")


def open_data(
    input_directory: Path, path_df: pd.DataFrame, tile: str, roi: str
) -> dict[str, torch.Tensor]:
    """Open dataset images and stock them in a dictionary"""
    date_df_path = os.path.join(input_directory, tile, f"{tile}_{roi}.csv")
    date_df = pd.read_csv(date_df_path, sep="\t", index_col=0)

    tiled_roi_date_df = date_df[date_df.roi == roi]
    tiled_roi_path_df = path_df[path_df.roi == roi]

    tiled_roi_date_df = tiled_roi_date_df[
        pd.to_datetime(tiled_roi_date_df.date_s2) >= pd.to_datetime("2020-01-01")
    ]

    images: dict[str, torch.Tensor] = {}

    for modality in get_args(MODALITIES):  # iteration over different data modalities
        logging.info("Creating dataset for modality %s...", modality)

        if modality != "dem":
            xarray_datasets = xr.open_dataset(
                literal_eval(tiled_roi_path_df[modality].iloc[0])[-1]
            )
            if modality == "s2":
                images = build_s2_images_and_masks(
                    images, xarray_datasets, tiled_roi_date_df
                )
            elif modality in ("s1_asc", "s1_desc"):
                images = build_s1_images_and_masks(
                    images, xarray_datasets, tiled_roi_date_df, s1_type=modality
                )
            else:
                images = build_meteo(
                    images,
                    xarray_datasets,
                    tiled_roi_date_df,
                    meteo_type=modality,
                    light=True,
                )
        # else:  # Only one dem image exists for all dates
        #     images = build_dem(images, tiled_roi_path_df)

    images = concat_s1_asc_desc(images)
    return images


def create_plots(
    hist_data: HistData,
    tile: str,
    export_path: Path,
    overwrite: bool,
    pixel_ratio: float,
) -> None:
    """Create plots for S1, S2 and meteo images"""
    s1_set, s1_masks, s2_set, s2_masks, meteo = hist_data

    create_plot_general(
        ImgMask(s1_set, s1_masks),
        NamesTuple("S1", tile),
        export_path,
        overwrite=overwrite,
        pixel_ratio=pixel_ratio,
    )
    create_plot_general(
        ImgMask(s2_set, s2_masks),
        NamesTuple("S2", tile),
        export_path,
        overwrite=overwrite,
        pixel_ratio=pixel_ratio,
    )
    create_plot_general(
        ImgMask(meteo, None),
        NamesTuple("METEO", tile),
        export_path,
        overwrite=overwrite,
        pixel_ratio=pixel_ratio,
    )


def process_tile_hist(
    input_directory: Path, tile: str, export_path: Path, overwrite: bool
) -> HistData:
    """Process a single tile with all the rois in it"""
    path_df_path = os.path.join(input_directory, tile, f"{tile}_tile_description.csv")
    path_df = pd.read_csv(path_df_path, sep="\t", index_col=0)
    rois = list(path_df.roi.unique())

    data_lists_tile: list[list[torch.Tensor]] = [[], [], [], [], []]

    for roi in rois:
        logging.info("Processing tile : %s roi : %s", tile, roi)

        images = open_data(input_directory, path_df, tile, roi)

        data_lists_tile[0].append(images["s1_set"])
        data_lists_tile[1].append(images["s1_valmasks"])

        data_lists_tile[2].append(images["s2_set"])
        data_lists_tile[3].append(images["s2_masks"])

        data_lists_tile[4].append(torch.cat([images[k] for k in METEO_BANDS], dim=1))

        explore_meteo(images, tile, roi, export_path)

    del images

    tile_hist_data = HistData(*[torch.cat(d) for d in data_lists_tile])

    create_plots(tile_hist_data, tile, export_path, overwrite, pixel_ratio=0.1)

    return tile_hist_data


def export_hist_from_nc(
    input_directory: Path,
    input_tiles: list[str],
    export_path: Path,
    overwrite: bool = False,
) -> None:
    """
    Process images per tile and create hist,
    then concat tiles and create hist for all data
    """
    data_lists_all: list[list[torch.Tensor]] = [[], [], [], [], []]

    for tile in tqdm(input_tiles):
        logging.info("Processing tile : %s", tile)

        data_hist_tile = process_tile_hist(
            input_directory, tile, export_path, overwrite
        )

        for enum, mod in enumerate(data_hist_tile):
            data_lists_all[enum].append(mod)

    # We create histograms for all selected data
    data_hist_all_tiles = HistData(*[torch.cat(d) for d in data_lists_all])

    create_plots(data_hist_all_tiles, "ALL", export_path, overwrite, pixel_ratio=0.1)


def explore_meteo(
    images: dict[str, torch.Tensor], tile: str, roi: int, export_path: Path
) -> None:
    """
    This function explores meteo data and plot patch image
    if Nans are detected
    """
    for k in METEO_BANDS:
        print(
            k,
            (images[k] < 0).sum(),
            (torch.isnan(images[k])).sum(),
            len(images[k].flatten()),
        )
        if (torch.isnan(images[k])).sum() > 0:
            binary_img = (torch.isnan(images[k]).sum(0) > 0).to(torch.int8)
            plt.imshow(binary_img[0] * 255, cmap="BuGn_r")
            label = f"Tile_{tile}_roi_{roi}_{k}"
            plt.xlabel(label + " " + str(torch.isnan(images[k]).sum().numpy()))
            plt.savefig(os.path.join(export_path, "Nans_" + label + ".png"))


def create_plot_s1(
    img_and_mask: ImgMask,
    export_label: Path,
    title: str,
    pixel_ratio: float = 0.1,
) -> None:
    """
    Creates histogram plots for S1 ascending and descending images.
    6 histograms in total
    """
    s1_asc, s1_desc = img_and_mask.img.split(3, 1)
    s1_asc_vm, s1_desc_vm = img_and_mask.mask.split(1, 1)

    plt.close()
    fig = plt.figure(figsize=(6 * 3, 20))

    fig.suptitle(title)

    plot_hist(
        ImgMask(s1_asc, s1_asc_vm),
        modality="S1 ASC",
        band_names=list(BANDS_S1) + ["ratio"],
        line=0,
        pixel_ratio=pixel_ratio,
    )
    plot_hist(
        ImgMask(s1_desc, s1_desc_vm),
        modality="S1 DESC",
        band_names=list(BANDS_S1) + ["ratio"],
        line=1,
        pixel_ratio=pixel_ratio,
    )

    fig.tight_layout()

    fig.savefig(export_label)


def create_plot_s2(
    img_and_mask: ImgMask,
    export_label: Path,
    title: str,
    pixel_ratio: float = 0.1,
) -> None:
    """
    Creates histogram plots for S2 image bands.
    10 histograms in total
    """
    s2_1, s2_2 = img_and_mask.img.split(5, 1)
    s2_mask = img_and_mask.mask.squeeze(1)

    plt.close()
    fig = plt.figure(figsize=(6 * 5, 20))

    fig.suptitle(title)

    plot_hist(
        ImgMask(s2_1, s2_mask),
        modality="S2",
        band_names=list(BANDS_S2)[:5],
        line=0,
        pixel_ratio=pixel_ratio,
    )
    plot_hist(
        ImgMask(s2_2, s2_mask),
        modality="S2",
        band_names=list(BANDS_S2)[5:],
        line=1,
        pixel_ratio=pixel_ratio,
    )

    fig.tight_layout()

    fig.savefig(export_label)


def create_plot_meteo(
    img_and_mask: ImgMask,
    export_label: Path,
    title: str,
    pixel_ratio: float = 0.1,
) -> None:
    """
    Creates histogram plots for meteo data.
    8 histograms in total
    """
    meteo_img, meteo_mask = img_and_mask
    meteo_1, meteo_2 = meteo_img.split(4, 1)
    if meteo_mask is None:
        meteo_mask = (meteo_img < 0).sum(dim=1, keepdim=True)

    fig = plt.figure(figsize=(6 * 4, 20))

    fig.suptitle(title)

    plot_hist(
        ImgMask(meteo_1, meteo_mask),
        modality="METEO",
        band_names=list(METEO_BANDS.keys())[:4],
        line=0,
        pixel_ratio=pixel_ratio,
    )
    plot_hist(
        ImgMask(meteo_2, meteo_mask),
        modality="METEO",
        band_names=list(METEO_BANDS.keys())[4:],
        line=1,
        pixel_ratio=pixel_ratio,
    )

    fig.tight_layout()

    fig.savefig(export_label)


plot_dict = {
    "S1": create_plot_s1,
    "S2": create_plot_s2,
    "METEO": create_plot_meteo,
}


def create_plot_general(
    img_and_mask: ImgMask,
    names: NamesTuple,
    export_path: Path,
    pixel_ratio: float = 0.1,
    overwrite: bool = False,
) -> None:
    """
    General function for histogram image creation for
    each modality, tile and roi
    """
    modality, tile = names
    image_name = f"mmdc_hist_{modality}_{tile}.png"
    export_label = Path(export_path) / image_name
    if not Path(export_label).exists() or overwrite:
        title = f"{modality} Histograms for Tile {tile}"
        plt.close()
        plot_dict[modality](img_and_mask, export_label, title, pixel_ratio)
        logging.info("exported: %s", export_label)
    else:
        logging.info("already exists: %s", export_label)


def plot_hist(
    img_and_mask: ImgMask,
    modality: str,
    band_names: list[str],
    line: int = 1,
    pixel_ratio: float = 0.1,
) -> None:
    """
    Function to compute and plot histogram and 5% and 95% quantiles
    """
    img, mask = img_and_mask
    quant_interval = torch.tensor([0.05, 0.95], dtype=torch.float)
    for col in range(img.shape[1]):
        band = img[:, col]
        values = torch.flatten(band[~mask.squeeze(1).to(torch.bool)])
        pix_nb = len(values)
        ax = plt.subplot2grid((2, img.shape[1]), (line, col))  # pylint: disable=C0103
        ax.hist(
            values[torch.randperm(pix_nb)[: int(pix_nb * pixel_ratio)]],
            bins=20 if modality == "METEO" else 100,
            density=True,
        )
        ax.set_title(f"{modality} {band_names[col]}")
        if pix_nb > MAX_SAMPLES_QUANTILE:
            quants = np.round(
                values[torch.randperm(10_000_000)]
                .to(torch.float32)
                .quantile(quant_interval)
            )
        else:
            quants = np.round(values.quantile(quant_interval).to(torch.float32))
        for quant in quants:
            plt.axvline(quant, color="red", linestyle="dashed", linewidth=1)
            plt.text(quant, 5, quant, rotation=90, verticalalignment="center")
        plt.xlabel(
            f"min={values.numpy().min().round(2)}, "
            f"max={values.numpy().max().round(2)}\n "
            f"q_0.05={quants[0]} q_0.95={quants[1]}"
        )


if __name__ == "__main__":
    # Parser arguments
    parser = get_parser()
    args = parser.parse_args()

    # Configure logging
    numeric_level = getattr(logging, args.loglevel.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError(f"Invalid log level:{args.loglevel}")

    logging.basicConfig(
        level=numeric_level,
        datefmt="%y-%m-%d %H:%M:%S",
        format="%(asctime)s :: %(levelname)s :: %(message)s",
    )

    logging.info(" input directory : %s", args.input_path)
    logging.info(" requested tiles : %s", args.input_tiles)
    logging.info(" output directory : %s", args.export_path)

    if Path(args.input_path).exists():
        export_hist_from_nc(
            input_directory=Path(args.input_path),
            input_tiles=args.input_tiles,
            export_path=Path(args.export_path),
            overwrite=args.force_write,
        )
    else:
        print(f"Folder {Path(args.input_path)} does not exist!!")

    logging.info("Histograms export finished")
