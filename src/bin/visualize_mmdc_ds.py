#!/usr/bin/env python3
# copyright: (c) 2022 cesbio / Centre National d'Etudes Spatiales

"""
Command line tool for the generation of charts with examples of
patches of an MMDC-SingleDate dataset
"""


import argparse
import logging
import os
from pathlib import Path

from mmdc_singledate.utils.explore_mmdc_dataset import export_visualization_graphs


def get_parser() -> argparse.ArgumentParser:
    """
    Generate argument parser for CLI
    """

    arg_parser = argparse.ArgumentParser(
        os.path.basename(__file__),
        description="Generate a png file for inspect the mmdc dataset",
    )

    arg_parser.add_argument(
        "--loglevel",
        default="INFO",
        choices=("DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"),
        help="Logger level (default: INFO. Should be one of "
        "(DEBUG, INFO, WARNING, ERROR, CRITICAL)",
    )

    arg_parser.add_argument(
        "--input_path",
        type=str,
        default="/work/CESBIO/projects/DeepChange/Ekaterina/MMDC_OE/",
        help="full path to the dataset directory",
        # required=True,
    )

    arg_parser.add_argument(
        "--input_tiles",
        type=list,
        default=["31TDL_8"],
        help="file containing the list of tiles and rois like T31TCJ_0",
        # required=True,
    )

    group = arg_parser.add_mutually_exclusive_group()
    group.add_argument(
        "--patch_index",
        nargs="+",
        default=[193],
        help="List of index of the patch's to visualize",
        required=False,
    )

    group.add_argument(
        "--nb_patches",
        type=int,
        default=None,
        help="number of index of the patch to visualize (select the index randomly)",
        required=False,
    )

    arg_parser.add_argument(
        "--export_path",
        default="/work/CESBIO/projects/DeepChange/Ekaterina/MMDC_OE/vizu",
        type=str,
        help="path to store results",
        # required=True
    )

    return arg_parser


def main(
    input_directory: Path,
    input_tiles: list[str],
    patch_index: list[int] | None,
    nb_patches: int | None,
    export_path: Path,
) -> None:
    """
    Entry point for the visualization
    """
    try:
        export_visualization_graphs(
            input_directory=input_directory,
            input_tiles=input_tiles,
            patch_index=patch_index,
            nb_patches=nb_patches,
            export_path=export_path,
        )

    except FileNotFoundError:
        if not input_directory.exists():
            print(f"Folder {input_directory} does not exist!!")

        if not export_path.exists():
            print(f"Folder {export_path} does not exist!!")


if __name__ == "__main__":
    # Parser arguments
    parser = get_parser()
    args = parser.parse_args()

    # check patch selection strategy
    if not (args.patch_index or args.nb_patches):
        parser.error(
            "Not patch indexing strategy requested,"
            " add --patch_index or --nb_patches"
        )

    # Configure logging
    numeric_level = getattr(logging, args.loglevel.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError(f"Invalid log level:{args.loglevel}")

    logging.basicConfig(
        level=numeric_level,
        datefmt="%y-%m-%d %H:%M:%S",
        format="%(asctime)s :: %(levelname)s :: %(message)s",
    )

    logging.info(" input directory : %s", args.input_path)
    logging.info(" requested tiles : %s", args.input_tiles)
    logging.info(" index patches : %s", args.patch_index)
    logging.info(" output directory : %s", args.export_path)

    # Go to main
    main(
        input_directory=Path(args.input_path),
        input_tiles=args.input_tiles,
        patch_index=args.patch_index,
        nb_patches=args.nb_patches,
        export_path=Path(args.export_path),
    )

    logging.info("Visualization export finished")
