"""Command line tool to run MMDC inference on full geotiffs"""


import argparse
import logging
from pathlib import Path

import torch

from mmdc_singledate.inference.processing import (
    InferenceConfig,
    OrbitType,
    S1InferenceParams,
    S2InferenceParams,
    inference,
)


def get_parser() -> argparse.ArgumentParser:
    """
    Generate argument parser for CLI
    """
    arg_parser = argparse.ArgumentParser(
        Path(__file__).name,
        description="MMDC Single Date inference",
    )

    arg_parser.add_argument(
        "--loglevel",
        default="INFO",
        choices=("DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"),
        help="Logger level (default: INFO. Should be one of "
        "(DEBUG, INFO, WARNING, ERROR, CRITICAL)",
    )

    arg_parser.add_argument(
        "--S2_roi", type=str, help="geotiff file with S2 ROI", required=False
    )

    arg_parser.add_argument(
        "--S1_asc_roi",
        type=str,
        help="geotiff file with S1 ascending ROI",
        required=False,
    )

    arg_parser.add_argument(
        "--S1_desc_roi",
        type=str,
        help="geotiff file with S1 descending ROI",
        required=False,
    )

    arg_parser.add_argument(
        "--SRTM_roi",
        type=str,
        help="geotiff file with SRTM ROI",
        required=True,
    )

    arg_parser.add_argument(
        "--WC_roi",
        type=str,
        help="geotiff file with WorldClim (Bio only) ROI",
        required=True,
    )

    arg_parser.add_argument(
        "--model",
        type=str,
        help="pth file containing the Pytorch model",
        required=True,
    )

    arg_parser.add_argument(
        "--out_dir",
        type=str,
        help="output directory where the inference images will be written",
        required=True,
    )

    return arg_parser


def configure_logging(cl_args: argparse.Namespace) -> None:
    """Configure logging"""
    numeric_level = getattr(logging, cl_args.loglevel.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError(f"Invalid log level: {cl_args.loglevel}")

    logging.basicConfig(
        level=numeric_level,
        datefmt="%y-%m-%d %H:%M:%S",
        format="%(asctime)s :: %(levelname)s :: %(message)s",
    )


def build_config(cl_args: argparse.Namespace) -> InferenceConfig:
    """Buiild the configuration from the command line arguments"""
    s1_config = None
    missing_orbit: OrbitType | None
    if cl_args.S1_asc_roi is not None or cl_args.S1_desc_roi is not None:
        match (cl_args.S1_asc_roi, cl_args.S1_desc_roi):
            case (_, None):
                missing_orbit = OrbitType.DESC
            case (None, _):
                missing_orbit = OrbitType.ASC
            case _:
                missing_orbit = None
        s1_config = S1InferenceParams(
            cl_args.S1_asc_roi,
            cl_args.S1_desc_roi,
            missing_orbit,
            cl_args.SRTM_roi,
            cl_args.WC_roi,
        )
    s2_config = None
    if cl_args.S2_roi is not None:
        s2_config = S2InferenceParams(cl_args.S2_roi, cl_args.SRTM_roi, cl_args.WC_roi)
    if s1_config is s2_config is None:
        raise RuntimeError("At least one of S1 ASC, S1 DESC or S2 have to be provided.")
    return InferenceConfig(s1_config, s2_config, cl_args.model, cl_args.out_dir)


if __name__ == "__main__":
    parser = get_parser()
    args = parser.parse_args()
    configure_logging(args)
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    config = build_config(args)
    if config.s1_config is not None:
        inference(config.s1_config, config.model, config.out_dir, device=device)
    if config.s2_config is not None:
        inference(config.s2_config, config.model, config.out_dir, device=device)
