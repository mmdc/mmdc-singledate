#!/usr/bin/env python3
# copyright: (c) 2022 cesbio / Centre National d'Etudes Spatiales

""" Split the list of ROIS into train, val and test. Ensure there is no overlap."""

import argparse
import glob
import logging
import os
import random
from pathlib import Path


def get_parser() -> argparse.ArgumentParser:
    """
    Generate argument parser for CLI
    """

    args_parser = argparse.ArgumentParser(
        os.path.basename(__file__),
        description="Split the tiles between train,val, and test",
    )

    args_parser.add_argument(
        "--loglevel",
        default="INFO",
        choices=("DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"),
        help="Logger level (default: INFO. Should be one of "
        "(DEBUG, INFO, WARNING, ERROR, CRITICAL)",
    )

    args_parser.add_argument(
        "--tensors_dir",
        type=str,
        help="full path to the exported tensors per ROI",
        required=True,
    )

    args_parser.add_argument(
        "--roi_intersections_file",
        type=str,
        help="file containing the intersections between ROIs",
        required=True,
    )

    args_parser.add_argument(
        "--test_percentage",
        type=float,
        help="percentage of the data used for training",
        required=True,
    )

    args_parser.add_argument(
        "--random_state", type=int, help="Set a value for", required=False, default=42
    )

    return args_parser


CoocurrenceType = list[str | list[str]]


def flatten(occ_list: CoocurrenceType) -> list[str]:
    """Flatten the list of rois (list of lists)"""
    return [roi for roi_list in occ_list for roi in roi_list]


def get_available_rois(
    tensors_dir: Path,
) -> set[str]:
    """Get the available rois in the tensor's dir as TILE_roi"""
    available_tensors = [
        Path(t).stem for t in glob.glob(f"{tensors_dir}/T*/T*_s1_set_*_?.pth")
    ]
    available_rois = set()
    for aval_tens in available_tensors:
        tile = aval_tens.split("_")[0]
        roi = aval_tens.split("_")[-1]
        available_rois.add(f"{tile}_{roi}")
    return available_rois


def split_roi_set(
    available_rois: set[str],
    coocurrence_list: CoocurrenceType,
    test_percentage: float,
    random_state: int,
) -> tuple[set[str], ...]:
    """Generate the train, val, test roi split form the co-occurence
    list and the available rois"""
    test_percentage = (
        test_percentage if test_percentage < 1.0 else test_percentage / 100
    )
    total_rois = len(coocurrence_list)
    nb_test = int(test_percentage * total_rois)
    nb_val = int((total_rois - nb_test) * 0.25)
    nb_train = total_rois - nb_val - nb_test

    random.Random(random_state).shuffle(coocurrence_list)

    train_rois = set(flatten(coocurrence_list[:nb_train])) & available_rois
    val_rois = (
        set(flatten(coocurrence_list[nb_train : nb_train + nb_val])) & available_rois
    )
    test_rois = set(flatten(coocurrence_list[nb_train + nb_val :])) & available_rois

    # remove intersections
    val_rois = (val_rois - train_rois) - test_rois
    train_rois = train_rois - test_rois

    return train_rois, val_rois, test_rois


def split_tile_rois(
    tensors_dir: Path,
    roi_intersections_file: Path,
    test_percentage: float,
    random_state: int,
) -> None:
    """
    Split the tiles/rois between train/val/test
    """
    # read the tile_rois co-ocurrences

    with roi_intersections_file.open(encoding="utf-8") as roi_intersect:
        coocurrence_list: CoocurrenceType = [line.split() for line in roi_intersect]

    available_rois = get_available_rois(tensors_dir)
    train_rois, val_rois, test_rois = split_roi_set(
        available_rois, coocurrence_list, test_percentage, random_state
    )

    for roi_file, roi_list in (
        ("train_rois", train_rois),
        ("val_rois", val_rois),
        ("test_rois", test_rois),
    ):
        with open(
            os.path.join(tensors_dir, f"{roi_file}.txt"), "w", encoding="utf-8"
        ) as roi_intersect:
            roi_intersect.writelines(sorted([f"{roi}\n" for roi in roi_list]))


def main(
    input_dir: Path, input_file: Path, test_percentage: int, random_state: int
) -> None:
    """
    Split the tiles/rois between train/val/test
    """
    split_tile_rois(input_dir, input_file, test_percentage, random_state)


if __name__ == "__main__":
    # Parser arguments
    parser = get_parser()
    args = parser.parse_args()

    # Configure logging
    numeric_level = getattr(logging, args.loglevel.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError(f"Invalid log level: {args.loglevel}")

    logging.basicConfig(
        level=numeric_level,
        datefmt="%y-%m-%d %H:%M:%S",
        format="%(asctime)s :: %(levelname)s :: %(message)s",
    )

    logging.info("input directory : %s", args.tensors_dir)
    logging.info("input file : %s", args.roi_intersections_file)
    logging.info("test percentage selected : %s", args.test_percentage)
    logging.info("random state value :  %s", args.random_state)

    # Go to main
    main(
        Path(args.tensors_dir),
        Path(args.roi_intersections_file),
        args.test_percentage,
        args.random_state,
    )
