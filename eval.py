#!/usr/bin/env python3
# Copyright (c) 2022 CESBIO / Centre National d'Etudes Spatiales
"""
Eval Models

Inpired by:
https://github.com/ashleve/lightning-hydra-template/blob/main/src/eval.py
"""

import logging

from omegaconf import DictConfig

# Configure logging
numeric_level = getattr(logging, "INFO", None)
logging.basicConfig(
    level=numeric_level, format="%(asctime)-15s %(levelname)s: %(message)s"
)
logger = logging.getLogger(__name__)


def evaluate(cfg: DictConfig) -> tuple[dict, dict]:
    """Evaluates given checkpoints on a datamodule testset

    Args: cfg(dictConfig): Configuration composed by hydra

    Returns: Tuple[dict, dict] : Dict with metrics and dict with all
        instantiated objects

    """
    assert cfg.ckpt_path

    logger.info("")

    return {}, {}


def main(cfg: DictConfig) -> None:
    evaluate(cfg)


if __name__ == "__main__":
    main()
