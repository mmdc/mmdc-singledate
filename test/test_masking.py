#!/usr/bin/env python3
# Copyright: (c) 2022 CESBIO / Centre National d'Etudes Spatiales
"""
Test the explore dataset functionalities
"""

import numpy as np
import pytest
import torch

from mmdc_singledate.datamodules.datatypes import (
    MMDCShiftScales,
    MMDCTensorStats,
    ShiftScale,
)
from mmdc_singledate.models.components.masking import DataMasking, MaskingStrategy

from .utils import setup_zero_data

EPOCHS_THR_LIST = [1, 5, 10]
MASKED_PERC_LIST = [10, 50, 100]
STRATEGY_LIST: list[MaskingStrategy] = [
    "random",
    "one_satellite_random",
    "one_satellite",
]
FILL = -10
BATCH, HEIGHT, WIDTH = 20, 10, 10


@pytest.fixture
def masking():
    return DataMasking(EPOCHS_THR_LIST, MASKED_PERC_LIST, STRATEGY_LIST)


@pytest.fixture
def dummy_scales():
    s1_stats = MMDCTensorStats(
        mean=torch.tensor([[-1.1044, -1.7885, -0.6859, -1.1542, -1.8245, -0.6728]]),
        std=torch.tensor([[0.3650, 0.4343, 0.4052, 0.3733, 0.4305, 0.4245]]),
        qmin=torch.tensor([[-1.7013, -2.5391, -1.3731, -1.7629, -2.5776, -1.3961]]),
        median=torch.tensor([[-1.0978, -1.7536, -0.6602, -1.1459, -1.7811, -0.6441]]),
        qmax=torch.tensor([[-0.1956, -0.8585, 0.1829, -0.2228, -0.9433, 0.2442]]),
    )
    s2_stats = MMDCTensorStats(
        mean=torch.tensor(
            [
                [
                    517.5057,
                    812.4586,
                    799.1879,
                    1332.3846,
                    2602.7866,
                    3077.1292,
                    3278.4924,
                    3355.4651,
                    2455.1719,
                    1583.9185,
                ]
            ]
        ),
        std=torch.tensor(
            [
                [
                    332.2384,
                    375.5963,
                    580.1564,
                    515.8276,
                    763.9240,
                    990.3655,
                    1072.9083,
                    1011.4355,
                    922.1917,
                    863.8082,
                ]
            ]
        ),
        qmin=torch.tensor(
            [
                [
                    144.0,
                    348.0,
                    185.0,
                    631.0,
                    1411.0,
                    1601.0,
                    1651.0,
                    1789.0,
                    1243.0,
                    611.0,
                ]
            ]
        ),
        median=torch.tensor(
            [
                [
                    418.0,
                    721.0,
                    603.0,
                    1216.0,
                    2577.0,
                    3001.0,
                    3215.0,
                    3322.0,
                    2196.0,
                    1289.0,
                ]
            ]
        ),
        qmax=torch.tensor(
            [
                [
                    1182.0,
                    1572.0,
                    2004.0,
                    2349.0,
                    3973.0,
                    4907.0,
                    5240.0,
                    5161.0,
                    4227.0,
                    3322.0,
                ]
            ]
        ),
    )

    scales = MMDCShiftScales(
        sen1=ShiftScale(shift=s1_stats.mean, scale=s1_stats.std),
        sen2=ShiftScale(shift=s2_stats.mean, scale=s2_stats.std),
        meteo=ShiftScale(shift=s2_stats.mean, scale=s2_stats.std),
        dem=ShiftScale(shift=s2_stats.mean, scale=s2_stats.std),
    )
    return scales


@pytest.fixture
def dummy_batch():
    return setup_zero_data(BATCH, HEIGHT, WIDTH, fill=FILL)


def compare_tensors(a, b, summ=False):
    """
    compare two tensors,
    elemet-wise or by using their sum
    """
    if summ:
        return a.sum() == b.sum()
    return torch.all(torch.eq(a, b))


@pytest.mark.trex
def test_get_strategy(masking) -> None:
    """
    test get strategy function for patches
    """
    for epoch in range(12):
        strat = masking.get_strategy(current_epoch=epoch)
        if epoch < EPOCHS_THR_LIST[0]:
            assert strat is None
        else:
            masked_perc, strategy = strat
            for i in range(1, len(EPOCHS_THR_LIST)):
                print(f"{epoch=}, {i=}")
                if epoch == EPOCHS_THR_LIST[i]:
                    assert masked_perc == MASKED_PERC_LIST[i]
                    assert strategy == STRATEGY_LIST[i]
            if epoch > EPOCHS_THR_LIST[-1]:
                assert masked_perc == 100
                assert strategy == "one_satellite"


@pytest.mark.trex
def test_mask_data(masking, dummy_scales, dummy_batch) -> None:
    """
    Test that the correct band ratio is masked
    """
    masking.scales = dummy_scales
    for epoch in range(0, EPOCHS_THR_LIST[-1] + 2):
        masked_batch = dummy_batch.copy()
        strat = masking.get_strategy(current_epoch=epoch)
        masked_batch = masking.mask_data(epoch, masked_batch)
        print(epoch)
        if epoch < EPOCHS_THR_LIST[0]:
            pass
        else:
            masked_perc, strategy = strat
            if strategy == "random":
                assert not compare_tensors(
                    masked_batch.s2_x, dummy_batch.s2_x, summ=True
                )
                assert not compare_tensors(
                    masked_batch.s1_x, dummy_batch.s1_x, summ=True
                )
                cat = torch.cat((masked_batch.s1_x, masked_batch.s2_x), 1)
                masked_bands_nb = round(cat.shape[1] * masked_perc / 100)
                assert (cat != FILL).sum() / torch.numel(
                    cat
                ) * 100 == masked_bands_nb / cat.shape[1] * 100
            elif strategy == "one_satellite_random":
                # One satellite is randomly masked in each image of a batch
                masked_bands_nb_s1 = np.round(
                    masked_batch.s1_x.shape[1] * masked_perc / 100
                )
                masked_bands_nb_s2 = np.round(
                    masked_batch.s2_x.shape[1] * masked_perc / 100
                )
                for b in range(BATCH):
                    assert compare_tensors(
                        masked_batch.s2_x[b], dummy_batch.s2_x[b]
                    ) is not compare_tensors(masked_batch.s1_x[b], dummy_batch.s1_x[b])
                    assert (
                        int(
                            torch.round(
                                (masked_batch.s1_x[b] != FILL).sum()
                                / torch.numel(masked_batch.s1_x[b])
                                * 100
                            )
                        )
                        == np.round(
                            masked_bands_nb_s1 / masked_batch.s1_x.shape[1] * 100
                        )
                    ) is not (
                        int(
                            torch.round(
                                (masked_batch.s2_x[b] != FILL).sum()
                                / torch.numel(masked_batch.s2_x[b])
                                * 100
                            )
                        )
                        == np.round(
                            masked_bands_nb_s2 / masked_batch.s2_x.shape[1] * 100
                        )
                    )
            else:
                assert compare_tensors(
                    masked_batch.s2_x, dummy_batch.s2_x
                ) is not compare_tensors(masked_batch.s1_x, dummy_batch.s1_x)
                assert compare_tensors(
                    masked_batch.s2_a, dummy_batch.s2_a
                ) is not compare_tensors(masked_batch.s1_a, dummy_batch.s1_a)
                assert (
                    (masked_batch.s2_x != FILL).sum() == torch.numel(masked_batch.s2_x)
                    and (masked_batch.s1_x != FILL).sum() == 0
                ) is not (
                    (masked_batch.s1_x != FILL).sum() == torch.numel(masked_batch.s1_x)
                    and (masked_batch.s2_x != FILL).sum() == 0
                )


@pytest.mark.trex
def test_get_gaussian_bands(masking, dummy_scales, dummy_batch):
    """
    Test that masking bands have the same size as S1/S2 data
    """
    masking.scales = dummy_scales
    for satellite in ("S1", "S2", "both", "both_one_per_sample"):
        gaussian_bands = masking.get_gaussian_bands(
            size=[BATCH, HEIGHT, WIDTH], satellite=satellite
        )
        if satellite == "S1":
            assert gaussian_bands.shape == dummy_batch.s1_x.shape
        if satellite == "S2":
            assert gaussian_bands.shape == dummy_batch.s2_x.shape
        if "both" in satellite:
            assert (
                gaussian_bands.shape
                == torch.cat((dummy_batch.s1_x, dummy_batch.s2_x), 1).shape
            )
