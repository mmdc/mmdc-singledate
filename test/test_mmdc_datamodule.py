#!/usr/bin/env/ python
# Copyright: (c) 2022 CESBIO / Centre National d'Etudes Spatiales


from pathlib import Path

import pytest
from torch.utils.data import DataLoader

from mmdc_singledate.datamodules.mmdc_datamodule import (
    IterableMMDCDataset,
    MMDCDataLoaderConfig,
    MMDCDataModule,
    MMDCDataPaths,
)

DATASET_DIR = "/work/CESBIO/projects/DeepChange/Ekaterina/MMDC_OE/"  # TREX
if Path("/gpfsscratch/rech/adz/uzh16pa/MMDC").exists():  # Jean Zay
    DATASET_DIR = "/gpfsscratch/rech/adz/uzh16pa/MMDC/"
TILES_CONFIG_DIR = f"{DATASET_DIR}/tiles_conf_training/tiny"

MMDC_DATA_COMPONENTS = 8


def build_datamodule(
    batch_size: int = 10,
    patch_size: int | None = None,
) -> tuple[MMDCDataModule, MMDCDataLoaderConfig]:
    dlc = MMDCDataLoaderConfig(
        max_open_files=1,
        batch_size_train=batch_size,
        batch_size_val=10,
        num_workers=1,
        pin_memory=False,
        patch_size=patch_size,
    )
    dpth = MMDCDataPaths(
        tensors_dir=Path(DATASET_DIR),
        train_rois=Path(f"{TILES_CONFIG_DIR}/train_tiny.txt"),
        val_rois=Path(f"{TILES_CONFIG_DIR}/val_tiny.txt"),
        test_rois=Path(f"{TILES_CONFIG_DIR}/test_tiny.txt"),
    )
    dm = MMDCDataModule(
        data_paths=dpth,
        dl_config=dlc,
    )
    return dm, dlc


@pytest.mark.trex
def test_mmdc_datamodule_random_available_tile() -> None:
    """
    test lightning datamodule
    """

    dm, _ = build_datamodule()
    dm.find_available_tiles_and_rois()
    assert dm.data_paths.train_rois
    assert dm.data_paths.val_rois
    assert dm.data_paths.test_rois

    dm.setup()
    dm.get_stats()
    for ddd in dm.files.stats_files:
        for dddd in ddd:
            assert Path(dddd).is_file()


def build_iterable_ds() -> (
    tuple[IterableMMDCDataset, MMDCDataLoaderConfig, MMDCDataModule]
):
    BATCH_SIZE = 4
    dlc = MMDCDataLoaderConfig(
        max_open_files=1,
        batch_size_train=BATCH_SIZE,
        batch_size_val=BATCH_SIZE,
        num_workers=1,
        pin_memory=False,
    )
    dpth = MMDCDataPaths(
        tensors_dir=Path(DATASET_DIR),
        train_rois=Path(f"{TILES_CONFIG_DIR}/train_small.txt"),
        val_rois=Path(f"{TILES_CONFIG_DIR}/val_small.txt"),
        test_rois=Path(f"{TILES_CONFIG_DIR}/test_small.txt"),
    )
    dm = MMDCDataModule(
        data_paths=dpth,
        dl_config=dlc,
    )
    ds = IterableMMDCDataset(
        files=dm.build_file_list(dpth.train_rois),
        batch_size=dlc.batch_size_train,
        max_open_files=dlc.max_open_files,
    )
    return ds, dlc, dm


def build_data_loader(
    batch_size: int = 10,
    patch_size: int | None = None,
) -> tuple[DataLoader, MMDCDataLoaderConfig, MMDCDataModule]:
    dm, dlc = build_datamodule(batch_size, patch_size)
    dm.setup("fit")
    dl = dm.train_dataloader()
    assert dm.data is not None
    assert isinstance(dm.data.train, IterableMMDCDataset)
    print(f"{dm.data.train.max_open_files=}")
    return dl, dlc, dm


@pytest.mark.trex
@pytest.mark.parametrize("patch_size", [(None), (8)])
def test_iterablemmdcdataset(patch_size: int | None) -> None:
    dl, dlc, _ = build_data_loader(patch_size=patch_size)
    print(f"{dlc=}")
    for batch, _ in zip(dl, range(3), strict=False):
        assert len(batch) == dlc.batch_size_train
        assert len(batch[0]) == MMDC_DATA_COMPONENTS
        if patch_size is not None:
            assert batch[0][0].shape[-1] == patch_size
