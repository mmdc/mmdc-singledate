# Copyright: (c) 2022 CESBIO / Centre National d'Etudes Spatiales

from typing import cast

import pytest
import torch

from mmdc_singledate.models.components.masking import DataMasking
from mmdc_singledate.models.datatypes import S1S2VAELosses, S1S2VAEMaskingLosses
from mmdc_singledate.models.lightning.full_experts import MMDCFullExpertsLitModule
from mmdc_singledate.models.torch.full_experts import MMDCFullExpertsModule

from .test_mmdc_full_module import DEVICE, build_config, build_data_loader


@pytest.mark.trex
def test_experts_lightning_module() -> None:
    config = build_config(True, True, True, True)
    config.experts_strategy = "MoE"
    data_masking = DataMasking(
        [0, 1, 2], [0, 10, 20], ["random", "one_satellite_random", "one_satellite"]
    )
    model = MMDCFullExpertsModule(config).to(DEVICE)
    module = MMDCFullExpertsLitModule(model, data_masking)
    dl, _, dm = build_data_loader(batch_size=2)
    dm.setup("fit")
    module.set_stats(dm.get_stats())
    for batch, _ in zip(dl, range(3), strict=False):
        # More elegant batch transfer does not work
        for b in range(len(batch)):
            for a in range(len(batch[b])):
                batch[b][a] = batch[b][a].to(DEVICE)
        loss = module.step(batch)
        loss, mask_loss = cast(
            tuple[S1S2VAELosses, S1S2VAEMaskingLosses],
            module.step(batch, masking_loss=True),
        )
        print(f"{loss=}")
        print(f"{mask_loss=}")
        for loss_com in (
            loss.s1_rec,
            loss.s2_rec,
            loss.s1_cross,
            loss.s2_cross,
            loss.code,
            loss.covar_code,
        ):
            if loss_com is not None:
                assert not torch.isnan(loss_com).any()
