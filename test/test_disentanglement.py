import torch

from mmdc_singledate.models.components.losses import off_diagonal_covar_loss


def test_covariance_loss():
    batch_size = 16
    nb_vars = 4
    x = torch.tensor(range(batch_size * nb_vars)).reshape(batch_size, nb_vars)
    loss = off_diagonal_covar_loss(x)
    assert torch.isclose(loss, torch.Tensor([272.0]))
    new_batch_size = 4
    patch_size = 2
    y = torch.zeros(new_batch_size, nb_vars, patch_size, patch_size)
    for b in range(new_batch_size):
        for i in range(patch_size):
            for j in range(patch_size):
                k = b * patch_size * patch_size + i * patch_size + j
                y[b, :, i, j] = x[k, :]
    loss = off_diagonal_covar_loss(y)
    assert torch.isclose(loss, torch.Tensor([272.0]))
