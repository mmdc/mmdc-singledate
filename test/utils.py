#!/usr/bin/env/ python
# Copyright: (c) 2022 CESBIO / Centre National d'Etudes Spatiales

from pathlib import Path

import torch

from mmdc_singledate.datamodules.datatypes import (
    MMDCBatch,
    MMDCDataStats,
    MMDCMeteoData,
    MMDCMeteoStats,
    MMDCShiftScales,
    ShiftScale,
)

from .test_mmdc_datamodule import build_data_loader

DATASET_DIR = "/work/CESBIO/projects/DeepChange/Ekaterina/MMDC_OE"
if Path("/gpfsscratch/rech/adz/uzh16pa/MMDC/").exists():
    DATASET_DIR = "/gpfsscratch/rech/adz/uzh16pa/MMDC/"
TILE = "30TXT"
PATCH_SIZE = "256x256"
ROI = 1


def setup_zero_data(batch: int, height: int, width: int, fill: int = 0):
    dummy_batch = MMDCBatch(
        s2_x=torch.zeros(size=[batch, 10, height, width]) + fill,
        s2_m=torch.zeros(size=[batch, 1, height, width]),
        s2_a=torch.zeros(size=[batch, 6, height, width]) + fill,
        s1_x=torch.zeros(size=[batch, 6, height, width]) + fill,
        s1_vm=torch.zeros(size=[batch, 2, height, width]),
        s1_a=torch.zeros(size=[batch, 2, height, width]) + fill,
        meteo_x=torch.zeros(size=[batch, 48, height, width]),
        dem_x=torch.zeros(size=[batch, 1, height, width]),
    )
    return dummy_batch


def setup_data(nb_samples=1):
    dl, _, _ = build_data_loader()
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    s2_x = torch.load(f"{DATASET_DIR}/{TILE}/{TILE}_s2_set_{PATCH_SIZE}_{ROI}.pth")[
        :nb_samples
    ].to(device)
    s2_m = torch.load(f"{DATASET_DIR}/{TILE}/{TILE}_s2_masks_{PATCH_SIZE}_{ROI}.pth")[
        :nb_samples
    ].to(device)
    s2_a = torch.load(f"{DATASET_DIR}/{TILE}/{TILE}_s2_angles_{PATCH_SIZE}_{ROI}.pth")[
        :nb_samples
    ].to(device)
    s1_x = torch.load(f"{DATASET_DIR}/{TILE}/{TILE}_s1_set_{PATCH_SIZE}_{ROI}.pth")[
        :nb_samples
    ].to(device)
    s1_vm = torch.load(
        f"{DATASET_DIR}/{TILE}/{TILE}_s1_valmasks_{PATCH_SIZE}_{ROI}.pth"
    )[:nb_samples].to(device)
    s1_a = torch.load(f"{DATASET_DIR}/{TILE}/{TILE}_s1_angles_{PATCH_SIZE}_{ROI}.pth")[
        :nb_samples
    ].to(device)
    dem_x = torch.load(f"{DATASET_DIR}/{TILE}/{TILE}_dem_{PATCH_SIZE}_{ROI}.pth")[
        :nb_samples
    ].to(device)
    meteo_x = MMDCMeteoData(
        torch.load(
            f"{DATASET_DIR}/{TILE}/{TILE}_meteo_dew_temp_{PATCH_SIZE}_{ROI}.pth"
        )[:nb_samples].to(device),
        torch.load(f"{DATASET_DIR}/{TILE}/{TILE}_meteo_prec_{PATCH_SIZE}_{ROI}.pth")[
            :nb_samples
        ].to(device),
        torch.load(f"{DATASET_DIR}/{TILE}/{TILE}_meteo_sol_rad_{PATCH_SIZE}_{ROI}.pth")[
            :nb_samples
        ].to(device),
        torch.load(
            f"{DATASET_DIR}/{TILE}/{TILE}_meteo_temp_max_{PATCH_SIZE}_{ROI}.pth"
        )[:nb_samples].to(device),
        torch.load(
            f"{DATASET_DIR}/{TILE}/{TILE}_meteo_temp_mean_{PATCH_SIZE}_{ROI}.pth"
        )[:nb_samples].to(device),
        torch.load(
            f"{DATASET_DIR}/{TILE}/{TILE}_meteo_temp_min_{PATCH_SIZE}_{ROI}.pth"
        )[:nb_samples].to(device),
        torch.load(
            f"{DATASET_DIR}/{TILE}/{TILE}_meteo_vap_press_{PATCH_SIZE}_{ROI}.pth"
        )[:nb_samples].to(device),
        torch.load(
            f"{DATASET_DIR}/{TILE}/{TILE}_meteo_wind_speed_{PATCH_SIZE}_{ROI}.pth"
        )[:nb_samples].to(device),
    ).concat_data()

    return MMDCBatch(
        s2_x,
        s2_m,
        s2_a,
        s1_x,
        s1_vm,
        s1_a,
        torch.flatten(meteo_x, start_dim=1, end_dim=2),
        dem_x,
    )


def get_scales():
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    meteo = MMDCMeteoStats(
        torch.load(
            f"{DATASET_DIR}/{TILE}/{TILE}_stats_meteo_dew_temp_{PATCH_SIZE}_{ROI}.pth"
        ),
        torch.load(
            f"{DATASET_DIR}/{TILE}/{TILE}_stats_meteo_prec_{PATCH_SIZE}_{ROI}.pth"
        ),
        torch.load(
            f"{DATASET_DIR}/{TILE}/{TILE}_stats_meteo_sol_rad_{PATCH_SIZE}_{ROI}.pth"
        ),
        torch.load(
            f"{DATASET_DIR}/{TILE}/{TILE}_stats_meteo_temp_max_{PATCH_SIZE}_{ROI}.pth"
        ),
        torch.load(
            f"{DATASET_DIR}/{TILE}/{TILE}_stats_meteo_temp_mean_{PATCH_SIZE}_{ROI}.pth"
        ),
        torch.load(
            f"{DATASET_DIR}/{TILE}/{TILE}_stats_meteo_temp_min_{PATCH_SIZE}_{ROI}.pth"
        ),
        torch.load(
            f"{DATASET_DIR}/{TILE}/{TILE}_stats_meteo_vap_press_{PATCH_SIZE}_{ROI}.pth"
        ),
        torch.load(
            f"{DATASET_DIR}/{TILE}/{TILE}_stats_meteo_wind_speed_{PATCH_SIZE}_{ROI}.pth"
        ),
    ).concat_stats()
    stats = MMDCDataStats(
        torch.load(f"{DATASET_DIR}/{TILE}/{TILE}_stats_s2_{PATCH_SIZE}_{ROI}.pth"),
        torch.load(f"{DATASET_DIR}/{TILE}/{TILE}_stats_s1_{PATCH_SIZE}_{ROI}.pth"),
        meteo,
        torch.load(f"{DATASET_DIR}/{TILE}/{TILE}_stats_dem_{PATCH_SIZE}_{ROI}.pth"),
    )
    scale_regul = torch.nn.Threshold(1e-10, 1.0)
    shift_scale_s2 = ShiftScale(
        stats.sen2.median.to(device),
        scale_regul((stats.sen2.qmax - stats.sen2.qmin) / 2.0).to(device),
    )
    shift_scale_s1 = ShiftScale(
        stats.sen1.median.to(device),
        scale_regul((stats.sen1.qmax - stats.sen1.qmin) / 2.0).to(device),
    )
    shift_scale_meteo = ShiftScale(
        stats.meteo.median.to(device),
        scale_regul((stats.meteo.qmax - stats.meteo.qmin) / 2.0).to(device),
    )
    shift_scale_dem = ShiftScale(
        stats.dem.median.to(device),
        scale_regul((stats.dem.qmax - stats.dem.qmin) / 2.0).to(device),
    )

    return MMDCShiftScales(
        shift_scale_s2, shift_scale_s1, shift_scale_meteo, shift_scale_dem
    )
