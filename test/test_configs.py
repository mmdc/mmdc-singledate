#!/usr/bin/env/ python
# Copyright: (c) 2022 CESBIO / Centre National d'Etudes Spatiales

import hydra
import pytest
from hydra import compose, initialize


@pytest.mark.skip(reason="Have to look into hydra testing")
def test_train_config():
    """
    test the properly reading of the configutations

    """

    with initialize(version_base=None, config_path="../configs"):
        cfg_train = compose(config_name="train")
        assert cfg_train
        assert cfg_train.datamodule
        assert cfg_train.model
        assert cfg_train.trainer

        hydra.utils.instantiate(cfg_train.datamodule)
        hydra.utils.instantiate(cfg_train.model)
