#!/usr/bin/env/ python
# Copyright: (c) 2022 CESBIO / Centre National d'Etudes Spatiales


import pytest
import torch

from mmdc_singledate.models.datatypes import (
    ConvnetParams,
    LatentLossWeights,
    MMDCDataUse,
    MMDCForwardType,
    MultiVAEAuxUseConfig,
    MultiVAEConfig,
    MultiVAEEncDecConfig,
    MultiVAELossWeights,
    S1FowardType,
    S1S2VAEForwardType,
    S2FowardType,
    UnetParams,
)
from mmdc_singledate.models.lightning.full import MMDCFullLitModule

# from mmdc_singledate.callbacks.image_callbacks import MMDCAEAuxCallback
from mmdc_singledate.models.torch.full import MMDCFullModule

from .test_architecture import DATASIZES
from .test_architecture import build_config as emb_config
from .test_mmdc_datamodule import build_data_loader
from .utils import get_scales, setup_data

DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def build_config(use_s1_angles, use_s2_angles, use_srtm, use_wc) -> MultiVAEConfig:
    embedding = emb_config().embeddings
    s1_encoder_params = UnetParams(4, [64, 128, 256, 512], 3, 3)
    s2_encoder_params = UnetParams(4, [64, 128, 256, 512], 3, 3)
    s1_decoder_params = ConvnetParams(0, [64, 32, 16], [3, 3, 3, 3])
    s2_decoder_params = ConvnetParams(0, [64, 32, 16], [3, 3, 3, 3])
    s1_enc_use = MMDCDataUse(use_s1_angles, use_s2_angles, use_srtm, use_wc)
    s2_enc_use = MMDCDataUse(use_s1_angles, use_s2_angles, use_srtm, use_wc)
    s1_dec_use = MMDCDataUse(use_s1_angles, use_s2_angles, use_srtm, use_wc)
    s2_dec_use = MMDCDataUse(use_s1_angles, use_s2_angles, use_srtm, use_wc)
    loss_weights = MultiVAELossWeights(
        1.0,
        LatentLossWeights(0.1, 0.1, False),
        1.0,
    )
    return MultiVAEConfig(
        DATASIZES,
        embedding,
        MultiVAEEncDecConfig(
            s1_encoder_params,
            s2_encoder_params,
            s1_decoder_params,
            s2_decoder_params,
            MultiVAEAuxUseConfig(s1_enc_use, s2_enc_use, s1_dec_use, s2_dec_use),
        ),
        loss_weights,
    )


@pytest.mark.parametrize(
    "use_s1_angles, use_s2_angles, use_srtm, use_wc",
    [
        (False, False, False, False),
        (False, False, False, True),
        (False, False, True, True),
        (True, True, False, True),
        (True, False, False, True),
        (True, True, True, True),
    ],
)
def test_mmdc_full_instantiate(use_s1_angles, use_s2_angles, use_srtm, use_wc):
    config = build_config(use_s1_angles, use_s2_angles, use_srtm, use_wc)
    print(config.embeddings.s1_angles)
    model = MMDCFullModule(config)
    assert model
    assert model


@pytest.mark.trex
@pytest.mark.parametrize(
    "use_s1_angles, use_s2_angles, use_srtm, use_wc",
    [
        (False, False, False, False),
        (False, False, False, True),
        (False, False, True, True),
        (True, True, False, True),
        (True, False, False, True),
    ],
)
def test_mmdc_full_inputs(use_s1_angles, use_s2_angles, use_srtm, use_wc):
    """
    Test decoders with different inputs
    """

    data = setup_data()

    print(f"{data.meteo_x.shape=}, {data.dem_x.shape}")

    config = build_config(use_s1_angles, use_s2_angles, use_srtm, use_wc)
    model = MMDCFullModule(config)
    # model = torch.compile(model)
    scales = get_scales()
    model.set_scales(scales)
    pred: S1S2VAEForwardType = model(
        MMDCForwardType(
            S2FowardType(data.s2_x, data.s2_m, data.s2_a),
            S1FowardType(data.s1_x, data.s1_vm, data.s1_a),
            data.meteo_x,
            data.dem_x,
        )
    )
    assert pred.recons.s1_pred.mean.shape == data.s1_x.shape


@pytest.mark.trex
def test_full_lightning_module():
    config = build_config(True, True, True, True)
    model = MMDCFullModule(config).to(DEVICE)
    module = MMDCFullLitModule(model)
    dl, _, dm = build_data_loader()
    dm.setup("fit")
    module.set_stats(dm.get_stats())
    for batch, _ in zip(dl, range(3), strict=False):
        # More elegant batch transfer does not work
        for b in range(len(batch)):
            for a in range(len(batch[b])):
                batch[b][a] = batch[b][a].to(DEVICE)
        loss = module.step(batch)
        print(f"{loss=}")
        for loss_com in (
            loss.s1_rec,
            loss.s2_rec,
            loss.s1_cross,
            loss.s2_cross,
            loss.code,
            loss.covar_code,
        ):
            if loss_com is not None:
                assert not torch.isnan(loss_com).any()
