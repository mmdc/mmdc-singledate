from typing import Literal

import pytest
import torch

from mmdc_singledate.models.components.losses import mask_and_flatten


def generate_dummy_s1(orbit: tuple[Literal["asc", "desc"], int]):
    batch_size = 2
    patch_size = 2
    s1_bands = 6
    s1_data = torch.ones(batch_size, s1_bands, patch_size, patch_size)
    mask = torch.ones(batch_size, 2, patch_size, patch_size)
    if orbit[0] == "asc":
        mask[:, 1, ...] = 0
        s1_data[:, : s1_bands // 2, :, :] = 0
        s1_data = s1_data * orbit[1]
    else:
        mask[:, 0, ...] = 0
        s1_data[:, s1_bands // 2 :, :, :] = 0
        s1_data = s1_data * orbit[1]
    return s1_data, mask


@pytest.mark.parametrize("orbit", [("asc", 2), ("desc", 4)])
def test_s1_mask(orbit):
    s1_data, mask = generate_dummy_s1(orbit)
    masked_data = mask_and_flatten(s1_data, mask)
    # all valid pixels are stacked in the batch dimension
    assert (
        masked_data.shape[0] == s1_data.shape[0] * s1_data.shape[2] * s1_data.shape[3]
    )
    assert masked_data.shape[1] == s1_data.shape[1] // 2
    assert masked_data.mean() == orbit[1]


@pytest.mark.parametrize(
    "batch_size, patch_size, x_min, x_max, y_min, y_max",
    [(10, 2, 0, 1, 1, 2), (10, 10, 5, 7, 0, 5)],
)
def test_s2_mask(batch_size, patch_size, x_min, x_max, y_min, y_max):
    s2_bands = 10
    s2_data = torch.ones(batch_size, s2_bands, patch_size, patch_size)
    mask = torch.ones(batch_size, 1, patch_size, patch_size)
    mask[:, :, x_min:x_max, y_min:y_max] = 0
    nb_valid_pixels = int((1.0 - mask).sum().item())
    masked_data = mask_and_flatten(s2_data, mask)
    assert masked_data.shape[0] == nb_valid_pixels
    assert masked_data.shape[1] == s2_bands
