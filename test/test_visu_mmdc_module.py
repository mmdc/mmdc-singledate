#!/usr/bin/env python3
# Copyright: (c) 2022 CESBIO / Centre National d'Etudes Spatiales
"""
Test the explore dataset functionalities
"""


import shutil
from pathlib import Path

import pytest

from mmdc_singledate.utils.explore_mmdc_dataset import export_visualization_graphs

from .utils import DATASET_DIR

TILES_CONFIG_DIR = f"{DATASET_DIR}/tiles_conf_training/tiny"


@pytest.mark.trex
def test_export_visualization_graphs() -> None:
    """
    test export_visualization_graphs function for patches
    """

    input_directory = Path(TILES_CONFIG_DIR)
    patch_index = [0, 1]
    export_path = Path("/tmp/dataset_inspection")
    if export_path.exists():
        shutil.rmtree(export_path)
    export_path.mkdir()

    tile_file = input_directory / "test_tiny.txt"

    with tile_file.open() as tile_f:
        input_tiles = tile_f.readlines()
        print(f"{input_tiles=}")
        export_visualization_graphs(
            input_directory=Path(DATASET_DIR),
            input_tiles=input_tiles,
            patch_index=patch_index,
            nb_patches=None,
            export_path=export_path,
        )

    with tile_file.open() as tf:
        for tile_roi in tf:
            tile, roi = tile_roi.split("_")
            assert Path(export_path / f"mmdc_ds_inspect_{tile}_{roi}_0.png").exists
    shutil.rmtree(export_path)
