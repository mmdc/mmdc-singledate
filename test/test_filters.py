import numpy as np
import pytest
import torch
from scipy import signal

from mmdc_singledate.utils.filters import despeckle_batch, lee_filter, window_reduction

from .utils import setup_data


@pytest.mark.trex
def test_despeckle():
    batch = setup_data(nb_samples=2)
    res = despeckle_batch(batch.s1_x)
    assert res.shape == batch.s1_x.shape


@pytest.mark.parametrize("kernel_size", [3, 5, 7])
def test_rolling_window(kernel_size):
    nb_samples = 3
    nb_channels = 6
    height = 256
    width = 256
    x = torch.rand(nb_samples, nb_channels, height, width)
    margin = (kernel_size - 1) // 2
    padded_patches = window_reduction(x, torch.mean, kernel_size, stride=1)
    print("patches_padded", padded_patches.size())
    for samp in range(x.shape[0]):
        for chan in range(x.shape[1]):
            np_mean = torch.tensor(
                signal.convolve2d(
                    x[samp, chan, :, :].cpu().detach().numpy(),
                    np.ones((kernel_size, kernel_size)),
                    "valid",
                )
                / (kernel_size * kernel_size)
            ).type_as(x)

            assert torch.all(
                torch.isclose(
                    padded_patches[samp, chan, margin:-margin, margin:-margin], np_mean
                )
            )


def test_lee_filter():
    nb_samples = 3
    nb_channels = 6
    height = 256
    width = 256
    x = torch.exp(torch.rand(nb_samples, nb_channels, height, width))
    winsize = 9
    x_lee = lee_filter(x, winsize)
    assert x_lee is not None
