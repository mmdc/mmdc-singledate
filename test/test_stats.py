import pytest
import torch

from mmdc_singledate.datamodules.components.datamodule_utils import (
    compute_stats,
    compute_stats_1d,
    mask_and_compute_stats_1d,
)


def test_compute_stats_1d():
    nb_samples = 1000000
    data = torch.rand(nb_samples)
    res = compute_stats_1d(data)
    assert 0.4 < res.mean < 0.6
    assert res.qmin < 0.1
    assert res.qmax > 0.9


@pytest.mark.skip(reason="Update for new data format")
def test_compute_stats_1d_mask():
    nb_samples = 1000000
    data = torch.rand(nb_samples)
    mask = torch.zeros(nb_samples, 1)
    mask[data < 0.5] = 1
    res = mask_and_compute_stats_1d(data, (0.05, 0.5, 0.95), mask=mask, dim=0)

    assert 0.4 + 0.25 < res.mean < 0.6 + 0.25
    assert res.qmin > 0.49
    assert res.qmax > 0.9


@pytest.mark.skip(reason="Update for new data format")
def test_compute_stats():
    nb_samples = 1000
    nb_channels = 3
    height = width = 10
    data = torch.rand(nb_samples, nb_channels, height, width)
    mask = torch.zeros(nb_samples, 1, height, width)
    mask[data[:, :1, ...] < 0.5] = 1
    res = compute_stats(data)
    for dim in range(2):
        assert 0.4 < res.mean[0][dim] < 0.6
        assert res.qmin[0][dim] < 0.1
        assert res.qmax[0][dim] > 0.9
    res = compute_stats(data, mask=mask)
    for dim in range(2):
        offset = 0.25 if dim < 1 else 0
        assert 0.4 + offset < res.mean[0][dim] < 0.6 + offset
        assert res.qmin[0][dim] < 0.1 + offset * 2
        assert res.qmax[0][dim] > 0.9
