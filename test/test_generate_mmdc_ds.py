#!/usr/bin/env python3
# Copyright: (c) 2023 CESBIO / Centre National d'Etudes Spatiales

"""
Test the datacreation routines in MMDC-SingleDate
"""

import glob
import os
import random
import shutil
from pathlib import Path

import pytest

from mmdc_singledate.datamodules.components.create_dataset import (
    CreateDatasetParams,
    create_dataset,
)
from mmdc_singledate.datamodules.components.datatypes import (
    DatasetPaths,
    PatchCreationParams,
    SeriesDays,
)

DATASET_DIR = "/work/CESBIO/projects/DeepChange/Iris/MMDC_OE/train"
# "/work/CESBIO/projects/DeepChange/Ekaterina/MMDC_OE/"


def prepare_data() -> dict[str, str | int | float]:
    res: dict[str, str | int | float] = {}
    res["samples_dir"] = DATASET_DIR
    res["export_path"] = "./tmp/torchfiles"
    if Path(res["export_path"]).exists():
        shutil.rmtree(res["export_path"])
    Path(res["export_path"]).mkdir(parents=True, exist_ok=True)
    res["days_gap"] = 1
    res["patch_size"] = 256
    res["patch_margin"] = 0
    res["threshold"] = 0.95
    res["roi"] = 0
    print(res["samples_dir"])
    print(res)
    print(glob.glob(f"{res['samples_dir']}/?????"))
    res["tile"] = random.sample(
        [
            t.split("/")[-1]
            for t in glob.glob(f"{res['samples_dir']}/?????")
            if Path(t).is_dir()
        ],
        1,
    )[0]
    print(res["tile"])
    return res


modalities = [
    "s2_set",
    "s2_masks",
    "s2_angles",
    "s1_set",
    "s1_valmasks",
    "s1_angles",
    "meteo_dew_temp",
    "meteo_prec",
    "meteo_sol_rad",
    "meteo_temp_max",
    "meteo_temp_mean",
    "meteo_temp_min",
    "meteo_vap_press",
    "meteo_wind_speed",
    "dem",
    "stats_s2",
    "stats_s1",
    "stats_meteo_dew_temp",
    "stats_meteo_prec",
    "stats_meteo_sol_rad",
    "stats_meteo_temp_max",
    "stats_meteo_temp_mean",
    "stats_meteo_temp_min",
    "stats_meteo_vap_press",
    "stats_meteo_wind_speed",
    "stats_dem",
]


def check_results(export_path, tile, roi):
    assert Path(os.path.join(export_path, tile))
    assert Path(os.path.join(export_path, tile, f"{tile}_bounds_{roi}.gpkg"))
    assert Path(os.path.join(export_path, tile, f"{tile}_bounds_{roi}.gpkg"))
    assert Path(os.path.join(export_path, tile, f"{tile}_{roi}.csv"))
    assert Path(os.path.join(export_path, tile, f"{tile}_{roi}.csv"))
    assert Path(os.path.join(export_path, tile, f"{tile}_dataset_256x256_{roi}.csv"))
    assert Path(os.path.join(export_path, tile, f"{tile}_dataset_256x256_{roi}.csv"))
    for mod in modalities:
        assert Path(os.path.join(export_path, tile, f"{tile}_{mod}_256x256_{roi}.pth"))
        assert Path(os.path.join(export_path, tile, f"{tile}_{mod}_256x256_{roi}.pth"))


def internal_create_dataset() -> None:
    """
    test function for create_dataset
    """
    params = prepare_data()
    print(f"Seleted tile: {params['tile']}")
    create_dataset(
        DatasetPaths(
            samples_dir=params["samples_dir"], export_path=params["export_path"]
        ),
        CreateDatasetParams(
            input_tile_list=[params["tile"]],
            days_selection=SeriesDays(
                days_gap=params["days_gap"],
                meteo_days_before=2,
                meteo_days_after=1,
                start_date_series=None,
                end_date_series=None,
            ),
            patch_params=PatchCreationParams(
                params["patch_size"] - 2 * params["patch_margin"],
                params["patch_margin"],
                params["threshold"],
            ),
            rois=[params["roi"]],
        ),
    )
    check_results(params["export_path"], params["tile"], params["roi"])


@pytest.mark.nojz
@pytest.mark.trex
@pytest.mark.slow
def test_create_dataset():
    internal_create_dataset()
