#!/usr/bin/env python3
# Copyright: (c) 2022 CESBIO / Centre National d'Etudes Spatiales

import torch

from mmdc_singledate.models.components.building_components import (
    build_dem_s1_angles,
    build_dem_s2_angles,
    build_embedders,
    build_meteo_embedder,
    build_s1_angles_embedder,
    build_s2_angles_embedder,
)
from mmdc_singledate.models.datatypes import (
    ConfigWithSizesAndEmbeddings,
    ConvnetParams,
    MMDCDataChannels,
    ModularEmbeddingConfig,
    UnetParams,
)

# Test building components

DATASIZES = MMDCDataChannels(6, 2, 10, 6, 4, 48)
BATCHSIZE = 5
PATCHSIZE = 256
DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def build_config() -> ConfigWithSizesAndEmbeddings:
    return ConfigWithSizesAndEmbeddings(
        DATASIZES,
        ModularEmbeddingConfig(
            ConvnetParams(3, [16, 8], [1, 1, 1]),
            UnetParams(4, [32, 64, 128], 3, 3),
            ConvnetParams(3, [16, 8], [1, 1, 1]),
            UnetParams(4, [32, 64, 128], 3, 3),
            ConvnetParams(3, [32, 32, 16], [1, 1, 1, 1]),
        ),
    )


def test_s1_angles_embedder():
    config = build_config()
    embedder = build_s1_angles_embedder(config, DEVICE)
    assert embedder is not None
    data = torch.rand(BATCHSIZE, DATASIZES.s1_angles, PATCHSIZE, PATCHSIZE).to(DEVICE)
    res = embedder(data)
    assert res is not None


def test_s2_angles_embedder():
    config = build_config()
    embedder = build_s2_angles_embedder(config, DEVICE)
    assert embedder is not None
    data = torch.rand(BATCHSIZE, DATASIZES.s2_angles, PATCHSIZE, PATCHSIZE).to(DEVICE)
    res = embedder(data)
    assert res is not None


def test_dem_s1_angles():
    config = build_config()
    embedder = build_dem_s1_angles(config, DEVICE)
    assert embedder is not None
    data = torch.rand(BATCHSIZE, DATASIZES.dem, PATCHSIZE, PATCHSIZE).to(DEVICE)
    angles = torch.rand(
        BATCHSIZE, config.embeddings.s1_angles.out_channels, PATCHSIZE, PATCHSIZE
    ).to(DEVICE)
    res = embedder(data, angles)
    assert res is not None


def test_dem_s2_angles():
    config = build_config()
    embedder = build_dem_s2_angles(config, DEVICE)
    assert embedder is not None
    data = torch.rand(BATCHSIZE, DATASIZES.dem, PATCHSIZE, PATCHSIZE).to(DEVICE)
    angles = torch.rand(
        BATCHSIZE, config.embeddings.s2_angles.out_channels, PATCHSIZE, PATCHSIZE
    ).to(DEVICE)
    res = embedder(data, angles)
    assert res is not None


def test_meteo_embedder():
    config = build_config()
    embedder = build_meteo_embedder(config, DEVICE)
    assert embedder is not None
    data = torch.rand(BATCHSIZE, DATASIZES.meteo, PATCHSIZE, PATCHSIZE).to(DEVICE)
    res = embedder(data)
    assert res is not None


def test_build_embedders():
    config = build_config()
    embedder = build_embedders(config, DEVICE)
    assert embedder is not None
