#!/usr/bin/env bash

# Although this could run automatically, it's better to manually execute
# this script. Proxies are brittle, pip is not robust and we have many
# dependencies.

export python_version="3.10.9"
export name="mmdc-sgld-with-pip"

source ~/set_proxy.sh    # or your preferred way to configure the proxy
if [ -z "$https_proxy" ]
then
    echo "Please set https_proxy environment variable before running this script"
    exit 1
fi

# Installing the env in a standard path simplifies job configuration
#export target=/work/scratch/env/$USER/virtualenv/$name
export target=/$WORK/.conda/envs/$name


echo "Installing $name in $target ..."

if [ -d "$target" ]; then
   echo "Cleaning previous conda env"
   rm -rf $target
fi

# Clean all modules
module purge

# Clone third parties
rm -rf thirdparties/sensorsio
git clone https://src.koda.cnrs.fr/mmdc/sensorsio.git thirdparties/sensorsio
rm -rf thirdparties/torchutils
git clone https://src.koda.cnrs.fr/mmdc/torchutils.git thirdparties/torchutils


# Create blank virtualenv
#module load conda/22.11.1
module load anaconda-py3
conda activate
conda create --yes --prefix $target python==${python_version} pip
conda deactivate
conda activate $target

# Check that we have the expected versions
which python
python --version

# nvidia, torch & lightning
pip install -r requirements-pytorch.txt

# Requirements
pip install -r requirements-mmdc-sgld.txt

# Install sensorsio
pip install thirdparties/sensorsio

# Install torchutils
pip install thirdparties/torchutils

# Install the current project in edit mode
pip install -e .[testing]

# Activate pre-commit hooks
pre-commit install

# End
conda deactivate
