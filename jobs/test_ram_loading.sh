#!/usr/bin/env bash

module load monitoring/2.0

source ~/env_shell/mmdc_init.sh

start_monitoring.sh --name test_ram

cd ~/src/mmdc/mmdc-singledate/

python ~/src/mmdc/mmdc-singledate/bin/test_dataloading.py

stop_monitoring.sh --name test_gpu
